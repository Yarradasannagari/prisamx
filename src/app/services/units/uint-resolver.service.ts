import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Unit, UnitService } from './uint.service';

@Injectable({ providedIn: 'root' })
export class UnitResolverService implements Resolve<Unit> {
  constructor(private unitService: UnitService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Unit> | Promise<Unit> | Unit {
    const id = route.paramMap.get('id');
    return this.unitService.get(id);
  }
}
