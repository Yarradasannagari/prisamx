import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Plant {
  changed_by?:number;
  changed_date?:string;
  changed_time?: string;
  city: string;
  country: string;
  createdBy?: string;
  createdDate?: string;
  created_time?: string;
  housenum: string;
  housenum1: string;
  id: string;
  name: string;
  name1: string;
  name2: string;
  orgId: string;
  plant: string;
  plantId: string;
  postcode1: string;
  region: string;
  street: string;
  werks: string;
  zipcode: string;
  active: boolean;
}


@Injectable({
  providedIn: 'root',
})
export class PlantService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Plant> {
    return this.entitySvc.get(Url.plants, id);
  }

  list(params?: Record<string, any>): Observable<Plant[]> {
    return this.entitySvc.list(Url.plants, params);
  }

  create(aclRule: Plant): Observable<Plant> {
    return this.entitySvc.create(Url.plants, aclRule); 
  }

  update(aclRule: Plant): Observable<Plant> {
    return this.entitySvc.update(Url.plants, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.plants, id);
  }
  
}
