import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Unit, UnitService } from "src/app/services/units/uint.service";
import { GetUnit } from "src/app/store/stores/units/unit.actions";
import {
  getUnitError,
  getUnit,
  getUnitLoading,
} from "src/app/store/stores/units/unit.store";
import { State, Store } from "../../../store";
import { DeleteUnitModelComponent } from "../delete-unit-model/delete-unit-model/delete-unit-model.component";

@Component({
  selector: "app-unit",
  templateUrl: "./unit.component.html",
  styleUrls: ["./unit.component.scss"],
})
export class UnitComponent implements OnDestroy, OnInit {
  unit$: Observable<Unit[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  unit: Unit[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private unitService: UnitService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getUnitLoading);
    this.store.dispatch(new GetUnit());
    this.unit$ = this.store.select(getUnit);
    this.store.select(getUnitError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }
  
  reTry() {
    this.isLoading$ = this.store.select(getUnitLoading);
    this.store.dispatch(new GetUnit());
    this.unit$ = this.store.select(getUnit);
    this.store.select(getUnitError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteUnitModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
