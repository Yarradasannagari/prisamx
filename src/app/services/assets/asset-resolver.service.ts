import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Asset, AssetService } from './asset.service';

@Injectable({ providedIn: 'root' })
export class AssetResolverService implements Resolve<Asset> {
  constructor(private assetService: AssetService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Asset> | Promise<Asset> | Asset {
    const id = route.paramMap.get('id');
    return this.assetService.get(id);
  }
}
