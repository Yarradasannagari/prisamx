import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Inspectiontype, InspectiontypeService } from './inspection-type.service';

@Injectable({ providedIn: 'root' })
export class InspectiontypeResolverService implements Resolve<Inspectiontype> {
  constructor(private inspectiontypeService: InspectiontypeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Inspectiontype> | Promise<Inspectiontype> | Inspectiontype {
    const id = route.paramMap.get('id');
    return this.inspectiontypeService.get(id);
  }
}
