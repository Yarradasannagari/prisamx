import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Plant } from "src/app/services/plant/plant.service";
import { CreatePlant, UpdatePlant } from "src/app/store/stores/plant/plant.actions";
import { getPlantCreated, getPlantUpdated } from "src/app/store/stores/plant/plant.store";
import { State, Store } from "../../../store";
import { Organization } from "src/app/services/organization/organization.service";
import { GetOrganizations } from "src/app/store/stores/organizations/organizations.actions";
import { getOrganizations } from "src/app/store/stores/organizations/organizations.store";

@Component({
  selector: "app-plant-form",
  templateUrl: "./plant-form.component.html",
  styleUrls: ["./plant-form.component.scss"],
})
export class PlantFormComponent implements OnInit { 
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    name: "",
    plantId: "",
    orgId: "",
    name1: "",
    name2: "",
    plant: "",
    postcode1: "",
    region: "",
    street: "",
    werks: "",
    zipcode: "",
    city: "",
    country: "",
    housenum: "",
    housenum1: "",
    active: true, 

  };
  plant: Plant;
  organization: Organization[];

  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(new GetOrganizations());
    this.store.select(getOrganizations).subscribe((organization) => {
      this.organization = organization;
    });
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["plant"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.plantId =
          this.route.snapshot.data["report"].plantId;
        this.formData.orgId = this.route.snapshot.data["report"].orgId;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.plantId);
    this.formGroup = this.fb.group({
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      plantId: [
        this.formData.plantId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(10),
        ]),
      ],
      orgId: [
        this.formData.orgId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(10),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const plant: Plant = {
      id: this.formData.id,
      plantId: this.formData.plantId,
      name: this.formData.name,
      orgId: this.formData.orgId,
      name1: this.formData.name1,
      name2: this.formData.name2,
      plant: this.formData.plant,
      postcode1: this.formData.postcode1,
      region: this.formData.region,
      street: this.formData.street,
      werks: this.formData.werks,
      zipcode: this.formData.zipcode,
      city: this.formData.city,
      country: this.formData.country,
      housenum: this.formData.housenum,
      housenum1: this.formData.housenum1,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updatePlant(plant);
    } else {
      this.createPlant(plant);
    }
  }
  private createPlant(plant: Plant): void {
    this.store.dispatch(new CreatePlant(plant));
    this.store
      .select(getPlantCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updatePlant(plant: Plant): void {
    this.store.dispatch(new UpdatePlant(plant));
    this.store
      .select(getPlantUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "plant"]);
  }
}
