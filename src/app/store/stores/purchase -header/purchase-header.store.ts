import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PurchaseHeader } from "src/app/services/purchase -header/purchase-header.service";

import { State } from "../..";

import { PurchaseHeaderTypes, Union } from "./purchase-header.actions";

export interface PurchaseHeaderState {
  loading: boolean;
  entities: PurchaseHeader[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
  uploading: boolean;
  uploaded: boolean;
}

export const INITIAL_STATE: PurchaseHeaderState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
  uploading: false,
  uploaded: false,
};

export function PurchaseHeaderReducer(
  state: PurchaseHeaderState = INITIAL_STATE,
  action: Union
): PurchaseHeaderState {
  switch (action.type) {
    case PurchaseHeaderTypes.GetPurchaseHeader:
      return {
        ...state,
        loading: true,
      };
    case PurchaseHeaderTypes.GetPurchaseHeaderSuccess:
      const purchaseHeader: PurchaseHeader[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: purchaseHeader,
      };
    case PurchaseHeaderTypes.GetPurchaseHeaderError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case PurchaseHeaderTypes.CreatePurchaseHeader:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case PurchaseHeaderTypes.CreatePurchaseHeadersuccess:
      const createdPurchaseHeader: PurchaseHeader = action.payload;
      const entitiesAppended = [...state.entities, createdPurchaseHeader];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case PurchaseHeaderTypes.UpdatePurchaseHeader:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case PurchaseHeaderTypes.UpdatePurchaseHeadersuccess:
      const updatedPurchaseHeader: PurchaseHeader = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedPurchaseHeader];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case PurchaseHeaderTypes.UpdatePurchaseHeaderError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case PurchaseHeaderTypes.CreatePurchaseHeaderError:
      return {
        ...state,
        creating: false,
        created: false,
      };

      case PurchaseHeaderTypes.UplodPurchaseHeader:
        return {
          ...state,
          uploading: true,
          uploaded: false,
        };
      case PurchaseHeaderTypes.UplodPurchaseHeadersuccess:
        const uploadPurchaseHeader: PurchaseHeader = action.payload;
        const uploadedentitiesAppended = [...state.entities, uploadPurchaseHeader];
        return {
          ...state,
          uploading: false,
          entities: uploadedentitiesAppended,
          uploaded: true,
        };
        case PurchaseHeaderTypes.UplodPurchaseHeaderError:
          return {
            ...state,
            uploading: false,
            uploaded: false,
          };

    case PurchaseHeaderTypes.DeletePurchaseHeader:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case PurchaseHeaderTypes.DeletePurchaseHeadersuccess:
      const afterDelete = state.entities.filter((purchaseheader) => purchaseheader.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case PurchaseHeaderTypes.DeletePurchaseHeaderError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getPurchaseHeaderState = createFeatureSelector<State, PurchaseHeaderState>("otfheader");

export const getPurchaseHeaderLoading = createSelector(
  getPurchaseHeaderState,
  (state) => state.loading
);

export const getPurchaseHeader = createSelector(getPurchaseHeaderState, (state) => state.entities);

export const getPurchaseHeaderCreating = createSelector(
  getPurchaseHeaderState,
  (state) => state.creating
);

export const getPurchaseHeaderCreated = createSelector(
  getPurchaseHeaderState,
  (state) => state.created
);

export const getPurchaseHeaderUploading = createSelector(
  getPurchaseHeaderState,
  (state) => state.uploading
);

export const getPurchaseHeaderUploaded = createSelector(
  getPurchaseHeaderState,
  (state) => state.uploaded
);

export const getPurchaseHeaderUpdating = createSelector(
  getPurchaseHeaderState,
  (state) => state.updating
);

export const getPurchaseHeaderUpdated = createSelector(
  getPurchaseHeaderState,
  (state) => state.updated
);
export const getPurchaseHeaderDeleting = createSelector(
  getPurchaseHeaderState,
  (state) => state.deleting
);

export const getPurchaseHeaderDeleted = createSelector(
  getPurchaseHeaderState,
  (state) => state.deleted
);

export const getPurchaseHeaderError = createSelector(
  getPurchaseHeaderState,
  (state) => state.error
);
