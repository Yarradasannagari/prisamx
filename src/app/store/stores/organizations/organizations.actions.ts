import { Action } from "@ngrx/store";
import { Organization } from "src/app/services/organization/organization.service";

export enum OrganizationTypes {
  GetOrganizations = "[Organization] GetOrganizations",
  GetOrganizationsSuccess = "[Organization] GetOrganizationsSuccess",
  GetOrganizationsError = "[Role] GetOrganizationsError",

  // GetOrganizationsError = "[Organization] GetOrganizationsError",
  CreateOrganization = "[Organization] CreateOrganization",
  Createorganizationsuccess = "[Organization] Createorganizationsuccess",
  CreateOrganizationError = "[Organization] CreateOrganizationError",
  UpdateOrganization = "[Organization] UpdateOrganization",
  UpdateOrganizationsuccess = "[Organization] UpdateOrganizationsuccess",
  UpdateOrganizationError = "[Organization] UpdateOrganizationError",
  DeleteOrganization = "[Organization] DeleteOrganization",
  DeleteOrganizationError = "[Organization] DeleteOrganizationError",
  DeleteOrganizationsuccess = "[Organization] DeleteOrganizationsuccess",
}

export class GetOrganizations implements Action {
  readonly type = OrganizationTypes.GetOrganizations;
}

export class GetOrganizationsSuccess implements Action {
  readonly type = OrganizationTypes.GetOrganizationsSuccess;
  constructor(readonly payload: Organization[]) {}
}
export class GetOrganizationsError implements Action {
  readonly type = OrganizationTypes.GetOrganizationsError;
  constructor(readonly payload: boolean) {}
}

export class CreateOrganization implements Action {
  readonly type = OrganizationTypes.CreateOrganization;
  constructor(readonly payload: Organization) {}
}

export class Createorganizationsuccess implements Action {
  readonly type = OrganizationTypes.Createorganizationsuccess;
  constructor(readonly payload: Organization) {}
}

export class CreateOrganizationError implements Action {
  readonly type = OrganizationTypes.CreateOrganizationError;
}
export class UpdateOrganization implements Action {
  readonly type = OrganizationTypes.UpdateOrganization;
  constructor(readonly payload: Organization) {}
}

export class UpdateOrganizationsuccess implements Action {
  readonly type = OrganizationTypes.UpdateOrganizationsuccess;
  constructor(readonly payload: Organization) {}
}

export class UpdateOrganizationError implements Action {
  readonly type = OrganizationTypes.UpdateOrganizationError;
}

export class DeleteOrganization implements Action {
  readonly type = OrganizationTypes.DeleteOrganization;
  constructor(readonly payload: string) {}
}

export class DeleteOrganizationsuccess implements Action {
  readonly type = OrganizationTypes.DeleteOrganizationsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteOrganizationError implements Action {
  readonly type = OrganizationTypes.DeleteOrganizationError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetOrganizations
  | GetOrganizationsSuccess
  |  GetOrganizationsError
  | CreateOrganization
  | Createorganizationsuccess
  | CreateOrganizationError
  | UpdateOrganization
  | UpdateOrganizationsuccess
  | UpdateOrganizationError
  | DeleteOrganization
  | DeleteOrganizationError
  | DeleteOrganizationsuccess;
