import { Action } from "@ngrx/store";
import { Inspectioncategory } from "../../../services/inspection-category/inspection-category.service";

export enum InspectioncategoryTypes {
  GetInspectioncategory = "[Inspectioncategory] GetInspectioncategory",
  GetInspectioncategorySuccess = "[Inspectioncategory] GetInspectioncategorySuccess",
  GetInspectioncategoryError = "[Inspectioncategory] GetInspectioncategoryError",
  CreateInspectioncategory = "[Inspectioncategory] CreateInspectioncategory",
  CreateInspectioncategorysuccess = "[Inspectioncategory] CreateInspectioncategorysuccess",
  CreateInspectioncategoryError = "[Inspectioncategory] CreateInspectioncategoryError",
  UpdateInspectioncategory = "[Inspectioncategory] UpdateInspectioncategory",
  UpdateInspectioncategorysuccess = "[Inspectioncategory] UpdateInspectioncategorysuccess",
  UpdateInspectioncategoryError = "[Inspectioncategory] UpdateInspectioncategoryError",
  DeleteInspectioncategory = "[Inspectioncategory] DeleteInspectioncategory",
  DeleteInspectioncategoryError = "[Inspectioncategory] DeleteInspectioncategoryError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectioncategorysuccess = "[AclRule] DeleteInspectioncategorysuccess",
}

export class GetInspectioncategory implements Action { 
  readonly type = InspectioncategoryTypes.GetInspectioncategory;
}

export class GetInspectioncategorySuccess implements Action {
  readonly type = InspectioncategoryTypes.GetInspectioncategorySuccess;
  constructor(readonly payload: Inspectioncategory[]) {}
}
export class GetInspectioncategoryError implements Action {
  readonly type = InspectioncategoryTypes.GetInspectioncategoryError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectioncategory implements Action {
  readonly type = InspectioncategoryTypes.CreateInspectioncategory;
  constructor(readonly payload: Inspectioncategory) {}
}

export class CreateInspectioncategorysuccess implements Action {
  readonly type = InspectioncategoryTypes.CreateInspectioncategorysuccess;
  constructor(readonly payload: Inspectioncategory) {}
}

export class CreateInspectioncategoryError implements Action {
  readonly type = InspectioncategoryTypes.CreateInspectioncategoryError;
}
export class UpdateInspectioncategory implements Action {
  readonly type = InspectioncategoryTypes.UpdateInspectioncategory;
  constructor(readonly payload: Inspectioncategory) {}
}

export class UpdateInspectioncategorysuccess implements Action {
  readonly type = InspectioncategoryTypes.UpdateInspectioncategorysuccess;
  constructor(readonly payload: Inspectioncategory) {}
}

export class UpdateInspectioncategoryError implements Action {
  readonly type = InspectioncategoryTypes.UpdateInspectioncategoryError;
}

export class DeleteInspectioncategory implements Action {
  readonly type = InspectioncategoryTypes.DeleteInspectioncategory;
  constructor(readonly payload: string) {}
}

export class DeleteInspectioncategorysuccess implements Action {
  readonly type = InspectioncategoryTypes.DeleteInspectioncategorysuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectioncategoryError implements Action {
  readonly type = InspectioncategoryTypes.DeleteInspectioncategoryError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectioncategory
  | GetInspectioncategorySuccess
  | GetInspectioncategoryError
  | CreateInspectioncategory
  | CreateInspectioncategorysuccess
  | CreateInspectioncategoryError
  | DeleteInspectioncategory
  | DeleteInspectioncategorysuccess
  | DeleteInspectioncategoryError
  | UpdateInspectioncategory
  | UpdateInspectioncategorysuccess
  | UpdateInspectioncategoryError
