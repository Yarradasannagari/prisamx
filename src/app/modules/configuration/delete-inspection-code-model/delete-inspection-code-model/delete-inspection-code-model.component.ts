import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteInspectioncode} from "src/app/store/stores/inspection-code/inspection-code.actions";
import { getInspectioncodeDeleted } from "src/app/store/stores/inspection-code/inspection-code.store";
@Component({
  selector: "app-delete-inspection-code-model",
  templateUrl: "./delete-inspection-code-model.component.html",
  styleUrls: ["./delete-inspection-code-model.component.scss"],
})
export class DeleteInspectionCodeModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteInspectioncode() {
    this.isLoading = true;
    this.store.dispatch(new DeleteInspectioncode(this.id));
    this.store
      .select(getInspectioncodeDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
