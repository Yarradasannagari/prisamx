import { Action } from "@ngrx/store";
import { Inspectiontype } from "../../../services/inspection-types/inspection-type.service";

export enum InspectiontypeTypes { 
  GetInspectiontype = "[Inspectiontype] GetInspectiontype",
  GetInspectiontypeSuccess = "[Inspectiontype] GetInspectiontypeSuccess",
  GetInspectiontypeError = "[Inspectiontype] GetInspectiontypeError",
  CreateInspectiontype = "[Inspectiontype] CreateInspectiontype",
  CreateInspectiontypesuccess = "[Inspectiontype] CreateInspectiontypesuccess",
  CreateInspectiontypeError = "[Inspectiontype] CreateInspectiontypeError",
  UpdateInspectiontype = "[Inspectiontype] UpdateInspectiontype",
  UpdateInspectiontypesuccess = "[Inspectiontype] UpdateInspectiontypesuccess",
  UpdateInspectiontypeError = "[Inspectiontype] UpdateInspectiontypeError",
  DeleteInspectiontype = "[Inspectiontype] DeleteInspectiontype",
  DeleteInspectiontypeError = "[Inspectiontype] DeleteInspectiontypeError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectiontypesuccess = "[AclRule] DeleteInspectiontypesuccess",
}

export class GetInspectiontype implements Action { 
  readonly type = InspectiontypeTypes.GetInspectiontype;
}

export class GetInspectiontypeSuccess implements Action {
  readonly type = InspectiontypeTypes.GetInspectiontypeSuccess;
  constructor(readonly payload: Inspectiontype[]) {}
}
export class GetInspectiontypeError implements Action {
  readonly type = InspectiontypeTypes.GetInspectiontypeError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectiontype implements Action {
  readonly type = InspectiontypeTypes.CreateInspectiontype;
  constructor(readonly payload: Inspectiontype) {}
}

export class CreateInspectiontypesuccess implements Action {
  readonly type = InspectiontypeTypes.CreateInspectiontypesuccess;
  constructor(readonly payload: Inspectiontype) {}
}

export class CreateInspectiontypeError implements Action {
  readonly type = InspectiontypeTypes.CreateInspectiontypeError;
}
export class UpdateInspectiontype implements Action {
  readonly type = InspectiontypeTypes.UpdateInspectiontype;
  constructor(readonly payload: Inspectiontype) {}
}

export class UpdateInspectiontypesuccess implements Action {
  readonly type = InspectiontypeTypes.UpdateInspectiontypesuccess;
  constructor(readonly payload: Inspectiontype) {}
}

export class UpdateInspectiontypeError implements Action {
  readonly type = InspectiontypeTypes.UpdateInspectiontypeError;
}

export class DeleteInspectiontype implements Action {
  readonly type = InspectiontypeTypes.DeleteInspectiontype;
  constructor(readonly payload: string) {}
}

export class DeleteInspectiontypesuccess implements Action {
  readonly type = InspectiontypeTypes.DeleteInspectiontypesuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectiontypeError implements Action {
  readonly type = InspectiontypeTypes.DeleteInspectiontypeError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectiontype
  | GetInspectiontypeSuccess
  | GetInspectiontypeError
  | CreateInspectiontype
  | CreateInspectiontypesuccess
  | CreateInspectiontypeError
  | DeleteInspectiontype
  | DeleteInspectiontypesuccess
  | DeleteInspectiontypeError
  | UpdateInspectiontype
  | UpdateInspectiontypesuccess
  | UpdateInspectiontypeError
