import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Organization } from "src/app/services/organization/organization.service";

import { State } from "../..";

import { OrganizationTypes, Union } from "./organizations.actions";

export interface OrganizationState {
  loading: boolean;
  entities: Organization[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: OrganizationState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function OrganizationReducer(
  state: OrganizationState = INITIAL_STATE,
  action: Union
): OrganizationState {
  switch (action.type) {
    case OrganizationTypes.GetOrganizations:
      return {
        ...state,
        loading: true,
      };
    case OrganizationTypes.GetOrganizationsSuccess:
      const Organizations: Organization[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: Organizations,
      };
      case OrganizationTypes.GetOrganizationsError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case OrganizationTypes.CreateOrganization:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case OrganizationTypes.Createorganizationsuccess:
      const createdOrganization: Organization = action.payload;
      const entitiesAppended = [...state.entities, createdOrganization];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
    case OrganizationTypes.UpdateOrganization:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case OrganizationTypes.UpdateOrganizationsuccess:
      const updatedOrganization: Organization = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedOrganization];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
    case OrganizationTypes.UpdateOrganizationError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case OrganizationTypes.CreateOrganizationError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case OrganizationTypes.DeleteOrganization:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case OrganizationTypes.DeleteOrganizationsuccess:
      const afterDelete = state.entities.filter(
        (organization) => organization.id !== action.payload
      );
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities: afterDelete,
      };

    case OrganizationTypes.DeleteOrganizationError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getOrganizationState = createFeatureSelector<State, OrganizationState>("organization");

export const getOrganizationsLoading = createSelector(
  getOrganizationState,
  (state) => state.loading
);

export const getOrganizations = createSelector(
  getOrganizationState,
  (state) => state.entities
);

export const getOrganizationCreating = createSelector(
  getOrganizationState,
  (state) => state.creating
);

export const getOrganizationCreated = createSelector(
  getOrganizationState,
  (state) => state.created
);
export const getOrganizationUpdating = createSelector(
  getOrganizationState,
  (state) => state.updating
);

export const getOrganizationUpdated = createSelector(
  getOrganizationState,
  (state) => state.updated
);
export const getOrganizationDeleting = createSelector(
  getOrganizationState,
  (state) => state.deleting
);

export const getOrganizationDeleted = createSelector(
  getOrganizationState,
  (state) => state.deleted
);

export const getOrganizationError = createSelector(
  getOrganizationState,
  (state) => state.error
);
