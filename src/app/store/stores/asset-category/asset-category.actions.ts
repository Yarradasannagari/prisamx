import { Action } from "@ngrx/store";
import { Assetcategory } from "../../../services/asset-category/asset-category.service";

export enum AssetcategoryTypes {
  GetAssetcategory = "[Assetcategory] GetAssetcategory",
  GetAssetcategorySuccess = "[Assetcategory] GetAssetcategorySuccess",
  GetAssetcategoryError = "[Assetcategory] GetAssetcategoryError",
  CreateAssetcategory = "[Assetcategory] CreateAssetcategory",
  CreateAssetcategorysuccess = "[Assetcategory] CreateAssetcategorysuccess",
  CreateAssetcategoryError = "[Assetcategory] CreateAssetcategoryError",
  UpdateAssetcategory = "[Assetcategory] UpdateAssetcategory",
  UpdateAssetcategorysuccess = "[Assetcategory] UpdateAssetcategorysuccess",
  UpdateAssetcategoryError = "[Assetcategory] UpdateAssetcategoryError",
  DeleteAssetcategory = "[Assetcategory] DeleteAssetcategory",
  DeleteAssetcategoryError = "[Assetcategory] DeleteAssetcategoryError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteAssetcategorysuccess = "[AclRule] DeleteAssetcategorysuccess",
}

export class GetAssetcategory implements Action { 
  readonly type = AssetcategoryTypes.GetAssetcategory;
}

export class GetAssetcategorySuccess implements Action {
  readonly type = AssetcategoryTypes.GetAssetcategorySuccess;
  constructor(readonly payload: Assetcategory[]) {}
}
export class GetAssetcategoryError implements Action {
  readonly type = AssetcategoryTypes.GetAssetcategoryError;
  constructor(readonly payload: boolean) {}
}

export class CreateAssetcategory implements Action {
  readonly type = AssetcategoryTypes.CreateAssetcategory;
  constructor(readonly payload: Assetcategory) {}
}

export class CreateAssetcategorysuccess implements Action {
  readonly type = AssetcategoryTypes.CreateAssetcategorysuccess;
  constructor(readonly payload: Assetcategory) {}
}

export class CreateAssetcategoryError implements Action {
  readonly type = AssetcategoryTypes.CreateAssetcategoryError;
}
export class UpdateAssetcategory implements Action {
  readonly type = AssetcategoryTypes.UpdateAssetcategory;
  constructor(readonly payload: Assetcategory) {}
}

export class UpdateAssetcategorysuccess implements Action {
  readonly type = AssetcategoryTypes.UpdateAssetcategorysuccess;
  constructor(readonly payload: Assetcategory) {}
}

export class UpdateAssetcategoryError implements Action {
  readonly type = AssetcategoryTypes.UpdateAssetcategoryError;
}

export class DeleteAssetcategory implements Action {
  readonly type = AssetcategoryTypes.DeleteAssetcategory;
  constructor(readonly payload: string) {}
}

export class DeleteAssetcategorysuccess implements Action {
  readonly type = AssetcategoryTypes.DeleteAssetcategorysuccess;
  constructor(readonly payload: string) {}
}

export class DeleteAssetcategoryError implements Action {
  readonly type = AssetcategoryTypes.DeleteAssetcategoryError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetAssetcategory
  | GetAssetcategorySuccess
  | GetAssetcategoryError
  | CreateAssetcategory
  | CreateAssetcategorysuccess
  | CreateAssetcategoryError
  | DeleteAssetcategory
  | DeleteAssetcategorysuccess
  | DeleteAssetcategoryError
  | UpdateAssetcategory
  | UpdateAssetcategorysuccess
  | UpdateAssetcategoryError
