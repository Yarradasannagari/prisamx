import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteInspectionclass} from "src/app/store/stores/inspection-class/inspection-class.actions";
import { getInspectionclassDeleted } from "src/app/store/stores/inspection-class/inspection-class.store";
@Component({
  selector: "app-delete-inspection-class-model",
  templateUrl: "./delete-inspection-class-model.component.html",
  styleUrls: ["./delete-inspection-class-model.component.scss"],
})
export class DeleteInspectionClassModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteInspectionclass() {
    this.isLoading = true;
    this.store.dispatch(new DeleteInspectionclass(this.id));
    this.store
      .select(getInspectionclassDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
