import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { User, UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class UserResolverService implements Resolve<User> {
  constructor(private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<User> | Promise<User> | User {
    const id = route.paramMap.get('id');
    return this.userService.get(id);
  }
}
