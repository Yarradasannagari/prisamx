import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { StringLiteralLike } from "typescript";
import { EntityService } from "../entity.service";

import { Url } from "../urls";

export interface PurchaseHeader {
  id: string;
  pdnum: string;
  prnum: string;
  pdDate: string;
  orgid: string;
  pdstatus: string;
  vendor: string;
  name: string;
  mobile: string;
  email: string;
  city: string;
  country: string;
  trnid: string;
  fmonth:string;
  fyear:string;
}

@Injectable({
  providedIn: "root",
})
export class PurchaseHeaderService {
  constructor(
    private readonly http: HttpClient,
    private entitySvc: EntityService<any>
  ) {}
  get(id: any): Observable<PurchaseHeader> {
    return this.entitySvc.get(Url.otfheader, id);
  }

  list(params?: Record<string, any>): Observable<PurchaseHeader[]> {
    return this.entitySvc.list(Url.otfheader, params);
  }

  create(purchaseHeader: PurchaseHeader): Observable<PurchaseHeader> {
    return this.entitySvc.create(Url.otfheader, purchaseHeader);
  }
  upload(list: PurchaseHeader[]): Observable<PurchaseHeader> {
    return this.entitySvc.upload(Url.otfheaderupload, list);
  }

  update(purchaseHeader: PurchaseHeader): Observable<PurchaseHeader> {
    return this.entitySvc.update(Url.otfheader, purchaseHeader);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.otfheader, id);
  }
}
