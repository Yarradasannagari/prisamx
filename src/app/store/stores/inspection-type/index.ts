import * as InspectiontypeActions from "./inspection-type.actions";
import { InspectiontypeEffects } from "./inspection-type.effects";
import { INITIAL_STATE, InspectiontypeReducer, InspectiontypeState } from "./inspection-type.store";

export { InspectiontypeActions, InspectiontypeReducer, InspectiontypeState, INITIAL_STATE, InspectiontypeEffects };
