import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Locationtype, LocationtypeService } from "src/app/services/location-type/location-type.service";
import { GetLocationtype } from "src/app/store/stores/location-type/location-type.actions";
import { getLocationtypeError, getLocationtype, getLocationtypeLoading,} from "src/app/store/stores/location-type/location-tye.store";
import { State, Store } from "../../../store";
import { DeleteLocationTypeModelComponent } from "../delete-location-type-model/delete-location-type-model/delete-location-type-model.component";

@Component({
  selector: "app-location-type",
  templateUrl: "./location-type.component.html",
  styleUrls: ["./location-type.component.scss"],
})
export class LocationTypeComponent implements OnDestroy, OnInit {
  locationtype$: Observable<Locationtype[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  locationtype: Locationtype[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private locationtypeService: LocationtypeService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getLocationtypeLoading);
    this.store.dispatch(new GetLocationtype());
    this.locationtype$ = this.store.select(getLocationtype);
    this.store.select(getLocationtypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 
  
  reTry() {
    this.isLoading$ = this.store.select(getLocationtypeLoading);
    this.store.dispatch(new GetLocationtype());
    this.locationtype$ = this.store.select(getLocationtype);
    this.store.select(getLocationtypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteLocationTypeModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
