import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Plant, PlantService } from './plant.service';

@Injectable({ providedIn: 'root' })
export class PlantResolverService implements Resolve<Plant> {
  constructor(private plantService: PlantService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Plant> | Promise<Plant> | Plant {
    const id = route.paramMap.get('id');
    return this.plantService.get(id);
  }
}
