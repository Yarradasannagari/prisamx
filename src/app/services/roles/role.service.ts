import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Role {
  id:string;
  role: string;
  description: string;
  active: boolean;
  createdBy?: string;
  createdDate?: string;
  changedBy?: number;
  changedDate?: string;
 
}


@Injectable({
  providedIn: 'root',
})
export class RoleService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Role> {
    return this.entitySvc.get(Url.roles, id);
  }

  list(params?: Record<string, any>): Observable<Role[]> {
    return this.entitySvc.list(Url.roles, params);
  }

  create(aclRule: Role): Observable<Role> {
    return this.entitySvc.create(Url.roles, aclRule);
  }

  update(aclRule: Role): Observable<Role> {
    return this.entitySvc.update(Url.roles, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.roles, id);
  }
  
}
