import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionTypeFormComponent } from './inspection-type-form.component';

describe('InspectionTypeFormComponent', () => {
  let component: InspectionTypeFormComponent;
  let fixture: ComponentFixture<InspectionTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
