import { Action } from "@ngrx/store";
import { Document } from "../../../services/documents/document.service";

export enum DocumentTypes {
  GetDocument = "[Document] GetDocument",
  GetDocumentSuccess = "[Document] GetDocumentSuccess",
  GetDocumentError = "[Document] GetDocumentError",
  CreateDocument = "[Document] CreateDocument",
  CreateDocumentsuccess = "[Document] CreateDocumentsuccess",
  CreateDocumentError = "[Document] CreateDocumentError",
  UpdateDocument = "[Document] UpdateDocument",
  UpdateDocumentsuccess = "[Document] UpdateDocumentsuccess",
  UpdateDocumentError = "[Document] UpdateDocumentError",
  DeleteDocument = "[Document] DeleteDocument",
  DeleteDocumentError = "[Document] DeleteDocumentError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteDocumentsuccess = "[AclRule] DeleteDocumentsuccess",
}

export class GetDocument implements Action { 
  readonly type = DocumentTypes.GetDocument;
}

export class GetDocumentSuccess implements Action {
  readonly type = DocumentTypes.GetDocumentSuccess;
  constructor(readonly payload: Document[]) {}
}
export class GetDocumentError implements Action {
  readonly type = DocumentTypes.GetDocumentError;
  constructor(readonly payload: boolean) {}
}

export class CreateDocument implements Action {
  readonly type = DocumentTypes.CreateDocument;
  constructor(readonly payload: Document) {}
}

export class CreateDocumentsuccess implements Action {
  readonly type = DocumentTypes.CreateDocumentsuccess;
  constructor(readonly payload: Document) {}
}

export class CreateDocumentError implements Action {
  readonly type = DocumentTypes.CreateDocumentError;
}
export class UpdateDocument implements Action {
  readonly type = DocumentTypes.UpdateDocument;
  constructor(readonly payload: Document) {}
}

export class UpdateDocumentsuccess implements Action {
  readonly type = DocumentTypes.UpdateDocumentsuccess;
  constructor(readonly payload: Document) {}
}

export class UpdateDocumentError implements Action {
  readonly type = DocumentTypes.UpdateDocumentError;
}

export class DeleteDocument implements Action {
  readonly type = DocumentTypes.DeleteDocument;
  constructor(readonly payload: string) {}
}

export class DeleteDocumentsuccess implements Action {
  readonly type = DocumentTypes.DeleteDocumentsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteDocumentError implements Action {
  readonly type = DocumentTypes.DeleteDocumentError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetDocument
  | GetDocumentSuccess
  | GetDocumentError
  | CreateDocument
  | CreateDocumentsuccess
  | CreateDocumentError
  | DeleteDocument
  | DeleteDocumentsuccess
  | DeleteDocumentError
  | UpdateDocument
  | UpdateDocumentsuccess
  | UpdateDocumentError
