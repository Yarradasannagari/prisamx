import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCheckListModelComponent } from './delete-checklist-model.component';

describe('DeleteCheckListModelComponent', () => {
  let component: DeleteCheckListModelComponent;
  let fixture: ComponentFixture<DeleteCheckListModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCheckListModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCheckListModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
