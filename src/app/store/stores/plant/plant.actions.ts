import { Action } from "@ngrx/store";
import { Plant } from "../../../services/plant/plant.service";

export enum PlantTypes {
  GetPlant = "[Plant] GetPlant",
  GetPlantSuccess = "[Plant] GetPlantSuccess",
  GetPlantError = "[Plant] GetPlantError",
  CreatePlant = "[Plant] CreatePlant",
  CreatePlantsuccess = "[Plant] CreatePlantsuccess",
  CreatePlantError = "[Plant] CreatePlantError",
  UpdatePlant = "[Plant] UpdatePlant",
  UpdatePlantsuccess = "[Plant] UpdatePlantsuccess",
  UpdatePlantError = "[Plant] UpdatePlantError",
  DeletePlant = "[Plant] DeletePlant",
  DeletePlantError = "[Plant] DeletePlantError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeletePlantsuccess = "[AclRule] DeletePlantsuccess",
}

export class GetPlant implements Action { 
  readonly type = PlantTypes.GetPlant;
}

export class GetPlantSuccess implements Action {
  readonly type = PlantTypes.GetPlantSuccess;
  constructor(readonly payload: Plant[]) {}
}
export class GetPlantError implements Action {
  readonly type = PlantTypes.GetPlantError;
  constructor(readonly payload: boolean) {}
}

export class CreatePlant implements Action {
  readonly type = PlantTypes.CreatePlant;
  constructor(readonly payload: Plant) {}
}

export class CreatePlantsuccess implements Action {
  readonly type = PlantTypes.CreatePlantsuccess;
  constructor(readonly payload: Plant) {}
}

export class CreatePlantError implements Action {
  readonly type = PlantTypes.CreatePlantError;
}
export class UpdatePlant implements Action {
  readonly type = PlantTypes.UpdatePlant;
  constructor(readonly payload: Plant) {}
}

export class UpdatePlantsuccess implements Action {
  readonly type = PlantTypes.UpdatePlantsuccess;
  constructor(readonly payload: Plant) {}
}

export class UpdatePlantError implements Action {
  readonly type = PlantTypes.UpdatePlantError;
}

export class DeletePlant implements Action {
  readonly type = PlantTypes.DeletePlant;
  constructor(readonly payload: string) {}
}

export class DeletePlantsuccess implements Action {
  readonly type = PlantTypes.DeletePlantsuccess;
  constructor(readonly payload: string) {}
}

export class DeletePlantError implements Action {
  readonly type = PlantTypes.DeletePlantError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetPlant
  | GetPlantSuccess
  | GetPlantError
  | CreatePlant
  | CreatePlantsuccess
  | CreatePlantError
  | DeletePlant
  | DeletePlantsuccess
  | DeletePlantError
  | UpdatePlant
  | UpdatePlantsuccess
  | UpdatePlantError
