import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Inspectionclass, InspectionclassService } from './inspection-class.service';

@Injectable({ providedIn: 'root' })
export class InspectionclassResolverService implements Resolve<Inspectionclass> {
  constructor(private inspectionclassService: InspectionclassService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Inspectionclass> | Promise<Inspectionclass> | Inspectionclass {
    const id = route.paramMap.get('id');
    return this.inspectionclassService.get(id);
  }
}
