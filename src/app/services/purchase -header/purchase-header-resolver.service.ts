import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { PurchaseHeader, PurchaseHeaderService } from './purchase-header.service';

@Injectable({ providedIn: 'root' })
export class PurchaseHeaderResolverService implements Resolve<PurchaseHeader> {
  constructor(private purchaseHeaderService: PurchaseHeaderService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<PurchaseHeader> | Promise<PurchaseHeader> | PurchaseHeader {
    const id = route.paramMap.get('id');
    return this.purchaseHeaderService.get(id);
  }
}
