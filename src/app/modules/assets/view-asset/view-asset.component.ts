import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Asset } from "src/app/services/assets/asset.service";
import { CreateAsset, UpdateAsset } from "src/app/store/stores/assets/asset.actions";
import { getAssetCreated, getAssetUpdated } from "src/app/store/stores/assets/asset.store";
import { State, Store } from "../../../store";

import { Location } from "src/app/services/locations/location.service";
import { GetLocation } from "src/app/store/stores/locations/location.actions";
import { getLocation } from "src/app/store/stores/locations/location.store";

import { InspectionPoint } from "src/app/services/inspection-point/inspection-point.service";
import { GetInspectionPoint } from "src/app/store/stores/inspection-point/inspection-point.actions";
import { getInspectionPoint } from "src/app/store/stores/inspection-point/inspection-point.store";

@Component({
  selector: "app-view-asset",
  templateUrl: "./view-asset.component.html",
  styleUrls: ["./view-asset.component.scss"],
})
export class ViewAssetComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  searchText:any;

  location: Location[];
  inspectionPoint: InspectionPoint[];
  formData = {
    id: null,
    locationId: "", 
    assetId: "",
    // name: "",
    // category: "",
    // type: "",
    // plant: "",
    status: "",
    startPoint: "",
    createdBy: "",
    latitude: "",
    longitude: "",
    addr1: "",
    addr2: "",
    addr3: "",
    landMark: "",
    zipcode: "",
    city: "",
    region: "",
    country: "",
    eqktx: "",
    typtx: "",
    eartx: "",
    swerk: "",
    // equnr: "",

    point: "",
    position: "",
    // name: "",
    objtyp: "",
    catergory: "",


    active: true,
  };
  asset: Asset;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(new GetLocation());
    this.store.select(getLocation).subscribe((location) => {
      this.location = location;
    });

    this.store.dispatch(new GetInspectionPoint());
    this.store.select(getInspectionPoint).subscribe((inspectionPoint) => {
      this.inspectionPoint = inspectionPoint;
    });
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.locationId = this.route.snapshot.data["report"].locationId;
        this.formData.assetId = this.route.snapshot.data["report"].assetId;
        // this.formData.name = this.route.snapshot.data["report"].name;
        // this.formData.category = this.route.snapshot.data["report"].category;
        // this.formData.type = this.route.snapshot.data["report"].type;
        // this.formData.plant = this.route.snapshot.data["report"].plant;
        this.formData.status = this.route.snapshot.data["report"].status;
        this.formData.startPoint = this.route.snapshot.data["report"].startPoint;
        this.formData.createdBy = this.route.snapshot.data["report"].createdBy;
        this.formData.latitude = this.route.snapshot.data["report"].latitude;
        this.formData.longitude = this.route.snapshot.data["report"].longitude;
        this.formData.addr1 = this.route.snapshot.data["report"].addr1;
        this.formData.addr2 = this.route.snapshot.data["report"].addr2;
        this.formData.addr3 = this.route.snapshot.data["report"].addr3;
        this.formData.landMark = this.route.snapshot.data["report"].landMark;
        this.formData.zipcode = this.route.snapshot.data["report"].zipcode;
        this.formData.city = this.route.snapshot.data["report"].city;
        this.formData.region = this.route.snapshot.data["report"].region;
        this.formData.country = this.route.snapshot.data["report"].country;
        this.formData.eqktx = this.route.snapshot.data["report"].eqktx;
        this.formData.typtx = this.route.snapshot.data["report"].typtx;
        this.formData.eartx = this.route.snapshot.data["report"].eartx;
        this.formData.swerk = this.route.snapshot.data["report"].swerk;
        // this.formData.equnr = this.route.snapshot.data["report"].equnr;

        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      locationId: [
        this.formData.locationId,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      assetId: [
        this.formData.assetId,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      // name: [
      //   this.formData.name,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // category: [
      //   this.formData.category,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // type: [
      //   this.formData.type,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // plant: [
      //   this.formData.plant,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      status: [
        this.formData.status,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      startPoint: [
        this.formData.startPoint,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      createdBy: [
        this.formData.createdBy,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      latitude: [
        this.formData.latitude,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      longitude: [
        this.formData.longitude,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        //  Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      addr1: [
        this.formData.addr1,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      addr2: [
        this.formData.addr2,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      addr3: [
        this.formData.addr3,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      landMark: [
        this.formData.landMark,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      zipcode: [
        this.formData.zipcode,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      city: [
        this.formData.city,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      region: [
        this.formData.region,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      country: [
        this.formData.country,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      eqktx: [
        this.formData.eqktx,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      typtx: [
        this.formData.typtx,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      eartx: [
        this.formData.eartx,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      swerk: [
        this.formData.swerk,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      // equnr: [
      //   this.formData.equnr,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],    
      active: [this.formData.active],
    });
  }
  submit() {
    const asset: Asset = {
      id: this.formData.id,
      locationId: this.formData.locationId,
      assetId: this.formData.assetId,
      // name: this.formData.name,
      // category: this.formData.category,
      // type: this.formData.type,
      // plant: this.formData.plant,
      status: this.formData.status,
      startPoint: this.formData.startPoint,
      createdBy: this.formData.createdBy,
      latitude: this.formData.latitude,
      longitude: this.formData.longitude,
      addr1: this.formData.addr1,
      addr2: this.formData.addr2,
      addr3: this.formData.addr3,
      landMark: this.formData.landMark,
      zipcode: this.formData.zipcode,
      city: this.formData.city,
      region: this.formData.region,
      country: this.formData.country,
      eqktx: this.formData.eqktx,
      typtx: this.formData.typtx,
      eartx: this.formData.eartx,
      swerk: this.formData.swerk,
      // equnr: this.formData.equnr,
      // active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateGroup(asset);
    } else {
      this.createGroup(asset);
    }
  }
  private createGroup(asset: Asset): void {
    this.store.dispatch(new CreateAsset(asset));
    this.store
      .select(getAssetCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateGroup(asset: Asset): void {
    this.store.dispatch(new UpdateAsset(asset));
    this.store
      .select(getAssetUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["aseets", "asset"]);
  }
}
