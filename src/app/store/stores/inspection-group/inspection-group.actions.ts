import { Action } from "@ngrx/store";
import { Inspectiongroup } from "../../../services/inspection-group/inspection-group.service"; 

export enum InspectiongroupTypes {
  GetInspectiongroup = "[Inspectiongroup] GetInspectiongroup",
  GetInspectiongroupSuccess = "[Inspectiongroup] GetInspectiongroupSuccess",
  GetInspectiongroupError = "[Inspectiongroup] GetInspectiongroupError",
  CreateInspectiongroup = "[Inspectiongroup] CreateInspectiongroup",
  CreateInspectiongroupsuccess = "[Inspectiongroup] CreateInspectiongroupsuccess",
  CreateInspectiongroupError = "[Inspectiongroup] CreateInspectiongroupError",
  UpdateInspectiongroup = "[Inspectiongroup] UpdateInspectiongroup",
  UpdateInspectiongroupsuccess = "[Inspectiongroup] UpdateInspectiongroupsuccess",
  UpdateInspectiongroupError = "[Inspectiongroup] UpdateInspectiongroupError",
  DeleteInspectiongroup = "[Inspectiongroup] DeleteInspectiongroup",
  DeleteInspectiongroupError = "[Inspectiongroup] DeleteInspectiongroupError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectiongroupsuccess = "[AclRule] DeleteInspectiongroupsuccess",
}

export class GetInspectiongroup implements Action { 
  readonly type = InspectiongroupTypes.GetInspectiongroup;
}

export class GetInspectiongroupSuccess implements Action {
  readonly type = InspectiongroupTypes.GetInspectiongroupSuccess;
  constructor(readonly payload: Inspectiongroup[]) {}
}
export class GetInspectiongroupError implements Action {
  readonly type = InspectiongroupTypes.GetInspectiongroupError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectiongroup implements Action {
  readonly type = InspectiongroupTypes.CreateInspectiongroup;
  constructor(readonly payload: Inspectiongroup) {}
}

export class CreateInspectiongroupsuccess implements Action {
  readonly type = InspectiongroupTypes.CreateInspectiongroupsuccess;
  constructor(readonly payload: Inspectiongroup) {}
}

export class CreateInspectiongroupError implements Action {
  readonly type = InspectiongroupTypes.CreateInspectiongroupError;
}
export class UpdateInspectiongroup implements Action {
  readonly type = InspectiongroupTypes.UpdateInspectiongroup;
  constructor(readonly payload: Inspectiongroup) {}
}

export class UpdateInspectiongroupsuccess implements Action {
  readonly type = InspectiongroupTypes.UpdateInspectiongroupsuccess;
  constructor(readonly payload: Inspectiongroup) {}
}

export class UpdateInspectiongroupError implements Action {
  readonly type = InspectiongroupTypes.UpdateInspectiongroupError;
}

export class DeleteInspectiongroup implements Action {
  readonly type = InspectiongroupTypes.DeleteInspectiongroup;
  constructor(readonly payload: string) {}
}

export class DeleteInspectiongroupsuccess implements Action {
  readonly type = InspectiongroupTypes.DeleteInspectiongroupsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectiongroupError implements Action {
  readonly type = InspectiongroupTypes.DeleteInspectiongroupError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectiongroup
  | GetInspectiongroupSuccess
  | GetInspectiongroupError
  | CreateInspectiongroup
  | CreateInspectiongroupsuccess
  | CreateInspectiongroupError
  | DeleteInspectiongroup
  | DeleteInspectiongroupsuccess
  | DeleteInspectiongroupError
  | UpdateInspectiongroup
  | UpdateInspectiongroupsuccess
  | UpdateInspectiongroupError
