import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Inspectioncategory, InspectioncategoryService } from "src/app/services/inspection-category/inspection-category.service";
import { GetInspectioncategory } from "src/app/store/stores/inspection-category/inspection-category.actions";
import {
  getInspectioncategoryError,
  getInspectioncategory,
  getInspectioncategoryLoading,
} from "src/app/store/stores/inspection-category/inspection-category.store";
import { State, Store } from "../../../store";
import { DeleteInspectionCategoryModelComponent } from "../delete-inspection-category-model/delete-inspection-category-model/delete-inspection-category-model.component";

@Component({
  selector: "app-inspection-category",
  templateUrl: "./inspection-category.component.html",
  styleUrls: ["./inspection-category.component.scss"],
})
export class InspectionCategoryComponent implements OnDestroy, OnInit {
  inspectioncategory$: Observable<Inspectioncategory[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  
  inspectioncategory: Inspectioncategory[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private roleService: InspectioncategoryService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectioncategoryLoading);
    this.store.dispatch(new GetInspectioncategory());
    this.inspectioncategory$ = this.store.select(getInspectioncategory);
    this.store.select(getInspectioncategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }

  reTry() {
    this.isLoading$ = this.store.select(getInspectioncategoryLoading);
    this.store.dispatch(new GetInspectioncategory());
    this.inspectioncategory$ = this.store.select(getInspectioncategory);
    this.store.select(getInspectioncategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionCategoryModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
