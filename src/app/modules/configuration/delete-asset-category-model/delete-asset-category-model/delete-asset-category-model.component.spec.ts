import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssetCategoryModelComponent } from './delete-asset-category-model.component';

describe('DeleteAssetCategoryModelComponent', () => {
  let component: DeleteAssetCategoryModelComponent;
  let fixture: ComponentFixture<DeleteAssetCategoryModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAssetCategoryModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssetCategoryModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
