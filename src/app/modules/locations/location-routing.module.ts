import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LocationsComponent } from "./location.component";

import { LocationComponent } from "./location/location.component";
import { LocationFormComponent } from "./location-form/location-form.component";
import { LocationResolverService } from "src/app/services/locations/location-resolver.service";
import { ViewLocationComponent } from "./view-location/view-location.component";

const routes: Routes = [
  {
    path: "",
    component: LocationsComponent,
    children: [
     
      {
        path: "location",
        component: LocationComponent,
      },
      {
        path: "add-location",
        component: LocationFormComponent,
      },
      {
        path: ":id/edit-location",
        component: LocationFormComponent,
        pathMatch: "full",
        resolve: {
          report: LocationResolverService,
        },
      },
      {
        path: ":id/view-location",
        component: ViewLocationComponent,
        pathMatch: "full",
        resolve: {
          report: LocationResolverService,
        },
      },
     
      { path: "", redirectTo: "location", pathMatch: "full" },
      { path: "**", redirectTo: "location", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationRoutingModule {}
