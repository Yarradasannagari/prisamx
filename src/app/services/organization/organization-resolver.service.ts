import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Organization, OrganizationService } from './organization.service';

@Injectable({ providedIn: 'root' })
export class OrganizationResolverService implements Resolve<Organization> {
  constructor(private organizationService: OrganizationService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Organization> | Promise<Organization> | Organization {
    const id = route.paramMap.get('id');
    return this.organizationService.get(id);
  }
}
