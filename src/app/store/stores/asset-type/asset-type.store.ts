import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Assettype } from "src/app/services/asset-type/asset-type.service";

import { State } from "../..";

import { AssettypeTypes, Union } from "./asset-type.actions";

export interface AssettypeState {
  loading: boolean;
  entities: Assettype[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: AssettypeState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function AssettypeReducer(
  state: AssettypeState = INITIAL_STATE,
  action: Union
): AssettypeState {
  switch (action.type) {
    case AssettypeTypes.GetAssettype:
      return {
        ...state,
        loading: true,
      };
    case AssettypeTypes.GetAssettypeSuccess:
      const assettype: Assettype[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: assettype,
      };
    case AssettypeTypes.GetAssettypeError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case AssettypeTypes.CreateAssettype:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case AssettypeTypes.CreateAssettypesuccess:
      const createdAssettype: Assettype = action.payload;
      const entitiesAppended = [...state.entities, createdAssettype];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case AssettypeTypes.UpdateAssettype:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case AssettypeTypes.UpdateAssettypesuccess:
      const updatedAssettype: Assettype = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedAssettype];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case AssettypeTypes.UpdateAssettypeError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case AssettypeTypes.CreateAssettypeError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case AssettypeTypes.DeleteAssettype:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case AssettypeTypes.DeleteAssettypesuccess:
      const afterDelete = state.entities.filter((assettype) => assettype.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case AssettypeTypes.DeleteAssettypeError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getAssettypeState = createFeatureSelector<State, AssettypeState>("assettype");

export const getAssettypeLoading = createSelector(
  getAssettypeState,
  (state) => state.loading
);

export const getAssettype = createSelector(getAssettypeState, (state) => state.entities);

export const getAssettypeCreating = createSelector(
  getAssettypeState,
  (state) => state.creating
);

export const getAssettypeCreated = createSelector(
  getAssettypeState,
  (state) => state.created
);
export const getAssettypeUpdating = createSelector(
  getAssettypeState,
  (state) => state.updating
);

export const getAssettypeUpdated = createSelector(
  getAssettypeState,
  (state) => state.updated
);
export const getAssettypeDeleting = createSelector(
  getAssettypeState,
  (state) => state.deleting
);

export const getAssettypeDeleted = createSelector(
  getAssettypeState,
  (state) => state.deleted
);

export const getAssettypeError = createSelector(
  getAssettypeState,
  (state) => state.error
);
