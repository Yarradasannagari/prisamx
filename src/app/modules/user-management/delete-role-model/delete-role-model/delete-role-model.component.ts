import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteRole } from "src/app/store/stores/roles/roles.actions";
import { getRoleDeleted } from "src/app/store/stores/roles/roles.store";
@Component({
  selector: "app-delete-role-model",
  templateUrl: "./delete-role-model.component.html",
  styleUrls: ["./delete-role-model.component.scss"],
})
export class DeleteRoleModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteRole() {
    this.isLoading = true;
    this.store.dispatch(new DeleteRole(this.id));
    this.store
      .select(getRoleDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
