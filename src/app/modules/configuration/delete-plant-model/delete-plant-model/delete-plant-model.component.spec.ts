import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePlantModelComponent } from './delete-plant-model.component';

describe('DeletePlantModelComponent', () => {
  let component: DeletePlantModelComponent;
  let fixture: ComponentFixture<DeletePlantModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletePlantModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePlantModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
