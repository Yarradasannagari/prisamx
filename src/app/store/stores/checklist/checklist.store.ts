import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Checklist } from "src/app/services/checklist/checklist.service";

import { State } from "../..";

import { CheckListTypes, Union } from "./checklist.actions";

export interface CheckListState {
  loading: boolean;
  entities: Checklist[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: CheckListState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function CheckListReducer(
  state: CheckListState = INITIAL_STATE,
  action: Union
): CheckListState {
  switch (action.type) {
    case CheckListTypes.GetCheckList:
      return {
        ...state,
        loading: true,
      };
    case CheckListTypes.GetCheckListSuccess:
      const checklist: Checklist[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: checklist,
      };
    case CheckListTypes.GetCheckListError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case CheckListTypes.CreateCheckList:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case CheckListTypes.CreateCheckListsuccess:
      const createdChecklist: Checklist = action.payload;
      const entitiesAppended = [...state.entities, createdChecklist];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case CheckListTypes.UpdateCheckList:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case CheckListTypes.UpdateCheckListsuccess:
      const updatedChecklist: Checklist = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedChecklist];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case CheckListTypes.UpdateCheckListError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case CheckListTypes.CreateCheckListError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case CheckListTypes.DeleteCheckList:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case CheckListTypes.DeleteCheckListsuccess:
      const afterDelete = state.entities.filter((checklist) => checklist.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case CheckListTypes.DeleteCheckListError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getCheckListState = createFeatureSelector<State, CheckListState>("checklist");

export const getCheckListLoading = createSelector(
  getCheckListState,
  (state) => state.loading
);

export const getCheckList = createSelector(getCheckListState, (state) => state.entities);

export const getCheckListCreating = createSelector(
  getCheckListState,
  (state) => state.creating
);

export const getCheckListCreated = createSelector(
  getCheckListState,
  (state) => state.created
);
export const getCheckListUpdating = createSelector(
  getCheckListState,
  (state) => state.updating
);

export const getCheckListUpdated = createSelector(
  getCheckListState,
  (state) => state.updated
);
export const getCheckListDeleting = createSelector(
  getCheckListState,
  (state) => state.deleting
);

export const getCheckListDeleted = createSelector(
  getCheckListState,
  (state) => state.deleted
);

export const getCheckListError = createSelector(
  getCheckListState,
  (state) => state.error
);
