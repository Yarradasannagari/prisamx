import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteOrganization} from "src/app/store/stores/organizations/organizations.actions";
import { getOrganizationDeleted } from "src/app/store/stores/organizations/organizations.store";
@Component({
  selector: "app-delete-organization-model",
  templateUrl: "./delete-organization-model.component.html",
  styleUrls: ["./delete-organization-model.component.scss"],
})
export class DeleteOrganizationModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteOrganization() {
    this.isLoading = true;
    this.store.dispatch(new DeleteOrganization(this.id));
    this.store
      .select(getOrganizationDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
