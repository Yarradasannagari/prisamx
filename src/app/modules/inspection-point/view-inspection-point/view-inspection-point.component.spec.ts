import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInspectionPointComponent } from './view-inspection-point.component';

describe('ViewInspectionPointComponent', () => {
  let component: ViewInspectionPointComponent;
  let fixture: ComponentFixture<ViewInspectionPointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInspectionPointComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInspectionPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
