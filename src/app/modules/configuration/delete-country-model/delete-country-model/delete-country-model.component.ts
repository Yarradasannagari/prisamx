import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteCountry} from "src/app/store/stores/country/country.actions";
import { getCountryDeleted } from "src/app/store/stores/country/country.store";
@Component({
  selector: "app-delete-country-model",
  templateUrl: "./delete-country-model.component.html",
  styleUrls: ["./delete-country-model.component.scss"],
})
export class DeleteCountryModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteCountry() {
    this.isLoading = true;
    this.store.dispatch(new DeleteCountry(this.id));
    this.store
      .select(getCountryDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
