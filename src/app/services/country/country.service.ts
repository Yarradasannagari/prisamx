import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Country {
  id:string;
  active: boolean;
  cname: string;
  countryId: string;
  sname: string;
  stateId: string; 
}


@Injectable({
  providedIn: 'root',
})
export class CountryService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Country> {
    return this.entitySvc.get(Url.country, id);
  }

  list(params?: Record<string, any>): Observable<Country[]> {
    return this.entitySvc.list(Url.country, params);
  }

  create(aclRule: Country): Observable<Country> {
    return this.entitySvc.create(Url.country, aclRule);
  }

  update(aclRule: Country): Observable<Country> {
    return this.entitySvc.update(Url.country, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.country, id);
  }
  
}
