import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { AssetcategoryService } from "../../../services/asset-category/asset-category.service"; 

import {
  GetAssetcategory,
  GetAssetcategorySuccess,
  GetAssetcategoryError,
  CreateAssetcategory,
  CreateAssetcategorysuccess,
  CreateAssetcategoryError,
  DeleteAssetcategory,
  DeleteAssetcategorysuccess,
  DeleteAssetcategoryError,
  AssetcategoryTypes,
  UpdateAssetcategory,
  UpdateAssetcategorysuccess,
  UpdateAssetcategoryError,
} from "./asset-category.actions";

@Injectable()
export class AssetcategoryEffects {
  constructor(
    private actions: Actions,
    private assetcategoryService: AssetcategoryService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getAssetcategory$: Observable<Action> = this.actions.pipe(
    ofType<GetAssetcategory>(AssetcategoryTypes.GetAssetcategory),
    exhaustMap(() =>
      from(this.assetcategoryService.list()).pipe(
        map((payload) => {
          return new GetAssetcategorySuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Asset Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetAssetcategoryError(true));
        })
      )
    )
  );

  @Effect()
  createAssetcategory$: Observable<Action> = this.actions.pipe(
    ofType<CreateAssetcategory>(AssetcategoryTypes.CreateAssetcategory),
    concatMap((action) =>
      from(this.assetcategoryService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset Category has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateAssetcategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Asset Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateAssetcategoryError());
        })
      )
    )
  );
  @Effect()
  updateAssetcategory$: Observable<Action> = this.actions.pipe(
    ofType<UpdateAssetcategory>(AssetcategoryTypes.UpdateAssetcategory),
    concatMap((action) =>
      from(this.assetcategoryService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset Category has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateAssetcategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Asset Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateAssetcategoryError());
        })
      )
    )
  );
  @Effect()
  deleteAssetcategory$: Observable<Action> = this.actions.pipe(
    ofType<DeleteAssetcategory>(AssetcategoryTypes.DeleteAssetcategory),
    concatMap((action) =>
      from(this.assetcategoryService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Asset Category has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteAssetcategorysuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Asset Category:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteAssetcategoryError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
