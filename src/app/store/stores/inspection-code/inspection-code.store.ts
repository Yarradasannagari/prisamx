import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Inspectioncode } from "src/app/services/inspection-code/inspection-code.service";

import { State } from "../..";

import { InspectioncodeTypes, Union } from "./inspection-code.actions";

export interface InspectioncodeState {
  loading: boolean;
  entities: Inspectioncode[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectioncodeState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectioncodeReducer(
  state: InspectioncodeState = INITIAL_STATE,
  action: Union
): InspectioncodeState {
  switch (action.type) {
    case InspectioncodeTypes.GetInspectioncode:
      return {
        ...state,
        loading: true,
      };
    case InspectioncodeTypes.GetInspectioncodeSuccess:
      const inspectioncode: Inspectioncode[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectioncode,
      };
    case InspectioncodeTypes.GetInspectioncodeError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectioncodeTypes.CreateInspectioncode:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectioncodeTypes.CreateInspectioncodesuccess:
      const createdInspectioncode: Inspectioncode = action.payload;
      const entitiesAppended = [...state.entities, createdInspectioncode];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectioncodeTypes.UpdateInspectioncode:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectioncodeTypes.UpdateInspectioncodesuccess:
      const updatedInspectioncode: Inspectioncode = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectioncode];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectioncodeTypes.UpdateInspectioncodeError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectioncodeTypes.CreateInspectioncodeError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectioncodeTypes.DeleteInspectioncode:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectioncodeTypes.DeleteInspectioncodesuccess:
      const afterDelete = state.entities.filter((inspectioncode) => inspectioncode.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectioncodeTypes.DeleteInspectioncodeError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectioncodeState = createFeatureSelector<State, InspectioncodeState>("inspectioncode");

export const getInspectioncodeLoading = createSelector(
  getInspectioncodeState,
  (state) => state.loading
);

export const getInspectioncode = createSelector(getInspectioncodeState, (state) => state.entities);

export const getInspectioncodeCreating = createSelector(
  getInspectioncodeState,
  (state) => state.creating
);

export const getInspectioncodeCreated = createSelector(
  getInspectioncodeState,
  (state) => state.created
);
export const getInspectioncodeUpdating = createSelector(
  getInspectioncodeState,
  (state) => state.updating
);

export const getInspectioncodeUpdated = createSelector(
  getInspectioncodeState,
  (state) => state.updated
);
export const getInspectioncodeDeleting = createSelector(
  getInspectioncodeState,
  (state) => state.deleting
);

export const getInspectioncodeDeleted = createSelector(
  getInspectioncodeState,
  (state) => state.deleted
);

export const getInspectioncodeError = createSelector(
  getInspectioncodeState,
  (state) => state.error
);
