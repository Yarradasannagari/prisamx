import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Country } from "src/app/services/country/country.service";

import { State } from "../..";

import { CountryTypes, Union } from "./country.actions";

export interface CountryState {
  loading: boolean;
  entities: Country[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: CountryState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function CountryReducer(
  state: CountryState = INITIAL_STATE,
  action: Union
): CountryState {
  switch (action.type) {
    case CountryTypes.GetCountry:
      return {
        ...state,
        loading: true,
      };
    case CountryTypes.GetCountrySuccess:
      const Country: Country[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: Country,
      };
      case CountryTypes.GetCountryError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case CountryTypes.CreateCountry:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case CountryTypes.CreateCountrysuccess:
      const createdCountry: Country = action.payload;
      const entitiesAppended = [...state.entities, createdCountry];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
    case CountryTypes.UpdateCountry:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case CountryTypes.UpdateCountrysuccess:
      const updatedCountry: Country = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedCountry];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
    case CountryTypes.UpdateCountryError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case CountryTypes.CreateCountryError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case CountryTypes.DeleteCountry:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case CountryTypes.DeleteCountrysuccess:
      const afterDelete = state.entities.filter(
        (country) => country.id !== action.payload
      );
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities: afterDelete,
      };

    case CountryTypes.DeleteCountryError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getCountryState = createFeatureSelector<State, CountryState>("country");

export const getCountryLoading = createSelector(
  getCountryState,
  (state) => state.loading
);

export const getCountry = createSelector(
  getCountryState,
  (state) => state.entities
);

export const getCountryCreating = createSelector(
  getCountryState,
  (state) => state.creating
);

export const getCountryCreated = createSelector(
  getCountryState,
  (state) => state.created
);
export const getCountryUpdating = createSelector(
  getCountryState,
  (state) => state.updating
);

export const getCountryUpdated = createSelector(
  getCountryState,
  (state) => state.updated
);
export const getCountryDeleting = createSelector(
  getCountryState,
  (state) => state.deleting
);

export const getCountryDeleted = createSelector(
  getCountryState,
  (state) => state.deleted
);

export const getCountryError = createSelector(
  getCountryState,
  (state) => state.error
);
