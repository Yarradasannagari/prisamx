import { Action } from "@ngrx/store";
import { Checklist } from "../../../services/checklist/checklist.service";

export enum CheckListTypes {
  GetCheckList = "[Checklist] GetCheckList",
  GetCheckListSuccess = "[Checklist] GetCheckListSuccess",
  GetCheckListError = "[Checklist] GetCheckListError",
  CreateCheckList = "[Checklist] CreateCheckList",
  CreateCheckListsuccess = "[Checklist] CreateCheckListsuccess",
  CreateCheckListError = "[Checklist] CreateCheckListError",
  UpdateCheckList = "[Checklist] UpdateCheckList",
  UpdateCheckListsuccess = "[Checklist] UpdateCheckListsuccess",
  UpdateCheckListError = "[Checklist] UpdateCheckListError",
  DeleteCheckList = "[Checklist] DeleteCheckList",
  DeleteCheckListError = "[Checklist] DeleteCheckListError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteCheckListsuccess = "[AclRule] DeleteCheckListsuccess",
}

export class GetCheckList implements Action { 
  readonly type = CheckListTypes.GetCheckList;
}

export class GetCheckListSuccess implements Action {
  readonly type = CheckListTypes.GetCheckListSuccess;
  constructor(readonly payload: Checklist[]) {}
}
export class GetCheckListError implements Action {
  readonly type = CheckListTypes.GetCheckListError;
  constructor(readonly payload: boolean) {}
}

export class CreateCheckList implements Action {
  readonly type = CheckListTypes.CreateCheckList;
  constructor(readonly payload: Checklist) {}
}

export class CreateCheckListsuccess implements Action {
  readonly type = CheckListTypes.CreateCheckListsuccess;
  constructor(readonly payload: Checklist) {}
}

export class CreateCheckListError implements Action {
  readonly type = CheckListTypes.CreateCheckListError;
}
export class UpdateCheckList implements Action {
  readonly type = CheckListTypes.UpdateCheckList;
  constructor(readonly payload: Checklist) {}
}

export class UpdateCheckListsuccess implements Action {
  readonly type = CheckListTypes.UpdateCheckListsuccess;
  constructor(readonly payload: Checklist) {}
}

export class UpdateCheckListError implements Action {
  readonly type = CheckListTypes.UpdateCheckListError;
}

export class DeleteCheckList implements Action {
  readonly type = CheckListTypes.DeleteCheckList;
  constructor(readonly payload: string) {}
}

export class DeleteCheckListsuccess implements Action {
  readonly type = CheckListTypes.DeleteCheckListsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteCheckListError implements Action {
  readonly type = CheckListTypes.DeleteCheckListError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetCheckList
  | GetCheckListSuccess
  | GetCheckListError
  | CreateCheckList
  | CreateCheckListsuccess
  | CreateCheckListError
  | DeleteCheckList
  | DeleteCheckListsuccess
  | DeleteCheckListError
  | UpdateCheckList
  | UpdateCheckListsuccess
  | UpdateCheckListError
