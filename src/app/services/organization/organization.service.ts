import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Organization {
  id:string;
  active: boolean;
  createdBy?: string;
  createdDate?: string;
  created_time?: string;
  cahngedBy?: number;
  changedDate?: string;
  changed_time?: string;
  country: string;
  currency: string;
  name: any;
  ordId: any;
  // orgid: any;
 
}


@Injectable({
  providedIn: 'root',
})
export class OrganizationService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Organization> {
    return this.entitySvc.get(Url.organizations, id);
  }

  list(params?: Record<string, any>): Observable<Organization[]> {
    return this.entitySvc.list(Url.organizations, params);
  }

  create(aclRule: Organization): Observable<Organization> {
    return this.entitySvc.create(Url.organizations, aclRule);
  }

  update(aclRule: Organization): Observable<Organization> {
    return this.entitySvc.update(Url.organizations, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.organizations, id);
  }
  
}
