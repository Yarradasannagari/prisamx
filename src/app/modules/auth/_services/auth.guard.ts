import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthService } from '..';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = localStorage.getItem('authentication');
    console.log("Cuurent User"+currentUser)
    
      // logged in so return true
      if(currentUser){
        return true;
      }else{
        this.authService.logout();
        return false;
      }
      

    

    // not logged in so redirect to login page with the return url
     
  }
}
