import { Action } from "@ngrx/store";
import { Purchase } from "../../../services/purchase/purchase.service";

export enum PurchaseTypes {
  GetPurchase = "[Purchase] GetPurchase",
  GetPurchaseSuccess = "[Purchase] GetPurchaseSuccess",
  GetPurchaseError = "[Purchase] GetPurchaseError",
  CreatePurchase = "[Purchase] CreatePurchase",
  CreatePurchasesuccess = "[Purchase] CreatePurchasesuccess",
  CreatePurchaseError = "[Purchase] CreatePurchaseError",
  UpdatePurchase = "[Purchase] UpdatePurchase",
  UpdatePurchasesuccess = "[Purchase] UpdatePurchasesuccess",
  UpdatePurchaseError = "[Purchase] UpdatePurchaseError",
  DeletePurchase = "[Purchase] DeletePurchase",
  DeletePurchaseError = "[Purchase] DeletePurchaseError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeletePurchasesuccess = "[AclRule] DeletePurchasesuccess",
}

export class GetPurchase implements Action { 
  readonly type = PurchaseTypes.GetPurchase;
}

export class GetPurchaseSuccess implements Action {
  readonly type = PurchaseTypes.GetPurchaseSuccess;
  constructor(readonly payload: Purchase[]) {}
}
export class GetPurchaseError implements Action {
  readonly type = PurchaseTypes.GetPurchaseError;
  constructor(readonly payload: boolean) {}
}

export class CreatePurchase implements Action {
  readonly type = PurchaseTypes.CreatePurchase;
  constructor(readonly payload: Purchase) {}
}

export class CreatePurchasesuccess implements Action {
  readonly type = PurchaseTypes.CreatePurchasesuccess;
  constructor(readonly payload: Purchase) {}
}

export class CreatePurchaseError implements Action {
  readonly type = PurchaseTypes.CreatePurchaseError;
}
export class UpdatePurchase implements Action {
  readonly type = PurchaseTypes.UpdatePurchase;
  constructor(readonly payload: Purchase) {}
}

export class UpdatePurchasesuccess implements Action {
  readonly type = PurchaseTypes.UpdatePurchasesuccess;
  constructor(readonly payload: Purchase) {}
}

export class UpdatePurchaseError implements Action {
  readonly type = PurchaseTypes.UpdatePurchaseError;
}

export class DeletePurchase implements Action {
  readonly type = PurchaseTypes.DeletePurchase;
  constructor(readonly payload: string) {}
}

export class DeletePurchasesuccess implements Action {
  readonly type = PurchaseTypes.DeletePurchasesuccess;
  constructor(readonly payload: string) {}
}

export class DeletePurchaseError implements Action {
  readonly type = PurchaseTypes.DeletePurchaseError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetPurchase
  | GetPurchaseSuccess
  | GetPurchaseError
  | CreatePurchase
  | CreatePurchasesuccess
  | CreatePurchaseError
  | DeletePurchase
  | DeletePurchasesuccess
  | DeletePurchaseError
  | UpdatePurchase
  | UpdatePurchasesuccess
  | UpdatePurchaseError
