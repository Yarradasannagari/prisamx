import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Classification } from "src/app/services/classification/classification.service";

import { State } from "../..";

import { ClassificationTypes, Union } from "./classification.actions";

export interface ClassificationState {
  loading: boolean;
  entities: Classification[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: ClassificationState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function ClassificationReducer(
  state: ClassificationState = INITIAL_STATE,
  action: Union
): ClassificationState {
  switch (action.type) {
    case ClassificationTypes.GetClassification:
      return {
        ...state,
        loading: true,
      };
    case ClassificationTypes.GetClassificationSuccess:
      const classification: Classification[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: classification,
      };
    case ClassificationTypes.GetClassificationError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case ClassificationTypes.CreateClassification:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case ClassificationTypes.CreateClassificationsuccess:
      const createdClassification: Classification = action.payload;
      const entitiesAppended = [...state.entities, createdClassification];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case ClassificationTypes.UpdateClassification:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case ClassificationTypes.UpdateClassificationsuccess:
      const updatedClassification: Classification = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedClassification];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case ClassificationTypes.UpdateClassificationError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case ClassificationTypes.CreateClassificationError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case ClassificationTypes.DeleteClassification:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case ClassificationTypes.DeleteClassificationsuccess:
      const afterDelete = state.entities.filter((classification) => classification.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case ClassificationTypes.DeleteClassificationError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getClassificationState = createFeatureSelector<State, ClassificationState>("classification");

export const getClassificationLoading = createSelector(
  getClassificationState,
  (state) => state.loading
);

export const getClassification = createSelector(getClassificationState, (state) => state.entities);

export const getClassificationCreating = createSelector(
  getClassificationState,
  (state) => state.creating
);

export const getClassificationCreated = createSelector(
  getClassificationState,
  (state) => state.created
);
export const getClassificationUpdating = createSelector(
  getClassificationState,
  (state) => state.updating
);

export const getClassificationUpdated = createSelector(
  getClassificationState,
  (state) => state.updated
);
export const getClassificationDeleting = createSelector(
  getClassificationState,
  (state) => state.deleting
);

export const getClassificationDeleted = createSelector(
  getClassificationState,
  (state) => state.deleted
);

export const getClassificationError = createSelector(
  getClassificationState,
  (state) => state.error
);
