import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Purchase, PurchaseService } from './purchase.service';

@Injectable({ providedIn: 'root' })
export class PurchaseResolverService implements Resolve<Purchase> {
  constructor(private purchaseService: PurchaseService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Purchase> | Promise<Purchase> | Purchase {
    const id = route.paramMap.get('id');
    return this.purchaseService.get(id);
  }
}
