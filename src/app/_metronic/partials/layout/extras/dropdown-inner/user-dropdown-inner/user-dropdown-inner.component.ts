import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LayoutService } from '../../../../../core';
import { UserModel } from '../../../../../../modules/auth/_models/user.model';
import { AuthService } from '../../../../../../modules/auth/_services/auth.service';
import { State, Store } from "../../../../../../store";
import { User } from "src/app/services/users/user.service";
import { GetUsers } from "src/app/store/stores/users/users.actions";
import { getUsers } from "src/app/store/stores/users/users.store";
@Component({
  selector: 'app-user-dropdown-inner',
  templateUrl: './user-dropdown-inner.component.html',
  styleUrls: ['./user-dropdown-inner.component.scss'],
})
export class UserDropdownInnerComponent implements OnInit {
  extrasUserDropdownStyle: 'light' | 'dark' = 'light';
  user$: Observable<User>;
  user: User[];


  constructor(private layout: LayoutService, private auth: AuthService,private store: Store<State>) {
    this.store.dispatch(new GetUsers());
    this.store.select(getUsers).subscribe((user) => {
      this.user = user;
    });
  }

  ngOnInit(): void {
    this.extrasUserDropdownStyle = this.layout.getProp(
      'extras.user.dropdown.style'
    );
    this.user$ = this.auth.currentUserSubject.asObservable();
  }

  logout() {
    this.auth.logout();
    document.location.reload();
  } 
}
