import { RouterReducerState, routerReducer } from "@ngrx/router-store";
import { ActionReducerMap, MetaReducer } from "@ngrx/store";

import { environment } from "../../environments/environment";

import * as role from "./stores/roles";
import * as user from "./stores/users";
import * as group from "./stores/groups";
import * as organization from "./stores/organizations";
import * as plant from "./stores/plant";
import * as locationcategory from "./stores/location-category";
import * as locationtype from "./stores/location-type";
import * as assetcategory from "./stores/asset-category";
import * as assettype from "./stores/asset-type";
import * as inspectioncategory from "./stores/inspection-category";
import * as inspectiontype from "./stores/inspection-type";
import * as inspectioncode from "./stores/inspection-code";
import * as inspectionclass from "./stores/inspection-class";
import * as inspectiongroup from "./stores/inspection-group";
import * as country from "./stores/country";
import * as unit from "./stores/units";
import * as location from "./stores/locations";
import * as asset from "./stores/assets";
import * as checklist from "./stores/checklist";
import * as otfheader from "./stores/purchase -header";
import * as otfitems from "./stores/purchase";
import * as inspectionpoint from "./stores/inspection-point";
import * as partner from "./stores/partner";
import * as classification from "./stores/classification";
import * as document from "./stores/document";

/**
 * Import Stores
 */

export { Store, StoreModule } from "@ngrx/store";

/**
 * State for each store
 */
export interface State {
  role: role.RoleState;
  user: user.UserState;
  group: group.GroupState;
  organization: organization.OrganizationState;
  plant: plant.PlantState;
  locationcategory: locationcategory.LocationcategoryState;
  locationtype: locationtype.LocationtypeState;
  assetcategory: assetcategory.AssetcategoryState;
  assettype: assettype.AssettypeState;
  inspectioncategory: inspectioncategory.InspectioncategoryState;
  inspectiontype: inspectiontype.InspectiontypeState;
  inspectioncode: inspectioncode.InspectioncodeState;
  inspectionclass: inspectionclass.InspectionclassState;
  inspectiongroup: inspectiongroup.InspectiongroupState;
  country: country.CountryState;
  unit: unit.UnitState;
  location: location.LocationState;
  asset: asset.AssetState;
  otfitems: otfitems.PurchaseState;
  checklist: checklist.CheckListState;
  otfheader: otfheader.PurchaseHeaderState;
  inspectionpoint: inspectionpoint.InspectionPointState;
  partner: partner.PartnerState;
  classification: classification.ClassificationState;
  document: document.DocumentState;

}

export function getInitialState() {
  return {
    role: role.INITIAL_STATE,
    user: user.INITIAL_STATE,
    group: group.INITIAL_STATE,
    organization: organization.INITIAL_STATE,
    plant: plant.INITIAL_STATE,
    locationcategory: locationcategory.INITIAL_STATE,
    locationtype: locationtype.INITIAL_STATE,
    assetcategory: assetcategory.INITIAL_STATE,
    assettype: assettype.INITIAL_STATE,
    inspectioncategory: inspectioncategory.INITIAL_STATE,
    inspectiontype: inspectiontype.INITIAL_STATE,
    inspectioncode: inspectioncode.INITIAL_STATE,
    inspectionclass: inspectionclass.INITIAL_STATE,
    inspectiongroup: inspectiongroup.INITIAL_STATE,
    country: country.INITIAL_STATE,
    unit: unit.INITIAL_STATE,
    location: location.INITIAL_STATE,
    asset: asset.INITIAL_STATE,
    otfitems: otfitems.INITIAL_STATE,
    checklist: checklist.INITIAL_STATE,
    otfheader: otfheader.INITIAL_STATE,
    inspectionpoint: inspectionpoint.INITIAL_STATE,
    partner: partner.INITIAL_STATE,
    classification: classification.INITIAL_STATE,
    document: document.INITIAL_STATE
  } as State;
}

/**
 * Effects for each store
 */
export const effects = [role.RoleEffects, user.UserEffects, group.GroupEffects, organization.OrganizationEffects,
   plant.PlantEffects,locationcategory.LocationcategoryEffects, locationtype.LocationtypeEffects, assetcategory.AssetcategoryEffects,
   assettype.AssettypeEffects, inspectioncategory.InspectioncategoryEffects, inspectiontype.InspectiontypeEffects,
   inspectioncode.InspectioncodeEffects,inspectionclass.InspectionclassEffects, inspectiongroup.InspectiongroupEffects,
    country.CountryEffects, unit.UnitEffects,location.LocationEffects,asset.AssetEffects,otfitems.PurchaseEffects,checklist.CheckListEffects,
    otfheader.PurchaseHeaderEffects, inspectionpoint.InspectionPointEffects, partner.PartnerEffects, classification.ClassificationEffects,
    document.DocumentEffects
    ];

/**
 * Reducer for each store
 */
export const reducers: ActionReducerMap<State> = {
  role: role.RoleReducer,
  user: user.UserReducer,
  group: group.GroupReducer,
  organization: organization.OrganizationReducer,
  plant: plant.PlantReducer,
  locationcategory: locationcategory.LocationcategoryReducer,
  locationtype: locationtype.LocationtypeReducer,
  assetcategory: assetcategory.AssetcategoryReducer,
  assettype: assettype.AssettypeReducer,
  inspectioncategory: inspectioncategory.InspectioncategoryReducer,
  inspectiontype: inspectiontype.InspectiontypeReducer,
  inspectioncode: inspectioncode.InspectioncodeReducer,
  inspectionclass: inspectionclass.InspectionclassReducer,
  inspectiongroup: inspectiongroup.InspectiongroupReducer,
  country: country.CountryReducer,
  unit: unit.UnitReducer,
  location: location.LocationReducer,
  asset: asset.AssetReducer,
  otfitems: otfitems.PurchaseReducer,
  checklist: checklist.CheckListReducer,
  otfheader: otfheader.PurchaseHeaderReducer,
  inspectionpoint: inspectionpoint.InspectionPointReducer,
  partner: partner.PartnerReducer,
  classification: classification.ClassificationReducer,
  document: document.DocumentReducer

};

/**
 * MetaReducer for each store
 */
export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
