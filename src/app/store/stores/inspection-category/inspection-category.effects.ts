import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { InspectioncategoryService } from "../../../services/inspection-category/inspection-category.service"; 

import {
  GetInspectioncategory,
  GetInspectioncategorySuccess,
  GetInspectioncategoryError,
  CreateInspectioncategory,
  CreateInspectioncategorysuccess,
  CreateInspectioncategoryError,
  DeleteInspectioncategory,
  DeleteInspectioncategorysuccess,
  DeleteInspectioncategoryError,
  InspectioncategoryTypes,
  UpdateInspectioncategory,
  UpdateInspectioncategorysuccess,
  UpdateInspectioncategoryError,
} from "./inspection-category.actions";

@Injectable()
export class InspectioncategoryEffects {
  constructor(
    private actions: Actions,
    private inspectioncategoryService: InspectioncategoryService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectioncategory$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectioncategory>(InspectioncategoryTypes.GetInspectioncategory),
    exhaustMap(() =>
      from(this.inspectioncategoryService.list()).pipe(
        map((payload) => {
          return new GetInspectioncategorySuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectioncategoryError(true));
        })
      )
    )
  );

  @Effect()
  createInspectioncategory$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectioncategory>(InspectioncategoryTypes.CreateInspectioncategory),
    concatMap((action) =>
      from(this.inspectioncategoryService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Category has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectioncategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectioncategoryError());
        })
      )
    )
  );
  @Effect()
  updateInspectioncategory$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectioncategory>(InspectioncategoryTypes.UpdateInspectioncategory),
    concatMap((action) =>
      from(this.inspectioncategoryService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Category has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectioncategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectioncategoryError());
        })
      )
    )
  );
  @Effect()
  deleteInspectioncategory$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectioncategory>(InspectioncategoryTypes.DeleteInspectioncategory),
    concatMap((action) =>
      from(this.inspectioncategoryService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Category has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectioncategorysuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Category:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectioncategoryError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
