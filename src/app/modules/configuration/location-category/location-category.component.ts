import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Locationcategory, LocationcategoryService } from "src/app/services/location-category/location-category.service";
import { GetLocationcategory } from "src/app/store/stores/location-category/location-category.actions";
import { State, Store } from "../../../store";
import {
  getLocationcategoryError,
  getLocationcategory,
  getLocationcategoryLoading,
} from "src/app/store/stores/location-category/location-category.store";
import { DeleteLocationCategoryModelComponent } from "../delete-location-category-model/delete-location-category-model/delete-location-category-model.component";


@Component({
  selector: "app-location-category",
  templateUrl: "./location-category.component.html",
  styleUrls: ["./location-category.component.scss"],
})
export class LocationCategoryComponent implements OnDestroy, OnInit {
  locationcategory$: Observable<Locationcategory[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private locationcategoryService: LocationcategoryService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getLocationcategoryLoading);
    this.store.dispatch(new GetLocationcategory());
    this.locationcategory$ = this.store.select(getLocationcategory);
    this.store.select(getLocationcategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 

  reTry() {
    this.isLoading$ = this.store.select(getLocationcategoryLoading);
    this.store.dispatch(new GetLocationcategory());
    this.locationcategory$ = this.store.select(getLocationcategory);
    this.store.select(getLocationcategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteLocationCategoryModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
