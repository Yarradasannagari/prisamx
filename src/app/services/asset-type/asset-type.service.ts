import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Assettype {
  id:string;
  active: boolean;
  name: string;
  type: string;
}


@Injectable({
  providedIn: 'root',
})
export class AssettypeService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Assettype> {
    return this.entitySvc.get(Url.assettype, id);
  }

  list(params?: Record<string, any>): Observable<Assettype[]> {
    return this.entitySvc.list(Url.assettype, params);
  }

  create(aclRule: Assettype): Observable<Assettype> {
    return this.entitySvc.create(Url.assettype, aclRule);
  }

  update(aclRule: Assettype): Observable<Assettype> {
    return this.entitySvc.update(Url.assettype, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.assettype, id);
  }
  
}
