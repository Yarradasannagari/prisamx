import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Organization, OrganizationService } from "src/app/services/organization/organization.service";
import { GetOrganizations } from "src/app/store/stores/organizations/organizations.actions";
import {
  getOrganizationError,
  getOrganizations,
  getOrganizationsLoading,
} from "src/app/store/stores/organizations/organizations.store";
import { State, Store } from "../../../store";
import { DeleteOrganizationModelComponent } from "../delete-organization-model/delete-organization-model/delete-organization-model.component";

@Component({
  selector: "app-organization",
  templateUrl: "./organization.component.html",
  styleUrls: ["./organization.component.scss"],
})
export class OrganizationComponent implements OnDestroy, OnInit { 
  organizations$: Observable<Organization[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  // organizations: Organization[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private organizationService: OrganizationService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getOrganizationsLoading);
    this.store.dispatch(new GetOrganizations());
    this.organizations$ = this.store.select(getOrganizations);
    this.store.select(getOrganizationError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 
  
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getOrganizationsLoading);
    this.store.dispatch(new GetOrganizations());
    this.organizations$ = this.store.select(getOrganizations);
    this.store.select(getOrganizationError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteOrganizationModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
