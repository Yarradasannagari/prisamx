import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Classification {
  // active: boolean;

  assetId: string;
  atbez: string;
  atnam: string;
  atzis: string;
  category: string;
  changed_by: string;
  changed_date: string;
  changed_time: string;
  clas: string;
  classId: string;
  createdBy: string;
  createdDate: string;
  created_time: string;
  equnr: string;
  id: string;
  kltxt: string;
  locationId: string;
  meins: string;
  name: string;
  ob_type: string;
  obj_nr: string;
  orgId: string;
  parent: string;
  plant: string;
  pplant: string;
  tplnr: string;
  type: string;
  wkctr: string;
}


@Injectable({
  providedIn: 'root',
})
export class ClassificationService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Classification> {
    return this.entitySvc.get(Url.classification, id);
  }

  list(params?: Record<string, any>): Observable<Classification[]> {
    return this.entitySvc.list(Url.classification, params);
  }

  create(aclRule: Classification): Observable<Classification> {
    return this.entitySvc.create(Url.classification, aclRule); 
  }

  update(aclRule: Classification): Observable<Classification> {
    return this.entitySvc.update(Url.classification, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.classification, id);
  }
  
}
