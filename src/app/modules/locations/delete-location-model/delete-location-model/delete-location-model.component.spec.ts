import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteLocationModelComponent } from './delete-location-model.component';

describe('DeleteLocationModelComponent', () => {
  let component: DeleteLocationModelComponent;
  let fixture: ComponentFixture<DeleteLocationModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteLocationModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteLocationModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
