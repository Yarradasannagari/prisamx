import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Plant } from "src/app/services/plant/plant.service";

import { State } from "../..";

import { PlantTypes, Union } from "./plant.actions";

export interface PlantState {
  loading: boolean;
  entities: Plant[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: PlantState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function PlantReducer(
  state: PlantState = INITIAL_STATE,
  action: Union
): PlantState {
  switch (action.type) {
    case PlantTypes.GetPlant:
      return {
        ...state,
        loading: true,
      };
    case PlantTypes.GetPlantSuccess:
      const plant: Plant[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: plant,
      };
    case PlantTypes.GetPlantError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case PlantTypes.CreatePlant:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case PlantTypes.CreatePlantsuccess:
      const createdPlant: Plant = action.payload;
      const entitiesAppended = [...state.entities, createdPlant];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case PlantTypes.UpdatePlant:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case PlantTypes.UpdatePlantsuccess:
      const updatedPlant: Plant = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedPlant];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case PlantTypes.UpdatePlantError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case PlantTypes.CreatePlantError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case PlantTypes.DeletePlant:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case PlantTypes.DeletePlantsuccess:
      const afterDelete = state.entities.filter((plant) => plant.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case PlantTypes.DeletePlantError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getPlantState = createFeatureSelector<State, PlantState>("plant");

export const getPlantLoading = createSelector(
  getPlantState,
  (state) => state.loading
);

export const getPlant = createSelector(getPlantState, (state) => state.entities);

export const getPlantCreating = createSelector(
  getPlantState,
  (state) => state.creating
);

export const getPlantCreated = createSelector(
  getPlantState,
  (state) => state.created
);
export const getPlantUpdating = createSelector(
  getPlantState,
  (state) => state.updating
);

export const getPlantUpdated = createSelector(
  getPlantState,
  (state) => state.updated
);
export const getPlantDeleting = createSelector(
  getPlantState,
  (state) => state.deleting
);

export const getPlantDeleted = createSelector(
  getPlantState,
  (state) => state.deleted
);

export const getPlantError = createSelector(
  getPlantState,
  (state) => state.error
);
