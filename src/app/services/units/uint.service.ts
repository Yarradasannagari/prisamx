import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Unit {
  id:string;
  active: boolean;
  name: string;
  uomId: string;
}


@Injectable({
  providedIn: 'root',
})
export class UnitService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Unit> {
    return this.entitySvc.get(Url.unit, id);
  }

  list(params?: Record<string, any>): Observable<Unit[]> {
    return this.entitySvc.list(Url.unit, params);
  }

  create(aclRule: Unit): Observable<Unit> {
    return this.entitySvc.create(Url.unit, aclRule);
  }

  update(aclRule: Unit): Observable<Unit> {
    return this.entitySvc.update(Url.unit, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.unit, id);
  }
  
}
