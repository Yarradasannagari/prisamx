import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Asset {
  // active: boolean;


  // abckz: string;
  // aclass: string;
  // action: string;
  addr1: string;
  addr2: string;
  addr3: string;
  // aedat: string;
  // aktiv: string;
  // anlar: string;
  // anlkl: string;
  // anlnr: string;
  // anltp: string;
  // anlun: string;
  // arbpl: string;
  // arbplname: string;
  assetId: string;
  // baujj: string;
  // baumm: string;
  // baummbaumm: string;
  // beber: string;
  // bstdt: string;
  // bukrs: string;
  // category: string;
  // changedBy: string;
  // changedDate: string;
  city: string;
  // classId: string;
  // classname: string;
  country: string;
  createdBy: string;
  // createdDate: string;
  // deakt: string;
  eartx: string;
  // emailid: string;
  // endPoint: string;
  // eqart: string;
  // eqfnr: string;
  eqktx: string;
  // eqtyp: string;
  // equnr: string;
  // gegst: string;
  // gplab: string;
  // gsber: string;
  // gtext: string;
  // gwldt: string;
  // gwlen: string;
  // haschild: string;
  // hasparent: string;
  // heqnr: string;
  // hequi: string;
  // hequiname: string;
  // herld: string;
  // herst: string;
  // housenum: string;
  id: string;
  // inbdt: string;
  // ingrp: string;
  // ingrpname: string;
  // inken: string;
  // invnr: string;
  // invzu: string;
  // istat: string;
  // ivdat: string;
  // iwerk: string;
  // iwerkname: string;
  // kostl: string;
  // ktext: string;
  landMark: string;
  latitude: string;
  // length: string;
  // lengthUom: string;
  // liefe: string;
  // lifnr: string;
  // linear_length: string;
  // linear_unit: string;
  locationId: string;
  longitude: string;
  // mapar: string;
  // matnr: string;
  // meins: string;
  // menge: string;
  // mobno: string;
  // msgrp: string;
  // name: string;
  // name1: string;
  // name2: string;
  // orgId: string;
  // parent: string;
  // parnr: string;
  // parnrname: string;
  // pgroup: string;
  // plant: string;
  // pltxt: string;
  // pplant: string;
  // rbnr: string;
  // rbnrx: string;
  region: string;
  // root: string;
  // serge: string;
  // sernr: string;
  startPoint: string;
  status: string;
  // statusName: string;
  // stort: string;
  // street: string;
  // strno: string;
  // submt: string;
  swerk: string;
  // swerkname: string;
  // telno: string;
  // tplnr: string;
  // txk50: string;
  // txt50: string;
  // typbz: string;
  // type: string;
  typtx: string;
  // userId: string;
  // usr01: string;
  // usr02: string;
  // usr03: string;
  // usr04: string;
  // usr05: string;
  // vrsbg: string;
  // vrsma: string;
  // vrstx: string;
  // vsart: string;
  // vsstx: string;
  // vstar: string;
  // vsztx: string;
  // waers: string;
  // wkctr: string;
  // wrtma: string;
  zipcode: string;

}


@Injectable({
  providedIn: 'root',
})
export class AssetService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Asset> {
    return this.entitySvc.get(Url.assets, id);
  }

  list(params?: Record<string, any>): Observable<Asset[]> {
    return this.entitySvc.list(Url.assets, params);
  }

  create(aclRule: Asset): Observable<Asset> {
    return this.entitySvc.create(Url.assets, aclRule); 
  }

  update(aclRule: Asset): Observable<Asset> {
    return this.entitySvc.update(Url.assets, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.assets, id);
  }
  
}
