import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { PurchaseHeaderService } from "../../../services/purchase -header/purchase-header.service"; 

import {
  GetPurchaseHeader,
  GetPurchaseHeaderSuccess,
  GetPurchaseHeaderError,
  CreatePurchaseHeader,
  CreatePurchaseHeadersuccess,
  CreatePurchaseHeaderError,
  UplodPurchaseHeader,
  UplodPurchaseHeadersuccess,
  UplodPurchaseHeaderError,
  DeletePurchaseHeader,
  DeletePurchaseHeadersuccess,
  DeletePurchaseHeaderError,
  PurchaseHeaderTypes,
  UpdatePurchaseHeader,
  UpdatePurchaseHeadersuccess,
  UpdatePurchaseHeaderError,
} from "./purchase-header.actions";

@Injectable()
export class PurchaseHeaderEffects {
  constructor(
    private actions: Actions,
    private purchaseHeaderService: PurchaseHeaderService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getPurchaseHeader$: Observable<Action> = this.actions.pipe(
    ofType<GetPurchaseHeader>(PurchaseHeaderTypes.GetPurchaseHeader),
    exhaustMap(() =>
      from(this.purchaseHeaderService.list()).pipe(
        map((payload) => {
          return new GetPurchaseHeaderSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Purchase Order", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetPurchaseHeaderError(true));
        })
      )
    )
  );

  @Effect()
  createPurchaseHeader$: Observable<Action> = this.actions.pipe(
    ofType<CreatePurchaseHeader>(PurchaseHeaderTypes.CreatePurchaseHeader),
    concatMap((action) =>
      from(this.purchaseHeaderService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase Order has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreatePurchaseHeadersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Purchase Order", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreatePurchaseHeaderError());
        })
      )
    )
  );

  @Effect()
  uploadPurchaseHeader$: Observable<Action> = this.actions.pipe(
    ofType<UplodPurchaseHeader>(PurchaseHeaderTypes.UplodPurchaseHeader),
    concatMap((action) =>
      from(this.purchaseHeaderService.upload(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase Order has been uploaded.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UplodPurchaseHeadersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to upload Purchase Order", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UplodPurchaseHeaderError());
        })
      )
    )
  );

  @Effect()
  updatePurchaseHeader$: Observable<Action> = this.actions.pipe(
    ofType<UpdatePurchaseHeader>(PurchaseHeaderTypes.UpdatePurchaseHeader),
    concatMap((action) =>
      from(this.purchaseHeaderService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase Order has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdatePurchaseHeadersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Purchase Order", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdatePurchaseHeaderError());
        })
      )
    )
  );
  @Effect()
  deletePurchaseHeader$: Observable<Action> = this.actions.pipe(
    ofType<DeletePurchaseHeader>(PurchaseHeaderTypes.DeletePurchaseHeader),
    concatMap((action) =>
      from(this.purchaseHeaderService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Purchase Order has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeletePurchaseHeadersuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Purchase Order:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeletePurchaseHeaderError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
