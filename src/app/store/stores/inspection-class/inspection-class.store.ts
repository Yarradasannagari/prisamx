import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Inspectionclass } from "src/app/services/inspection-class/inspection-class.service";

import { State } from "../..";

import { InspectionclassTypes, Union } from "./inspection-class.actions"; 

export interface InspectionclassState {
  loading: boolean;
  entities: Inspectionclass[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectionclassState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectionclassReducer(
  state: InspectionclassState = INITIAL_STATE,
  action: Union
): InspectionclassState {
  switch (action.type) {
    case InspectionclassTypes.GetInspectionclass:
      return {
        ...state,
        loading: true,
      };
    case InspectionclassTypes.GetInspectionclassSuccess:
      const inspectionclass: Inspectionclass[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectionclass,
      };
    case InspectionclassTypes.GetInspectionclassError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectionclassTypes.CreateInspectionclass:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectionclassTypes.CreateInspectionclasssuccess:
      const createdInspectionclass: Inspectionclass = action.payload;
      const entitiesAppended = [...state.entities, createdInspectionclass];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectionclassTypes.UpdateInspectionclass:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectionclassTypes.UpdateInspectionclasssuccess:
      const updatedInspectionclass: Inspectionclass = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectionclass];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectionclassTypes.UpdateInspectionclassError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectionclassTypes.CreateInspectionclassError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectionclassTypes.DeleteInspectionclass:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectionclassTypes.DeleteInspectionclasssuccess:
      const afterDelete = state.entities.filter((inspectionclass) => inspectionclass.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectionclassTypes.DeleteInspectionclassError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectionclassState = createFeatureSelector<State, InspectionclassState>("inspectionclass");

export const getInspectionclassLoading = createSelector(
  getInspectionclassState,
  (state) => state.loading
);

export const getInspectionclass = createSelector(getInspectionclassState, (state) => state.entities);

export const getInspectionclassCreating = createSelector(
  getInspectionclassState,
  (state) => state.creating
);

export const getInspectionclassCreated = createSelector(
  getInspectionclassState,
  (state) => state.created
);
export const getInspectionclassUpdating = createSelector(
  getInspectionclassState,
  (state) => state.updating
);

export const getInspectionclassUpdated = createSelector(
  getInspectionclassState,
  (state) => state.updated
);
export const getInspectionclassDeleting = createSelector(
  getInspectionclassState,
  (state) => state.deleting
);

export const getInspectionclassDeleted = createSelector(
  getInspectionclassState,
  (state) => state.deleted
);

export const getInspectionclassError = createSelector(
  getInspectionclassState,
  (state) => state.error
);
