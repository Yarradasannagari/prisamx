import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Purchase {
  // active: boolean;

  item: string;
  material: string;
  id: string;
  matName: string;
  quantity: string;
  qunit: string;
  punit: string;
  gvalue: string;
  // item: string;
  // item_cat: string;

}


@Injectable({
  providedIn: 'root',
})
export class PurchaseService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Purchase> {
    return this.entitySvc.get(Url.otfitems, id);
  }

  list(params?: Record<string, any>): Observable<Purchase[]> {
    return this.entitySvc.list(Url.otfitems, params);
  }

  create(aclRule: Purchase): Observable<Purchase> {
    return this.entitySvc.create(Url.otfitems, aclRule); 
  }

  update(aclRule: Purchase): Observable<Purchase> {
    return this.entitySvc.update(Url.otfitems, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.otfitems, id);
  }
  
}
