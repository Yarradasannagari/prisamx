import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionClassModelComponent } from './delete-inspection-class-model.component';

describe('DeleteInspectionClassModelComponent', () => {
  let component: DeleteInspectionClassModelComponent;
  let fixture: ComponentFixture<DeleteInspectionClassModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionClassModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionClassModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
