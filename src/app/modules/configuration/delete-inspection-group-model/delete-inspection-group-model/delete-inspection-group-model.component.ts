import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteInspectiongroup} from "src/app/store/stores/inspection-group/inspection-group.actions";
import { getInspectiongroupDeleted } from "src/app/store/stores/inspection-group/inspection-group.store";
@Component({
  selector: "app-delete-inspection-group-model",
  templateUrl: "./delete-inspection-group-model.component.html",
  styleUrls: ["./delete-inspection-group-model.component.scss"],
})
export class DeleteInspectionGroupModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteInspectiongroup() {
    this.isLoading = true;
    this.store.dispatch(new DeleteInspectiongroup(this.id));
    this.store
      .select(getInspectiongroupDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
