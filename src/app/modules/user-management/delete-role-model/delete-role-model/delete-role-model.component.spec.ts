import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteRoleModelComponent } from './delete-role-model.component';

describe('DeleteRoleModelComponent', () => {
  let component: DeleteRoleModelComponent;
  let fixture: ComponentFixture<DeleteRoleModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteRoleModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteRoleModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
