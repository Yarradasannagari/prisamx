import { Action } from "@ngrx/store";
import { Asset } from "../../../services/assets/asset.service";

export enum AssetTypes {
  GetAsset = "[Asset] GetAsset",
  GetAssetSuccess = "[Asset] GetAssetSuccess",
  GetAssetError = "[Asset] GetAssetError",
  CreateAsset = "[Asset] CreateAsset",
  CreateAssetsuccess = "[Asset] CreateAssetsuccess",
  CreateAssetError = "[Asset] CreateAssetError",
  UpdateAsset = "[Asset] UpdateAsset",
  UpdateAssetsuccess = "[Asset] UpdateAssetsuccess",
  UpdateAssetError = "[Asset] UpdateAssetError",
  DeleteAsset = "[Asset] DeleteAsset",
  DeleteAssetError = "[Asset] DeleteAssetError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteAssetsuccess = "[AclRule] DeleteAssetsuccess",
}

export class GetAsset implements Action { 
  readonly type = AssetTypes.GetAsset;
}

export class GetAssetSuccess implements Action {
  readonly type = AssetTypes.GetAssetSuccess;
  constructor(readonly payload: Asset[]) {}
}
export class GetAssetError implements Action {
  readonly type = AssetTypes.GetAssetError;
  constructor(readonly payload: boolean) {}
}

export class CreateAsset implements Action {
  readonly type = AssetTypes.CreateAsset;
  constructor(readonly payload: Asset) {}
}

export class CreateAssetsuccess implements Action {
  readonly type = AssetTypes.CreateAssetsuccess;
  constructor(readonly payload: Asset) {}
}

export class CreateAssetError implements Action {
  readonly type = AssetTypes.CreateAssetError;
}
export class UpdateAsset implements Action {
  readonly type = AssetTypes.UpdateAsset;
  constructor(readonly payload: Asset) {}
}

export class UpdateAssetsuccess implements Action {
  readonly type = AssetTypes.UpdateAssetsuccess;
  constructor(readonly payload: Asset) {}
}

export class UpdateAssetError implements Action {
  readonly type = AssetTypes.UpdateAssetError;
}

export class DeleteAsset implements Action {
  readonly type = AssetTypes.DeleteAsset;
  constructor(readonly payload: string) {}
}

export class DeleteAssetsuccess implements Action {
  readonly type = AssetTypes.DeleteAssetsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteAssetError implements Action {
  readonly type = AssetTypes.DeleteAssetError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetAsset
  | GetAssetSuccess
  | GetAssetError
  | CreateAsset
  | CreateAssetsuccess
  | CreateAssetError
  | DeleteAsset
  | DeleteAssetsuccess
  | DeleteAssetError
  | UpdateAsset
  | UpdateAssetsuccess
  | UpdateAssetError
