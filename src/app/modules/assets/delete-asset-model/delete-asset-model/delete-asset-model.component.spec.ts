import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssetModelComponent } from './delete-asset-model.component';

describe('DeleteAssetModelComponent', () => {
  let component: DeleteAssetModelComponent;
  let fixture: ComponentFixture<DeleteAssetModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAssetModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssetModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
