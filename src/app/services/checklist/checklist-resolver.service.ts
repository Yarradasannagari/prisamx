import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Checklist, ChecklistService } from './checklist.service';

@Injectable({ providedIn: 'root' })
export class ChecklistResolverService implements Resolve<Checklist> {
  constructor(private checklistService: ChecklistService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Checklist> | Promise<Checklist> | Checklist {
    const id = route.paramMap.get('id');
    return this.checklistService.get(id);
  }
}
