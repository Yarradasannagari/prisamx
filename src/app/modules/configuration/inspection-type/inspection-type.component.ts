import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Inspectiontype, InspectiontypeService } from "src/app/services/inspection-types/inspection-type.service";
import { GetInspectiontype } from "src/app/store/stores/inspection-type/inspection-type.actions";
import {
  getInspectiontypeError,
  getInspectiontype,
  getInspectiontypeLoading,
} from "src/app/store/stores/inspection-type/inspection-type.store";
import { State, Store } from "../../../store";
import { DeleteInspectionTypeModelComponent } from "../delete-inspection-type-model/delete-inspection-type-model/delete-inspection-type-model.component";

@Component({
  selector: "app-inspection-type",
  templateUrl: "./inspection-type.component.html",
  styleUrls: ["./inspection-type.component.scss"],
})
export class InspectionTypeComponent implements OnDestroy, OnInit {
  inspectiontype$: Observable<Inspectiontype[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  inspectiontype: Inspectiontype[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private inspectiontypeService: InspectiontypeService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectiontypeLoading);
    this.store.dispatch(new GetInspectiontype());
    this.inspectiontype$ = this.store.select(getInspectiontype);
    this.store.select(getInspectiontypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }
  
  reTry() {
    this.isLoading$ = this.store.select(getInspectiontypeLoading);
    this.store.dispatch(new GetInspectiontype());
    this.inspectiontype$ = this.store.select(getInspectiontype);
    this.store.select(getInspectiontypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionTypeModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
