import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Document } from "src/app/services/documents/document.service";

import { State } from "../..";

import { DocumentTypes, Union } from "./document.actions";

export interface DocumentState {
  loading: boolean;
  entities: Document[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: DocumentState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function DocumentReducer(
  state: DocumentState = INITIAL_STATE,
  action: Union
): DocumentState {
  switch (action.type) {
    case DocumentTypes.GetDocument:
      return {
        ...state,
        loading: true,
      };
    case DocumentTypes.GetDocumentSuccess:
      const document: Document[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: document,
      };
    case DocumentTypes.GetDocumentError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case DocumentTypes.CreateDocument:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case DocumentTypes.CreateDocumentsuccess:
      const createdDocument: Document = action.payload;
      const entitiesAppended = [...state.entities, createdDocument];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case DocumentTypes.UpdateDocument:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case DocumentTypes.UpdateDocumentsuccess:
      const updatedDocument: Document = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedDocument];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case DocumentTypes.UpdateDocumentError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case DocumentTypes.CreateDocumentError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case DocumentTypes.DeleteDocument:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case DocumentTypes.DeleteDocumentsuccess:
      const afterDelete = state.entities.filter((document) => document.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case DocumentTypes.DeleteDocumentError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getDocumentState = createFeatureSelector<State, DocumentState>("document");

export const getDocumentLoading = createSelector(
  getDocumentState,
  (state) => state.loading
);

export const getDocument = createSelector(getDocumentState, (state) => state.entities);

export const getDocumentCreating = createSelector(
  getDocumentState,
  (state) => state.creating
);

export const getDocumentCreated = createSelector(
  getDocumentState,
  (state) => state.created
);
export const getDocumentUpdating = createSelector(
  getDocumentState,
  (state) => state.updating
);

export const getDocumentUpdated = createSelector(
  getDocumentState,
  (state) => state.updated
);
export const getDocumentDeleting = createSelector(
  getDocumentState,
  (state) => state.deleting
);

export const getDocumentDeleted = createSelector(
  getDocumentState,
  (state) => state.deleted
);

export const getDocumentError = createSelector(
  getDocumentState,
  (state) => state.error
);
