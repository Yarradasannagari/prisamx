import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { RoleService } from "../../../services/roles/role.service";

import {
  GetGroups,
  GetGroupsSuccess,
  GetGroupsError,
  CreateGroup,
  CreateGroupsuccess,
  CreateGroupError,
  DeleteGroup,
  DeleteGroupsuccess,
  DeleteGroupError,
  GroupTypes,
  UpdateGroup,
  UpdateGroupsuccess,
  UpdateGroupError,
} from "./groups.actions";
import { GroupService } from "src/app/services/groups/group.service";

@Injectable()
export class GroupEffects {
  constructor(
    private actions: Actions,
    private groupService: GroupService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getGroups$: Observable<Action> = this.actions.pipe(
    ofType<GetGroups>(GroupTypes.GetGroups),
    exhaustMap(() =>
      from(this.groupService.list()).pipe(
        map((payload) => {
          return new GetGroupsSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Groups", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetGroupsError(true));
        })
      )
    )
  );

  @Effect()
  createGroup$: Observable<Action> = this.actions.pipe(
    ofType<CreateGroup>(GroupTypes.CreateGroup),
    concatMap((action) =>
      from(this.groupService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Group has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateGroupsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Group", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateGroupError());
        })
      )
    )
  );
  @Effect()
  updateGroup$: Observable<Action> = this.actions.pipe(
    ofType<UpdateGroup>(GroupTypes.UpdateGroup),
    concatMap((action) =>
      from(this.groupService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Group has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateGroupsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Group", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateGroupError());
        })
      )
    )
  );
  @Effect()
  deleteGroup$: Observable<Action> = this.actions.pipe(
    ofType<DeleteGroup>(GroupTypes.DeleteGroup),
    concatMap((action) =>
      from(this.groupService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "User Group is deleted!.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteGroupsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Group:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteGroupError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
