import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Inspectioncode, InspectioncodeService } from './inspection-code.service';

@Injectable({ providedIn: 'root' })
export class InspectioncodeResolverService implements Resolve<Inspectioncode> {
  constructor(private inspectioncodeService: InspectioncodeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Inspectioncode> | Promise<Inspectioncode> | Inspectioncode {
    const id = route.paramMap.get('id');
    return this.inspectioncodeService.get(id);
  }
}
