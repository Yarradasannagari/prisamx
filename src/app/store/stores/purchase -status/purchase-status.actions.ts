import { Action } from "@ngrx/store";
import { PurchaseStatus } from "../../../services/purchase -status/purchase-status.service";

export enum PurchaseStatusTypes {
  GetPurchaseStatus = "[PurchaseStatus] GetPurchaseStatus",
  GetPurchaseStatusSuccess = "[PurchaseStatus] GetPurchaseStatusSuccess",
  GetPurchaseStatusError = "[PurchaseStatus] GetPurchaseStatusError",
  CreatePurchaseStatus = "[PurchaseStatus] CreatePurchaseStatus",
  CreatePurchaseStatussuccess = "[PurchaseStatus] CreatePurchaseStatussuccess",
  CreatePurchaseStatusError = "[PurchaseStatus] CreatePurchaseStatusError",
  UpdatePurchaseStatus = "[PurchaseStatus] UpdatePurchaseStatus",
  UpdatePurchaseStatussuccess = "[PurchaseStatus] UpdatePurchaseStatussuccess",
  UpdatePurchaseStatusError = "[PurchaseStatus] UpdatePurchaseStatusError",
  DeletePurchaseStatus = "[PurchaseStatus] DeletePurchaseStatus",
  DeletePurchaseStatusError = "[PurchaseStatus] DeletePurchaseStatusError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeletePurchaseStatussuccess = "[AclRule] DeletePurchaseStatussuccess",
}

export class GetPurchaseStatus implements Action { 
  readonly type = PurchaseStatusTypes.GetPurchaseStatus;
}

export class GetPurchaseStatusSuccess implements Action {
  readonly type = PurchaseStatusTypes.GetPurchaseStatusSuccess;
  constructor(readonly payload: PurchaseStatus[]) {}
}
export class GetPurchaseStatusError implements Action {
  readonly type = PurchaseStatusTypes.GetPurchaseStatusError;
  constructor(readonly payload: boolean) {}
}

export class CreatePurchaseStatus implements Action {
  readonly type = PurchaseStatusTypes.CreatePurchaseStatus;
  constructor(readonly payload: PurchaseStatus) {}
}

export class CreatePurchaseStatussuccess implements Action {
  readonly type = PurchaseStatusTypes.CreatePurchaseStatussuccess;
  constructor(readonly payload: PurchaseStatus) {}
}

export class CreatePurchaseStatusError implements Action {
  readonly type = PurchaseStatusTypes.CreatePurchaseStatusError;
}
export class UpdatePurchaseStatus implements Action {
  readonly type = PurchaseStatusTypes.UpdatePurchaseStatus;
  constructor(readonly payload: PurchaseStatus) {}
}

export class UpdatePurchaseStatussuccess implements Action {
  readonly type = PurchaseStatusTypes.UpdatePurchaseStatussuccess;
  constructor(readonly payload: PurchaseStatus) {}
}

export class UpdatePurchaseStatusError implements Action {
  readonly type = PurchaseStatusTypes.UpdatePurchaseStatusError;
}

export class DeletePurchaseStatus implements Action {
  readonly type = PurchaseStatusTypes.DeletePurchaseStatus;
  constructor(readonly payload: string) {}
}

export class DeletePurchaseStatussuccess implements Action {
  readonly type = PurchaseStatusTypes.DeletePurchaseStatussuccess;
  constructor(readonly payload: string) {}
}

export class DeletePurchaseStatusError implements Action {
  readonly type = PurchaseStatusTypes.DeletePurchaseStatusError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetPurchaseStatus
  | GetPurchaseStatusSuccess
  | GetPurchaseStatusError
  | CreatePurchaseStatus
  | CreatePurchaseStatussuccess
  | CreatePurchaseStatusError
  | DeletePurchaseStatus
  | DeletePurchaseStatussuccess
  | DeletePurchaseStatusError
  | UpdatePurchaseStatus
  | UpdatePurchaseStatussuccess
  | UpdatePurchaseStatusError
