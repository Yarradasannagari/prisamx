import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Inspectiongroup, InspectiongroupService } from "src/app/services/inspection-group/inspection-group.service";
import { GetInspectiongroup } from "src/app/store/stores/inspection-group/inspection-group.actions";
import {
  getInspectiongroupError,
  getInspectiongroup,
  getInspectiongroupLoading,
} from "src/app/store/stores/inspection-group/inspection-group.store";
import { State, Store } from "../../../store";
import { DeleteInspectionGroupModelComponent } from "../delete-inspection-group-model/delete-inspection-group-model/delete-inspection-group-model.component";

@Component({
  selector: "app-inspection-group",
  templateUrl: "./inspection-group.component.html",
  styleUrls: ["./inspection-group.component.scss"],
})
export class InspectionGroupComponent implements OnDestroy, OnInit {
  inspectiongroup$: Observable<Inspectiongroup[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  inspectiongroup: Inspectiongroup[];
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private inspectiongroupService: InspectiongroupService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectiongroupLoading);
    this.store.dispatch(new GetInspectiongroup());
    this.inspectiongroup$ = this.store.select(getInspectiongroup);
    this.store.select(getInspectiongroupError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }

  reTry() {
    this.isLoading$ = this.store.select(getInspectiongroupLoading);
    this.store.dispatch(new GetInspectiongroup());
    this.inspectiongroup$ = this.store.select(getInspectiongroup);
    this.store.select(getInspectiongroupError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionGroupModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
