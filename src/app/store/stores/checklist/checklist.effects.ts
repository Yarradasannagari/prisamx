import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { ChecklistService } from "../../../services/checklist/checklist.service"; 

import {
  GetCheckList,
  GetCheckListSuccess,
  GetCheckListError,
  CreateCheckList,
  CreateCheckListsuccess,
  CreateCheckListError,
  DeleteCheckList,
  DeleteCheckListsuccess,
  DeleteCheckListError,
  CheckListTypes,
  UpdateCheckList,
  UpdateCheckListsuccess,
  UpdateCheckListError,
} from "./checklist.actions";

@Injectable()
export class CheckListEffects {
  constructor(
    private actions: Actions,
    private checklistService: ChecklistService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getCheckList$: Observable<Action> = this.actions.pipe(
    ofType<GetCheckList>(CheckListTypes.GetCheckList),
    exhaustMap(() =>
      from(this.checklistService.list()).pipe(
        map((payload) => {
          return new GetCheckListSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve CheckList", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetCheckListError(true));
        })
      )
    )
  );

  @Effect()
  createCheckList$: Observable<Action> = this.actions.pipe(
    ofType<CreateCheckList>(CheckListTypes.CreateCheckList),
    concatMap((action) =>
      from(this.checklistService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The CheckList has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateCheckListsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create CheckList", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateCheckListError());
        })
      )
    )
  );
  @Effect()
  updateCheckList$: Observable<Action> = this.actions.pipe(
    ofType<UpdateCheckList>(CheckListTypes.UpdateCheckList),
    concatMap((action) =>
      from(this.checklistService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Check List has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateCheckListsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Check List", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateCheckListError());
        })
      )
    )
  );
  @Effect()
  deleteCheckList$: Observable<Action> = this.actions.pipe(
    ofType<DeleteCheckList>(CheckListTypes.DeleteCheckList),
    concatMap((action) =>
      from(this.checklistService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Check List has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteCheckListsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Check List:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteCheckListError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
