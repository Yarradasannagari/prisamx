import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Assetcategory, AssetcategoryService } from "src/app/services/asset-category/asset-category.service";
import { GetAssetcategory } from "src/app/store/stores/asset-category/asset-category.actions"; 
import {
  getAssetcategoryError,
  getAssetcategory,
  getAssetcategoryLoading,
} from "src/app/store/stores/asset-category/asset-category.store";
import { State, Store } from "../../../store";
import { DeleteAssetCategoryModelComponent } from "../delete-asset-category-model/delete-asset-category-model/delete-asset-category-model.component";

@Component({
  selector: "app-asset-category",
  templateUrl: "./asset-category.component.html",
  styleUrls: ["./asset-category.component.scss"],
})
export class AssetCategoryComponent implements OnDestroy, OnInit {
  assetcategory$: Observable<Assetcategory[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  
  roles: Assetcategory[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private roleService: AssetcategoryService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getAssetcategoryLoading);
    this.store.dispatch(new GetAssetcategory());
    this.assetcategory$ = this.store.select(getAssetcategory);
    this.store.select(getAssetcategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 

  reTry() {
    this.isLoading$ = this.store.select(getAssetcategoryLoading);
    this.store.dispatch(new GetAssetcategory());
    this.assetcategory$ = this.store.select(getAssetcategory);
    this.store.select(getAssetcategoryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteAssetCategoryModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
