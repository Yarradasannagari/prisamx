import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Assettype, AssettypeService } from "src/app/services/asset-type/asset-type.service";
import { GetAssettype } from "src/app/store/stores/asset-type/asset-type.actions";
import {
  getAssettypeError,
  getAssettype,
  getAssettypeLoading,
} from "src/app/store/stores/asset-type/asset-type.store";
import { State, Store } from "../../../store";
import { DeleteAssetTypeModelComponent } from "../delete-asset-type-model/delete-asset-type-model/delete-asset-type-model.component";

@Component({
  selector: "app-asset-type",
  templateUrl: "./asset-type.component.html",
  styleUrls: ["./asset-type.component.scss"],
})
export class AssetTypeComponent implements OnDestroy, OnInit {
  assettype$: Observable<Assettype[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  roles: Assettype[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private roleService: AssettypeService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getAssettypeLoading);
    this.store.dispatch(new GetAssettype());
    this.assettype$ = this.store.select(getAssettype);
    this.store.select(getAssettypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }
  
  reTry() {
    this.isLoading$ = this.store.select(getAssettypeLoading);
    this.store.dispatch(new GetAssettype());
    this.assettype$ = this.store.select(getAssettype);
    this.store.select(getAssettypeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteAssetTypeModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
