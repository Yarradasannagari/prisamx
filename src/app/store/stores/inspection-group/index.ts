import * as InspectiongroupActions from "./inspection-group.actions";
import { InspectiongroupEffects } from "./inspection-group.effects";
import { INITIAL_STATE, InspectiongroupReducer, InspectiongroupState } from "./inspection-group.store";

export { InspectiongroupActions, InspectiongroupReducer, InspectiongroupState, INITIAL_STATE, InspectiongroupEffects };
