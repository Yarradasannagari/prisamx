import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Inspectiontype } from "src/app/services/inspection-types/inspection-type.service";
import { CreateInspectiontype, UpdateInspectiontype } from "src/app/store/stores/inspection-type/inspection-type.actions";
import { getInspectiontypeCreated, getInspectiontypeUpdated } from "src/app/store/stores/inspection-type/inspection-type.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-inspection-type-form",
  templateUrl: "./inspection-type-form.component.html",
  styleUrls: ["./inspection-type-form.component.scss"],
})
export class InspectionTypeFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    catergoryId: "",
    name: "",
    typeId: "",
    active: true,
  };
  inspectiontype: Inspectiontype;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.catergoryId = this.route.snapshot.data["report"].catergoryId;
        this.formData.name =
          this.route.snapshot.data["report"].name;
          this.formData.typeId = this.route.snapshot.data["report"].typeId;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      catergoryId: [
        this.formData.catergoryId,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
          
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          
        ]),
      ],
      typeId: [
        this.formData.typeId,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectiontype: Inspectiontype = {
      id: this.formData.id,
      catergoryId: this.formData.catergoryId,
      name: this.formData.name,
      typeId: this.formData.typeId,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectiontype(inspectiontype);
    } else {
      this.createInspectiontype(inspectiontype);
    }
  }
  private createInspectiontype(inspectiontype: Inspectiontype): void {
    this.store.dispatch(new CreateInspectiontype(inspectiontype));
    this.store
      .select(getInspectiontypeCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectiontype(inspectiontype: Inspectiontype): void {
    this.store.dispatch(new UpdateInspectiontype(inspectiontype));
    this.store
      .select(getInspectiontypeUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "inspection-type"]);
  }
}
