import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteLocationtype} from "src/app/store/stores/location-type/location-type.actions";
import { getLocationtypeDeleted } from "src/app/store/stores/location-type/location-tye.store";
@Component({
  selector: "app-delete-location-type-model",
  templateUrl: "./delete-location-type-model.component.html",
  styleUrls: ["./delete-location-type-model.component.scss"],
})
export class DeleteLocationTypeModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteLocationtype() {
    this.isLoading = true;
    this.store.dispatch(new DeleteLocationtype(this.id));
    this.store
      .select(getLocationtypeDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
