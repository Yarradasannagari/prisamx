import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserManagementComponent } from "./user-management.component";
import { UsersComponent } from "./users/users.component";
import { RolesComponent } from "./roles/roles.component";
import { RolesFormComponent } from "./roles-form/roles-form.component";
import { RoleResolverService } from "src/app/services/roles/role-resolver.service";
import { UserFormComponent } from "./user-form/user-form/user-form.component";
import { UserResolverService } from "src/app/services/users/user-resolver.service";

import { GroupComponent } from "./group/group.component";
import { GroupFormComponent } from "./group-form/group-form.component";
import { GroupResolverService } from "src/app/services/groups/group-resolver.service";

const routes: Routes = [
  {
    path: "",
    component: UserManagementComponent,
    children: [
      {
        path: "users",
        component: UsersComponent,
      },
      {
        path: "roles",
        component: RolesComponent,
      },
      {
        path: "add-role",
        component: RolesFormComponent,
      },
      {
        path: ":id/edit-role",
        component: RolesFormComponent,
        pathMatch: "full",
        resolve: {
          report: RoleResolverService,
        },
      },
      {
        path: "group",
        component: GroupComponent,
      },
      {
        path: "add-group",
        component: GroupFormComponent,
      },
      {
        path: ":id/edit-group",
        component: GroupFormComponent,
        pathMatch: "full",
        resolve: {
          report: GroupResolverService,
        },
      },
      {
        path: ":id/edit-user",
        component: UserFormComponent,
        pathMatch: "full",
        resolve: {
          report: UserResolverService,
        },
      },
      {
        path: "add-user",
        component: UserFormComponent,
      },
      { path: "", redirectTo: "users", pathMatch: "full" },
      { path: "**", redirectTo: "users", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserManagementRoutingModule {}
