import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionPointModelComponent } from './delete-inspection-point-model.component';

describe('DeleteInspectionPointModelComponent', () => {
  let component: DeleteInspectionPointModelComponent;
  let fixture: ComponentFixture<DeleteInspectionPointModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionPointModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionPointModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
