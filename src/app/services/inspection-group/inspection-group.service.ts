import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Inspectiongroup {
  id:string;
  active: boolean;
  group: string;
  nameSpace: string;
}


@Injectable({
  providedIn: 'root',
})
export class InspectiongroupService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Inspectiongroup> {
    return this.entitySvc.get(Url.inspectiongroup, id);
  }

  list(params?: Record<string, any>): Observable<Inspectiongroup[]> {
    return this.entitySvc.list(Url.inspectiongroup, params);
  }

  create(aclRule: Inspectiongroup): Observable<Inspectiongroup> {
    return this.entitySvc.create(Url.inspectiongroup, aclRule);
  }

  update(aclRule: Inspectiongroup): Observable<Inspectiongroup> {
    return this.entitySvc.update(Url.inspectiongroup, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectiongroup, id);
  }
  
}
