import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';

import { WidgetsModule } from '../../_metronic/partials/content/widgets/widgets.module';
import { PurchasingComponent } from "./purchase.component";
import { PurchaseRoutingModule } from "./purchase-routing.module";
import { InlineSVGModule } from 'ng-inline-svg';
import { ActiveRolesPipe } from './pipes/active-roles.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { PurchaseComponent } from "./purchase/purchase.component";
import { PurchaseFormComponent } from "./purchase-form/purchase-form.component";
import { ViewPurchaseComponent } from "./view-purchase/view-purchase.component";
import { DeletePurchaseModelComponent } from './delete-purchase-model/delete-purchase-model/delete-purchase-model.component';
import { AgGridModule } from 'ag-grid-angular';
import {MatStepperModule} from '@angular/material/stepper';
import {MatMenuModule} from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [
    PurchasingComponent,
    ActiveRolesPipe,
    SearchPipe,
    PurchaseComponent,
    PurchaseFormComponent,
    ViewPurchaseComponent,
    DeletePurchaseModelComponent
  ],
  imports: [
    CommonModule,
    Ng2OrderModule,
    NgxPaginationModule,
    PurchaseRoutingModule,
    CommonModule,
    CommonModule,
    FormsModule,
    InlineSVGModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    NgbModule,
    ReactiveFormsModule,
    WidgetsModule,
    MatStepperModule,
    AgGridModule.withComponents([])
  ],
})
export class PurchaseModule {}
