import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteGroup } from "src/app/store/stores/groups/groups.actions";
import { getGroupDeleted } from "src/app/store/stores/groups/groups.store";
@Component({
  selector: "app-delete-group-model",
  templateUrl: "./delete-group-model.component.html",
  styleUrls: ["./delete-group-model.component.scss"],
})
export class DeleteGroupModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteGroup() {
    this.isLoading = true;
    this.store.dispatch(new DeleteGroup(this.id));
    this.store
      .select(getGroupDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
