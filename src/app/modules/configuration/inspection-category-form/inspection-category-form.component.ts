import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Inspectioncategory } from "src/app/services/inspection-category/inspection-category.service";
import { CreateInspectioncategory, UpdateInspectioncategory } from "src/app/store/stores/inspection-category/inspection-category.actions";
import { getInspectioncategoryCreated, getInspectioncategoryUpdated } from "src/app/store/stores/inspection-category/inspection-category.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-inspection-category-form",
  templateUrl: "./inspection-category-form.component.html",
  styleUrls: ["./inspection-category-form.component.scss"],
})
export class InspectionCategoryFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    catergoryId: "",
    name: "",
    active: true,
  };
  inspectioncategory: Inspectioncategory;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.catergoryId = this.route.snapshot.data["report"].catergoryId;
        this.formData.name =
          this.route.snapshot.data["report"].name;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      catergoryId: [
        this.formData.catergoryId,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
          
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectioncategory: Inspectioncategory = {
      id: this.formData.id,
      catergoryId: this.formData.catergoryId,
      name: this.formData.name,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectioncategory(inspectioncategory);
    } else {
      this.createInspectioncategory(inspectioncategory);
    }
  }
  private createInspectioncategory(inspectioncategory: Inspectioncategory): void {
    this.store.dispatch(new CreateInspectioncategory(inspectioncategory));
    this.store
      .select(getInspectioncategoryCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectioncategory(inspectioncategory: Inspectioncategory): void {
    this.store.dispatch(new UpdateInspectioncategory(inspectioncategory));
    this.store
      .select(getInspectioncategoryUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "inspection-category"]);
  }
}
