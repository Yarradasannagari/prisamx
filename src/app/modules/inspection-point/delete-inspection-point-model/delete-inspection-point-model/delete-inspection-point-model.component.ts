import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteAsset } from "src/app/store/stores/assets/asset.actions";
import { getAssetDeleted } from "src/app/store/stores/assets/asset.store";
@Component({
  selector: "app-delete-inspection-point-model",
  templateUrl: "./delete-inspection-point-model.component.html",
  styleUrls: ["./delete-inspection-point-model.component.scss"],
})
export class DeleteInspectionPointModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteAsset() {
    this.isLoading = true;
    this.store.dispatch(new DeleteAsset(this.id));
    this.store
      .select(getAssetDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
