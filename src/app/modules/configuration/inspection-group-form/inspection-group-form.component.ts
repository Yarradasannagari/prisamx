import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Inspectiongroup } from "src/app/services/inspection-group/inspection-group.service";
import { CreateInspectiongroup, UpdateInspectiongroup } from "src/app/store/stores/inspection-group/inspection-group.actions";
import { getInspectiongroupCreated, getInspectiongroupUpdated } from "src/app/store/stores/inspection-group/inspection-group.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-inspection-group-form",
  templateUrl: "./inspection-group-form.component.html",
  styleUrls: ["./inspection-group-form.component.scss"],
})
export class InspectionGroupFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup; 
  formData = {
    id: null,
    group: "",
    nameSpace: "",
    active: true,
  };
  inspectiongroup: Inspectiongroup;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.group = this.route.snapshot.data["report"].group;
        this.formData.nameSpace =
          this.route.snapshot.data["report"].nameSpace;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      group: [
        this.formData.group,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(10),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      nameSpace: [
        this.formData.nameSpace,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectiongroup: Inspectiongroup = {
      id: this.formData.id,
      group: this.formData.group,
      nameSpace: this.formData.nameSpace,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectiongroup(inspectiongroup);
    } else {
      this.createInspectiongroup(inspectiongroup);
    }
  }
  private createInspectiongroup(inspectiongroup: Inspectiongroup): void {
    this.store.dispatch(new CreateInspectiongroup(inspectiongroup));
    this.store
      .select(getInspectiongroupCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectiongroup(inspectiongroup: Inspectiongroup): void {
    this.store.dispatch(new UpdateInspectiongroup(inspectiongroup));
    this.store
      .select(getInspectiongroupUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "inspection-group"]);
  }
}
