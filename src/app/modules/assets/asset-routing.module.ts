import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AssetsComponent } from "./asset.component";

import { AssetComponent } from "./asset/asset.component";
import { AssetFormComponent } from "./asset-form/asset-form.component";
import { AssetResolverService } from "src/app/services/assets/asset-resolver.service";
import { ViewAssetComponent } from "./view-asset/view-asset.component";

const routes: Routes = [
  {
    path: "",
    component: AssetsComponent,
    children: [
      {
        path: "asset",
        component: AssetComponent,
      },
      {
        path: "add-asset",
        component: AssetFormComponent,
      },
      {
        path: ":id/edit-asset",
        component: AssetFormComponent,
        pathMatch: "full",
        resolve: {
          report: AssetResolverService,
        },
      },
      {
        path: ":id/view-asset",
        component: ViewAssetComponent,
        pathMatch: "full",
        resolve: {
          report: AssetResolverService,
        },
      },
     
      { path: "", redirectTo: "asset", pathMatch: "full" },
      { path: "**", redirectTo: "asset", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssetsRoutingModule {}
