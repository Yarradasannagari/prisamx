import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Inspectiontype {
  id:string;
  active: boolean;
  catergoryId: string;
  name: string;
  typeId: string;
}


@Injectable({
  providedIn: 'root',
})
export class InspectiontypeService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Inspectiontype> {
    return this.entitySvc.get(Url.inspectiontype, id);
  }

  list(params?: Record<string, any>): Observable<Inspectiontype[]> {
    return this.entitySvc.list(Url.inspectiontype, params);
  }

  create(aclRule: Inspectiontype): Observable<Inspectiontype> {
    return this.entitySvc.create(Url.inspectiontype, aclRule);
  }

  update(aclRule: Inspectiontype): Observable<Inspectiontype> {
    return this.entitySvc.update(Url.inspectiontype, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectiontype
      , id);
  }
  
}
