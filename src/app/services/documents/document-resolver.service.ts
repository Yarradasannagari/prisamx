import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Document, DocumentService } from './document.service';

@Injectable({ providedIn: 'root' })
export class DocumentResolverService implements Resolve<Document> {
  constructor(private documentService: DocumentService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Document> | Promise<Document> | Document {
    const id = route.paramMap.get('id');
    return this.documentService.get(id);
  }
}
