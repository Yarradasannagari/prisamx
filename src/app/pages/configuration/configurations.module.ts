import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ConfigurationComponent } from './configurations.component';
import { ConfigurationsModule } from 'src/app/pages/configurations.module';

@NgModule({
  declarations: [ConfigurationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConfigurationComponent,
      },
    ]),
    ConfigurationsModule,
  ],
})
export class ConfigurationModule {}
