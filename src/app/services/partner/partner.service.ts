import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Partner {
  // active: boolean;
  asset_id: string;
  aufnr: string;
  building: string;
  changed_by: string;
  changed_date: string;
  changed_time: string;
  city: string;
  city1: string;
  counter: string;
  country: string;
  created_by: string;
  created_date: string;
  created_time: string;
  custodian: string;
  email_addr: string;
  equnr: string; 
  floor: string;
  house_no1: string;
  house_no2: string;
  house_num1: string;
  house_num2: string;
  id: string;
  landmark: string;
  location: string;
  location_id: string;
  mob_number: string;
  name1: string;
  name2: string;
  nrart: string;
  ob_type: string; 
  obj_id: string;
  obj_nr: string;
  obj_type: string;
  par_func: string;
  par_func_name: string;
  par_type: string;
  parnr: string;
  parnr_id: string;
  partxt: string;
  parvw: string;
  post_code1: string;
  qmnum: string;
  region: string;
  str_suppl1: string; 
  str_suppl2: string;
  str_suppl3: string;
  street: string;
  street2: string;
  street3: string;
  street4: string;
  tel_number: string;
  ticket_id: string;
  tplnr: string;
  user_1: string;
  user_2: string;
  user_3: string;
  user_4: string;
  user_5: string; 
  work_order: string;
  zipcode: string;

}


@Injectable({
  providedIn: 'root',
})
export class PartnerService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Partner> {
    return this.entitySvc.get(Url.partner, id);
  }

  list(params?: Record<string, any>): Observable<Partner[]> {
    return this.entitySvc.list(Url.partner, params);
  }

  create(aclRule: Partner): Observable<Partner> {
    return this.entitySvc.create(Url.partner, aclRule); 
  }

  update(aclRule: Partner): Observable<Partner> {
    return this.entitySvc.update(Url.partner, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.partner, id);
  }
  
}
