import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Group, GroupService } from './group.service';

@Injectable({ providedIn: 'root' })
export class GroupResolverService implements Resolve<Group> {
  constructor(private groupService: GroupService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Group> | Promise<Group> | Group {
    const id = route.paramMap.get('id');
    return this.groupService.get(id);
  }
}
