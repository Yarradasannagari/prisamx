import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import {
  PurchaseHeader,
  PurchaseHeaderService,
} from "src/app/services/purchase -header/purchase-header.service";
import {
  GetPurchaseHeader,
  UplodPurchaseHeader,
} from "src/app/store/stores/purchase -header/purchase-header.actions";
import {
  getPurchaseHeaderError,
  getPurchaseHeader,
  getPurchaseHeaderLoading,
} from "src/app/store/stores/purchase -header/purchase-header.store";
import { State, Store } from "../../../store";
import { DeletePurchaseHeaderModelComponent } from "../delete-purchase-header-model/delete-purchase-header-model/delete-purchase-header-model.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import * as XLSX from "xlsx";
import { MatSnackBar } from "@angular/material/snack-bar";
import { getPurchaseHeaderUploaded } from "src/app/store/stores/purchase -header/purchase-header.store";
import { takeUntil } from "rxjs/operators";
type AOA = any[][];

@Component({
  selector: "app-purchase",
  templateUrl: "./purchase.component.html",
  styleUrls: ["./purchase.component.scss"],
})
export class PurchaseComponent implements OnDestroy, OnInit {
  name = "This is XLSX TO JSON CONVERTER";
  data: AOA = [];
  checked = false;
  isDuplicate: Boolean = false;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  columnDefs = [
    { field: "PO #", sortable: true, filter: true },
    { field: "TransactionId", sortable: true, filter: true },
    { field: "POStatus", sortable: true, filter: true },
    { field: "PODate", sortable: true, filter: true },
    { field: "Quantity", sortable: true, filter: true },
    { field: "Vendor#", sortable: true, filter: true },
    { field: "VendorName", sortable: true, filter: true },
    { field: "Actions", sortable: true, filter: true },
  ];

  rowData = [
    {
      "PO #": 100012,
      TransactionId: 1011,
      POStatus: "New",
      PODate: "30-06-2021",
      Quantity: 100,
    },
    {
      "PO #": 100011,
      TransactionId: 2021062100002,
      POStatus: "Progress",
      PODate: "30-06-2021",
      Quantity: 200,
    },
    {
      "PO #": 1123001,
      TransactionId: 1009090721865,
      POStatus: "Closed",
      PODate: "30-06-2021",
      Quantity: 300,
    },
  ];

  myForm = new FormGroup({
    name: new FormControl("", [Validators.required, Validators.minLength(3)]),
    file: new FormControl("", [Validators.required]),
    fileSource: new FormControl("", [Validators.required]),
  });

  purchase$: Observable<PurchaseHeader[]>;

  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText: any;
  aclRule: any;

  purchase: PurchaseHeader[];

  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private purchaseHeaderService: PurchaseHeaderService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar
  ) {
    this.isLoading$ = this.store.select(getPurchaseHeaderLoading);
    this.store.dispatch(new GetPurchaseHeader());
    this.purchase$ = this.store.select(getPurchaseHeader);
    this.store.select(getPurchaseHeaderError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }
  
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getPurchaseHeaderLoading);
    this.store.dispatch(new GetPurchaseHeader());
    this.purchase$ = this.store.select(getPurchaseHeader);
    this.store.select(getPurchaseHeaderError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeletePurchaseHeaderModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {},
      () => {}
    );
  }

  // onFileChange(event) {

  //   if (event.target.files.length > 0) {
  //     const file = event.target.files[0];
  //     this.myForm.patchValue({
  //       fileSource: file
  //     });
  //   }
  // }
  onFileChange1(ev) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: "binary" });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        // var sheetName = workbook.SheetNames[0];
        const sheet = workBook.SheetNames[0];
        initial = XLSX.utils.sheet_to_json(workBook.Sheets[sheet]);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);

      var dataarray = [];
      jsonData.forEach((item) => {});
    };

    reader.readAsBinaryString(file);
  }
  onFileChange(ev) {
    let workBook = null;
    let jsonData = null;
    const target: DataTransfer = <DataTransfer>ev.target;
    if (target.files.length !== 1) throw new Error("Cannot use multiple files");
    const reader: FileReader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (e: any) => {
      console.log("excel onload" + this.data);
      const data = reader.result;
      workBook = XLSX.read(data, { type: "binary" });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        // var sheetName = workbook.SheetNames[0];
        const sheet = workBook.SheetNames[0];
        initial = XLSX.utils.sheet_to_json(workBook.Sheets[sheet]);
        return initial;
      }, {});
      const dataString = JSON.stringify(jsonData);
      console.log("json data =" + dataString);
      this.store.dispatch(new UplodPurchaseHeader(jsonData));
      this.store
        .select(getPurchaseHeaderUploaded)
        .pipe(takeUntil(this.destroy))
        .subscribe((upload) => {
          this.reTry();
        });
    };
    console.log("excel data2 = " + this.data);
    reader.readAsBinaryString(file);
    // private uploadPurchaseHeader(Purchaseheader: PurchaseHeader): void {

    // this.store.dispatch(new UplodPurchaseHeader(this.purchase));
    //   this.store
    //     .select(getPurchaseHeaderUploaded)
    //     .pipe(takeUntil(this.destroy))
    //     .subscribe((upload) => {

    //     });
    // }
  }
  // reader.readAsBinaryString(file);

  // showToast(message:any,action:any,tostclass:any){
  //   this._snackBar.open(message,action, {
  //     duration: 3000,
  //     verticalPosition: 'top',
  //     horizontalPosition: 'end',
  //     panelClass: [tostclass]
  //   });
  // }
  // }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
