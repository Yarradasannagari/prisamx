import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';

import { InspectioncategoryService } from './inspection-category.service';

describe('UserService', () => {
  let service: InspectioncategoryService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InspectioncategoryService],
    });
    service = TestBed.inject(InspectioncategoryService
      
      );
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    // Finally, assert that there are no outstanding requests.
    httpMock.verify();
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
