import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Country } from "src/app/services/country/country.service";
import { CreateCountry, UpdateCountry } from "src/app/store/stores/country/country.actions";
import { getCountryCreated, getCountryUpdated } from "src/app/store/stores/country/country.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-country-form",
  templateUrl: "./country-form.component.html",
  styleUrls: ["./country-form.component.scss"],
})
export class CountryFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    cname: "",
    countryId: "",
    sname: "",
    stateId: "",
    active: true,
  };
  toppingList1: string[] = ["AP", "KA", "TG", "TN", "UP", "BR"];
  toppingList: string[] = ["Andra Pradesh", "Karnataka", "Telangana", "Tamil Nadu", "Uttar Pradesh", "Bihar"];

  country: Country;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.cname = this.route.snapshot.data["report"].cname;
        this.formData.countryId =
          this.route.snapshot.data["report"].countryId;
          this.formData.sname = this.route.snapshot.data["report"].sname;
          this.formData.stateId = this.route.snapshot.data["report"].stateId;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      cname: [
        this.formData.cname,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
          
        ]),
      ],
      countryId: [
        this.formData.countryId,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(2),
        ]),
      ],
      sname: [
        this.formData.sname,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
        ]),
      ],
      stateId: [
        this.formData.stateId,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const country: Country = {
      id: this.formData.id,
      cname: this.formData.cname,
      countryId: this.formData.countryId,
      sname: this.formData.sname,
      stateId: this.formData.stateId,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateCountry(country);
    } else {
      this.createCountry(country);
    }
  }
  private createCountry(country: Country): void {
    this.store.dispatch(new CreateCountry(country));
    this.store
      .select(getCountryCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateCountry(country: Country): void {
    this.store.dispatch(new UpdateCountry(country));
    this.store
      .select(getCountryUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "country"]);
  }
}
