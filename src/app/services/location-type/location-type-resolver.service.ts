import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Locationtype, LocationtypeService } from './location-type.service';

@Injectable({ providedIn: 'root' })
export class LocationtypeResolverService implements Resolve<Locationtype> {
  constructor(private locationtypeService: LocationtypeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Locationtype> | Promise<Locationtype> | Locationtype {
    const id = route.paramMap.get('id');
    return this.locationtypeService.get(id);
  }
}
