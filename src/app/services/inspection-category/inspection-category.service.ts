import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Inspectioncategory {
  id:string;
  active: boolean;
  catergoryId: string;
  name: string; 
}


@Injectable({
  providedIn: 'root',
})
export class InspectioncategoryService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Inspectioncategory> {
    return this.entitySvc.get(Url.inspectioncat, id);
  }

  list(params?: Record<string, any>): Observable<Inspectioncategory[]> {
    return this.entitySvc.list(Url.inspectioncat, params);
  }

  create(aclRule: Inspectioncategory): Observable<Inspectioncategory> {
    return this.entitySvc.create(Url.inspectioncat, aclRule);
  }

  update(aclRule: Inspectioncategory): Observable<Inspectioncategory> {
    return this.entitySvc.update(Url.inspectioncat, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectioncat, id);
  }
  
}
