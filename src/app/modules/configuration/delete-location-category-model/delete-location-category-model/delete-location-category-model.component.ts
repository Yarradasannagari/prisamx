import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteLocationcategory} from "src/app/store/stores/location-category/location-category.actions";
import { getLocationcategoryDeleted } from "src/app/store/stores/location-category/location-category.store";
@Component({
  selector: "app-delete-location-category-model",
  templateUrl: "./delete-location-category-model.component.html",
  styleUrls: ["./delete-location-category-model.component.scss"],
})
export class DeleteLocationCategoryModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteLocationcategory() {
    this.isLoading = true;
    this.store.dispatch(new DeleteLocationcategory(this.id));
    this.store
      .select(getLocationcategoryDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
