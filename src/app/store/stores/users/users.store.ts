import { createFeatureSelector, createSelector } from "@ngrx/store";
import { User } from "src/app/services/users/user.service";

import { State } from "../..";

import { UserTypes, Union } from "./users.actions";

export interface UserState {
  loading: boolean;
  entities: User[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  error: boolean;
}

export const INITIAL_STATE: UserState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  error: false,
  updating: false,
  updated: false,
};

export function UserReducer(
  state: UserState = INITIAL_STATE,
  action: Union
): UserState {
  switch (action.type) {
    case UserTypes.GetUsers:
      return {
        ...state,
        loading: true,
      };
    case UserTypes.GetUsersSuccess:
      const users: User[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: users,
      };
    case UserTypes.GetUsersError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case UserTypes.CreateUser:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case UserTypes.CreateUsersuccess:
      const createdUser: User = action.payload;
      const entitiesAppended = [...state.entities, createdUser];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
    case UserTypes.UpdateUser:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case UserTypes.UpdateUsersuccess:
      const updatedUser: User = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedUser];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
    case UserTypes.UpdateUserError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case UserTypes.CreateUserError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    default:
      return state;
  }
}

export const getUserState = createFeatureSelector<State, UserState>("user");

export const getUsersLoading = createSelector(
  getUserState,
  (state) => state.loading
);

export const getUsers = createSelector(getUserState, (state) => state.entities);

export const getUserCreating = createSelector(
  getUserState,
  (state) => state.creating
);

export const getUserCreated = createSelector(
  getUserState,
  (state) => state.created
);
export const getUserUpdating = createSelector(
  getUserState,
  (state) => state.updating
);

export const getUserUpdated = createSelector(
  getUserState,
  (state) => state.updated
);
