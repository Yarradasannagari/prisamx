import * as InspectioncategoryActions from "./inspection-category.actions";
import { InspectioncategoryEffects } from "./inspection-category.effects";
import { INITIAL_STATE, InspectioncategoryReducer, InspectioncategoryState } from "./inspection-category.store";

export { InspectioncategoryActions, InspectioncategoryReducer, InspectioncategoryState, INITIAL_STATE, InspectioncategoryEffects };
