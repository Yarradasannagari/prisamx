import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Inspectionclass } from "src/app/services/inspection-class/inspection-class.service";
import { CreateInspectionclass, UpdateInspectionclass } from "src/app/store/stores/inspection-class/inspection-class.actions";
import { getInspectionclassCreated, getInspectionclassUpdated } from "src/app/store/stores/inspection-class/inspection-class.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-inspection-class-form",
  templateUrl: "./inspection-class-form.component.html",
  styleUrls: ["./inspection-class-form.component.scss"],
})
export class InspectionClassFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    classId: "",
    name: "",
    active: true,
  };
  inspectionclass: Inspectionclass;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.classId = this.route.snapshot.data["report"].classId;
        this.formData.name =
          this.route.snapshot.data["report"].name;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      classId: [
        this.formData.classId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(30),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectionclass: Inspectionclass = {
      id: this.formData.id,
      classId: this.formData.classId,
      name: this.formData.name,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectionclass(inspectionclass);
    } else {
      this.createInspectionclass(inspectionclass);
    }
  }
  private createInspectionclass(inspectionclass: Inspectionclass): void {
    this.store.dispatch(new CreateInspectionclass(inspectionclass));
    this.store
      .select(getInspectionclassCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectionclass(inspectionclass: Inspectionclass): void {
    this.store.dispatch(new UpdateInspectionclass(inspectionclass));
    this.store
      .select(getInspectionclassUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "inspection-class"]);
  }
}
