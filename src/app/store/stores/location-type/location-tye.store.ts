import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Locationtype } from "src/app/services/location-type/location-type.service";

import { State } from "../..";

import { LocationtypeTypes, Union } from "./location-type.actions";

export interface LocationtypeState {
  loading: boolean;
  entities: Locationtype[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: LocationtypeState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function LocationtypeReducer(
  state: LocationtypeState = INITIAL_STATE,
  action: Union
): LocationtypeState {
  switch (action.type) {
    case LocationtypeTypes.GetLocationtype:
      return {
        ...state,
        loading: true,
      };
    case LocationtypeTypes.GetLocationtypeSuccess:
      const Locationtype: Locationtype[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: Locationtype,
      };
    case LocationtypeTypes.GetLocationtypeError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case LocationtypeTypes.CreateLocationtype:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case LocationtypeTypes.CreateLocationtypesuccess:
      const createdLocationtype: Locationtype = action.payload;
      const entitiesAppended = [...state.entities, createdLocationtype];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case LocationtypeTypes.UpdateLocationtype:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case LocationtypeTypes.UpdateLocationtypesuccess:
      const updatedLocationtype: Locationtype = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedLocationtype];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case LocationtypeTypes.UpdateLocationtypeError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case LocationtypeTypes.CreateLocationtypeError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case LocationtypeTypes.DeleteLocationtype:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case LocationtypeTypes.DeleteLocationtypesuccess:
      const afterDelete = state.entities.filter((Locationtype) => Locationtype.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case LocationtypeTypes.DeleteLocationtypeError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getLocationtypeState = createFeatureSelector<State, LocationtypeState>("locationtype");

export const getLocationtypeLoading = createSelector(
  getLocationtypeState,
  (state) => state.loading
);

export const getLocationtype = createSelector(getLocationtypeState, (state) => state.entities);

export const getLocationtypeCreating = createSelector(
  getLocationtypeState,
  (state) => state.creating
);

export const getLocationtypeCreated = createSelector(
  getLocationtypeState,
  (state) => state.created
);
export const getLocationtypeUpdating = createSelector(
  getLocationtypeState,
  (state) => state.updating
);

export const getLocationtypeUpdated = createSelector(
  getLocationtypeState,
  (state) => state.updated
);
export const getLocationtypeDeleting = createSelector(
  getLocationtypeState,
  (state) => state.deleting
);

export const getLocationtypeDeleted = createSelector(
  getLocationtypeState,
  (state) => state.deleted
);

export const getLocationtypeError = createSelector(
  getLocationtypeState,
  (state) => state.error
);
