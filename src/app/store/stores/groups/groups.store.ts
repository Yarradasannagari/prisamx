import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Group } from "src/app/services/groups/group.service";

import { State } from "../..";

import { GroupTypes, Union } from "./groups.actions";

export interface GroupState {
  loading: boolean;
  entities: Group[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: GroupState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function GroupReducer(
  state: GroupState = INITIAL_STATE,
  action: Union
): GroupState {
  switch (action.type) {
    case GroupTypes.GetGroups:
      return {
        ...state,
        loading: true,
      };
    case GroupTypes.GetGroupsSuccess:
      const groups: Group[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: groups,
      };
    case GroupTypes.GetGroupsError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case GroupTypes.CreateGroup:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case GroupTypes.CreateGroupsuccess:
      const createdGroup: Group = action.payload;
      const entitiesAppended = [...state.entities, createdGroup];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
    case GroupTypes.UpdateGroup:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case GroupTypes.UpdateGroupsuccess:
      const updatedGroup: Group = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedGroup];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
    case GroupTypes.UpdateGroupError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case GroupTypes.CreateGroupError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case GroupTypes.DeleteGroup:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case GroupTypes.DeleteGroupsuccess:
      const afterDelete = state.entities.filter(
        (group) => group.id !== action.payload
      );
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities: afterDelete,
      };

    case GroupTypes.DeleteGroupError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getGroupState = createFeatureSelector<State, GroupState>("group");

export const getGroupsLoading = createSelector(
  getGroupState,
  (state) => state.loading
);

export const getGroups = createSelector(
  getGroupState,
  (state) => state.entities
);

export const getGroupCreating = createSelector(
  getGroupState,
  (state) => state.creating
);

export const getGroupCreated = createSelector(
  getGroupState,
  (state) => state.created
);
export const getGroupUpdating = createSelector(
  getGroupState,
  (state) => state.updating
);

export const getGroupUpdated = createSelector(
  getGroupState,
  (state) => state.updated
);
export const getGroupDeleting = createSelector(
  getGroupState,
  (state) => state.deleting
);

export const getGroupDeleted = createSelector(
  getGroupState,
  (state) => state.deleted
);

export const getGroupError = createSelector(
  getGroupState,
  (state) => state.error
);
