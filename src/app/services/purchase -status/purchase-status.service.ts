import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface PurchaseStatus {
  // active: boolean;

  id: string;
  trnid: string;
  plant: string;
  stroom: string;
  gvalue: string;
  navlue: string;
  grtid: string;
  matName: string;
  material: string;
  quantity: string;
  vendorName: string;
 

}


@Injectable({
  providedIn: 'root',
})
export class PurchaseStatusService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<PurchaseStatus> {
    return this.entitySvc.get(Url.otfitems, id);
  }

  list(params?: Record<string, any>): Observable<PurchaseStatus[]> {
    return this.entitySvc.list(Url.otfitems, params);
  }

  create(aclRule: PurchaseStatus): Observable<PurchaseStatus> {
    return this.entitySvc.create(Url.otfitems, aclRule); 
  }

  update(aclRule: PurchaseStatus): Observable<PurchaseStatus> {
    return this.entitySvc.update(Url.otfitems, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.otfitems, id);
  }
  
}
