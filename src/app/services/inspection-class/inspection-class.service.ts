import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Inspectionclass {
  id:string;
  active: boolean;
  classId: string;
  name: string;
}


@Injectable({
  providedIn: 'root',
})
export class InspectionclassService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Inspectionclass> {
    return this.entitySvc.get(Url.inspectionclass, id);
  }

  list(params?: Record<string, any>): Observable<Inspectionclass[]> {
    return this.entitySvc.list(Url.inspectionclass, params);
  }

  create(aclRule: Inspectionclass): Observable<Inspectionclass> {
    return this.entitySvc.create(Url.inspectionclass, aclRule);
  }

  update(aclRule: Inspectionclass): Observable<Inspectionclass> {
    return this.entitySvc.update(Url.inspectionclass, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectionclass, id);
  }
  
}
