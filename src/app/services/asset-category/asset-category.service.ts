import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Assetcategory {
  id:string;
  active: boolean;
  category: string;
  name: string;
}


@Injectable({
  providedIn: 'root',
})
export class AssetcategoryService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Assetcategory> {
    return this.entitySvc.get(Url.assetcat, id);
  }

  list(params?: Record<string, any>): Observable<Assetcategory[]> {
    return this.entitySvc.list(Url.assetcat, params);
  }

  create(aclRule: Assetcategory): Observable<Assetcategory> {
    return this.entitySvc.create(Url.assetcat, aclRule);
  }

  update(aclRule: Assetcategory): Observable<Assetcategory> {
    return this.entitySvc.update(Url.assetcat, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.assetcat, id);
  }
  
}
