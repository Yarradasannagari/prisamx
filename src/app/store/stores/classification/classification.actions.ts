import { Action } from "@ngrx/store";
import { Classification } from "../../../services/classification/classification.service";

export enum ClassificationTypes {
  GetClassification = "[Classification] GetClassification",
  GetClassificationSuccess = "[Classification] GetClassificationSuccess",
  GetClassificationError = "[Classification] GetClassificationError",
  CreateClassification = "[Classification] CreateClassification",
  CreateClassificationsuccess = "[Classification] CreateClassificationsuccess",
  CreateClassificationError = "[Classification] CreateClassificationError",
  UpdateClassification = "[Classification] UpdateClassification",
  UpdateClassificationsuccess = "[Classification] UpdateClassificationsuccess",
  UpdateClassificationError = "[Classification] UpdateClassificationError",
  DeleteClassification = "[Classification] DeleteClassification",
  DeleteClassificationError = "[Classification] DeleteClassificationError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteClassificationsuccess = "[AclRule] DeleteClassificationsuccess",
}

export class GetClassification implements Action { 
  readonly type = ClassificationTypes.GetClassification;
}

export class GetClassificationSuccess implements Action {
  readonly type = ClassificationTypes.GetClassificationSuccess;
  constructor(readonly payload: Classification[]) {}
}
export class GetClassificationError implements Action {
  readonly type = ClassificationTypes.GetClassificationError;
  constructor(readonly payload: boolean) {}
}

export class CreateClassification implements Action {
  readonly type = ClassificationTypes.CreateClassification;
  constructor(readonly payload: Classification) {}
}

export class CreateClassificationsuccess implements Action {
  readonly type = ClassificationTypes.CreateClassificationsuccess;
  constructor(readonly payload: Classification) {}
}

export class CreateClassificationError implements Action {
  readonly type = ClassificationTypes.CreateClassificationError;
}
export class UpdateClassification implements Action {
  readonly type = ClassificationTypes.UpdateClassification;
  constructor(readonly payload: Classification) {}
}

export class UpdateClassificationsuccess implements Action {
  readonly type = ClassificationTypes.UpdateClassificationsuccess;
  constructor(readonly payload: Classification) {}
}

export class UpdateClassificationError implements Action {
  readonly type = ClassificationTypes.UpdateClassificationError;
}

export class DeleteClassification implements Action {
  readonly type = ClassificationTypes.DeleteClassification;
  constructor(readonly payload: string) {}
}

export class DeleteClassificationsuccess implements Action {
  readonly type = ClassificationTypes.DeleteClassificationsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteClassificationError implements Action {
  readonly type = ClassificationTypes.DeleteClassificationError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetClassification
  | GetClassificationSuccess
  | GetClassificationError
  | CreateClassification
  | CreateClassificationsuccess
  | CreateClassificationError
  | DeleteClassification
  | DeleteClassificationsuccess
  | DeleteClassificationError
  | UpdateClassification
  | UpdateClassificationsuccess
  | UpdateClassificationError
