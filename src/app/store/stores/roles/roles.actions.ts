import { Action } from "@ngrx/store";
import { Role } from "../../../services/roles/role.service";

export enum RoleTypes {
  GetRoles = "[Role] GetRoles",
  GetRolesSuccess = "[Role] GetRolesSuccess",
  GetRolesError = "[Role] GetRolesError",
  CreateRole = "[Role] CreateRole",
  CreateRolesuccess = "[Role] CreateRolesuccess",
  CreateRoleError = "[Role] CreateRoleError",
  UpdateRole = "[Role] UpdateRole",
  UpdateRolesuccess = "[Role] UpdateRolesuccess",
  UpdateRoleError = "[Role] UpdateRoleError",
  DeleteRole = "[Role] DeleteRole",
  DeleteRoleError = "[Role] DeleteRoleError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteRolesuccess = "[AclRule] DeleteRolesuccess",
}

export class GetRoles implements Action {
  readonly type = RoleTypes.GetRoles;
}

export class GetRolesSuccess implements Action {
  readonly type = RoleTypes.GetRolesSuccess;
  constructor(readonly payload: Role[]) {}
}
export class GetRolesError implements Action {
  readonly type = RoleTypes.GetRolesError;
  constructor(readonly payload: boolean) {}
}

export class CreateRole implements Action {
  readonly type = RoleTypes.CreateRole;
  constructor(readonly payload: Role) {}
}

export class CreateRolesuccess implements Action {
  readonly type = RoleTypes.CreateRolesuccess;
  constructor(readonly payload: Role) {}
}

export class CreateRoleError implements Action {
  readonly type = RoleTypes.CreateRoleError;
}
export class UpdateRole implements Action {
  readonly type = RoleTypes.UpdateRole;
  constructor(readonly payload: Role) {}
}

export class UpdateRolesuccess implements Action {
  readonly type = RoleTypes.UpdateRolesuccess;
  constructor(readonly payload: Role) {}
}

export class UpdateRoleError implements Action {
  readonly type = RoleTypes.UpdateRoleError;
}

export class DeleteRole implements Action {
  readonly type = RoleTypes.DeleteRole;
  constructor(readonly payload: string) {}
}

export class DeleteRolesuccess implements Action {
  readonly type = RoleTypes.DeleteRolesuccess;
  constructor(readonly payload: string) {}
}

export class DeleteRoleError implements Action {
  readonly type = RoleTypes.DeleteRoleError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetRoles
  | GetRolesSuccess
  | GetRolesError
  | CreateRole
  | CreateRolesuccess
  | CreateRoleError
  | DeleteRole
  | DeleteRolesuccess
  | DeleteRoleError
  | UpdateRole
  | UpdateRolesuccess
  | UpdateRoleError
