import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Inspectionclass, InspectionclassService } from "src/app/services/inspection-class/inspection-class.service";
import { GetInspectionclass } from "src/app/store/stores/inspection-class/inspection-class.actions";
import {
  getInspectionclassError,
  getInspectionclass,
  getInspectionclassLoading,
} from "src/app/store/stores/inspection-class/inspection-class.store";
import { State, Store } from "../../../store";
import { DeleteInspectionClassModelComponent } from "../delete-inspection-class-model/delete-inspection-class-model/delete-inspection-class-model.component";

@Component({
  selector: "app-inspection-class",
  templateUrl: "./inspection-class.component.html",
  styleUrls: ["./inspection-class.component.scss"],
})
export class InspectionClassComponent implements OnDestroy, OnInit {
  inspectionclass$: Observable<Inspectionclass[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  inspectionclass: Inspectionclass[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private inspectionclassService: InspectionclassService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectionclassLoading);
    this.store.dispatch(new GetInspectionclass());
    this.inspectionclass$ = this.store.select(getInspectionclass);
    this.store.select(getInspectionclassError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }

  reTry() {
    this.isLoading$ = this.store.select(getInspectionclassLoading);
    this.store.dispatch(new GetInspectionclass());
    this.inspectionclass$ = this.store.select(getInspectionclass);
    this.store.select(getInspectionclassError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionClassModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
