import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { InspectionPointsComponent } from "./inspection-points.component";

import { InspectionPointComponent } from "./inspection-point/inspection-point.component";
import { InspectionPointFormComponent } from "./inspection-point-form/inspection-point-form.component";
import { InspectionPointResolverService } from "src/app/services/inspection-point/inspection-point-resolver.service";
import { ViewInspectionPointComponent } from "./view-inspection-point/view-inspection-point.component";

const routes: Routes = [
  {
    path: "",
    component: InspectionPointsComponent,
    children: [
      {
        path: "inspection-point",
        component: InspectionPointComponent,
      },
      {
        path: "add-inspection-point",
        component: InspectionPointFormComponent,
      },
      {
        path: ":id/edit-inspection-point",
        component: InspectionPointFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectionPointResolverService,
        },
      },
      {
        path: ":id/view-inspection-point",
        component: ViewInspectionPointComponent,
        pathMatch: "full",
        resolve: {
          report: InspectionPointResolverService,
        },
      },
     
      { path: "", redirectTo: "inspection-point", pathMatch: "full" },
      { path: "**", redirectTo: "inspection-point", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InspectionPointRoutingModule {}
