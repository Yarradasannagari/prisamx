import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeletePurchase } from "src/app/store/stores/purchase/purchase.actions";
import { getPurchaseDeleted } from "src/app/store/stores/purchase/purchase.store";
@Component({
  selector: "app-delete-purchase-model",
  templateUrl: "./delete-purchase-model.component.html",
  styleUrls: ["./delete-purchase-model.component.scss"],
})
export class DeletePurchaseModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deletePurchase() {
    this.isLoading = true;
    this.store.dispatch(new DeletePurchase(this.id));
    this.store
      .select(getPurchaseDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
