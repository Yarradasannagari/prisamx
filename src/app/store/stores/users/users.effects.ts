import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import {
  CreateUser,
  CreateUserError,
  CreateUsersuccess,
  GetUsers,
  GetUsersError,
  GetUsersSuccess,
  UpdateUser,
  UpdateUserError,
  UpdateUsersuccess,
  UserTypes,
} from "./users.actions";
import { UserService } from "src/app/services/users/user.service";

@Injectable()
export class UserEffects {
  constructor(
    private actions: Actions,
    private userService: UserService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getUsers$: Observable<Action> = this.actions.pipe(
    ofType<GetUsers>(UserTypes.GetUsers),
    
    exhaustMap(() =>
      from(this.userService.list()).pipe(
        map((payload) => {
          console.log("user Effect")
          return new GetUsersSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve User", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetUsersError(true));
        })
      )
    )
  );

  @Effect()
  createUser$: Observable<Action> = this.actions.pipe(
    ofType<CreateUser>(UserTypes.CreateUser),
    concatMap((action) =>
      from(this.userService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The User has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateUsersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create User", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateUserError());
        })
      )
    )
  );
  @Effect()
  updateUser$: Observable<Action> = this.actions.pipe(
    ofType<UpdateUser>(UserTypes.UpdateUser),
    concatMap((action) =>
      from(this.userService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Role has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateUsersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update User", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateUserError());
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
