import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { InspectioncodeService } from "../../../services/inspection-code/inspection-code.service";  

import {
  GetInspectioncode,
  GetInspectioncodeSuccess,
  GetInspectioncodeError,
  CreateInspectioncode,
  CreateInspectioncodesuccess,
  CreateInspectioncodeError,
  DeleteInspectioncode,
  DeleteInspectioncodesuccess,
  DeleteInspectioncodeError,
  InspectioncodeTypes,
  UpdateInspectioncode,
  UpdateInspectioncodesuccess,
  UpdateInspectioncodeError,
} from "./inspection-code.actions";

@Injectable()
export class InspectioncodeEffects {
  constructor(
    private actions: Actions,
    private inspectioncodeService: InspectioncodeService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectioncode$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectioncode>(InspectioncodeTypes.GetInspectioncode),
    exhaustMap(() =>
      from(this.inspectioncodeService.list()).pipe(
        map((payload) => {
          return new GetInspectioncodeSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Code", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectioncodeError(true));
        })
      )
    )
  );

  @Effect()
  createInspectioncode$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectioncode>(InspectioncodeTypes.CreateInspectioncode),
    concatMap((action) =>
      from(this.inspectioncodeService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Code has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectioncodesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Code", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectioncodeError());
        })
      )
    )
  );
  @Effect()
  updateInspectioncode$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectioncode>(InspectioncodeTypes.UpdateInspectioncode),
    concatMap((action) =>
      from(this.inspectioncodeService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Code has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectioncodesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Code", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectioncodeError());
        })
      )
    )
  );
  @Effect()
  deleteInspectioncode$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectioncode>(InspectioncodeTypes.DeleteInspectioncode),
    concatMap((action) =>
      from(this.inspectioncodeService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Code has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectioncodesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Code:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectioncodeError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
