import { Action } from "@ngrx/store";
import { User } from "src/app/services/users/user.service";

export enum UserTypes {
  GetUsers = "[User] GetUsers",
  GetUsersSuccess = "[User] GetUsersSuccess",
  GetUsersError = "[User] GetUsersError",
  CreateUser = "[User] CreateUser",
  CreateUsersuccess = "[User] CreateUsersuccess",
  CreateUserError = "[User] CreateUserError",
  UpdateUser = "[User] UpdateUser",
  UpdateUsersuccess = "[User] UpdateUsersuccess",
  UpdateUserError = "[User] UpdateUserError",
}

export class GetUsers implements Action {
  readonly type = UserTypes.GetUsers;
}

export class GetUsersSuccess implements Action {
  readonly type = UserTypes.GetUsersSuccess;
  constructor(readonly payload: User[]) {
    console.log("Get users success action");
  }
}
export class GetUsersError implements Action {
  readonly type = UserTypes.GetUsersError;
  constructor(readonly payload: boolean) {}
}

export class CreateUser implements Action {
  readonly type = UserTypes.CreateUser;
  constructor(readonly payload: User) {}
}

export class CreateUsersuccess implements Action {
  readonly type = UserTypes.CreateUsersuccess;
  constructor(readonly payload: User) {}
}

export class CreateUserError implements Action {
  readonly type = UserTypes.CreateUserError;
}
export class UpdateUser implements Action {
  readonly type = UserTypes.UpdateUser;
  constructor(readonly payload: User) {}
}

export class UpdateUsersuccess implements Action {
  readonly type = UserTypes.UpdateUsersuccess;
  constructor(readonly payload: User) {}
}

export class UpdateUserError implements Action {
  readonly type = UserTypes.UpdateUserError;
}

export type Union =
  | GetUsers
  | GetUsersSuccess
  | GetUsersError
  | CreateUser
  | CreateUsersuccess
  | CreateUserError
  | UpdateUser
  | UpdateUsersuccess
  | UpdateUserError;
