import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Partner } from "src/app/services/partner/partner.service";

import { State } from "../..";

import { PartnerTypes, Union } from "./partner.actions";

export interface PartnerState {
  loading: boolean;
  entities: Partner[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: PartnerState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function PartnerReducer(
  state: PartnerState = INITIAL_STATE,
  action: Union
): PartnerState {
  switch (action.type) {
    case PartnerTypes.GetPartner:
      return {
        ...state,
        loading: true,
      };
    case PartnerTypes.GetPartnerSuccess:
      const partner: Partner[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: partner,
      };
    case PartnerTypes.GetPartnerError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case PartnerTypes.CreatePartner:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case PartnerTypes.CreatePartnersuccess:
      const createdPartner: Partner = action.payload;
      const entitiesAppended = [...state.entities, createdPartner];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case PartnerTypes.UpdatePartner:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case PartnerTypes.UpdatePartnersuccess:
      const updatedPartner: Partner = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedPartner];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case PartnerTypes.UpdatePartnerError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case PartnerTypes.CreatePartnerError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case PartnerTypes.DeletePartner:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case PartnerTypes.DeletePartnersuccess:
      const afterDelete = state.entities.filter((partner) => partner.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case PartnerTypes.DeletePartnerError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getPartnerState = createFeatureSelector<State, PartnerState>("partner");

export const getPartnerLoading = createSelector(
  getPartnerState,
  (state) => state.loading
);

export const getPartner = createSelector(getPartnerState, (state) => state.entities);

export const getPartnerCreating = createSelector(
  getPartnerState,
  (state) => state.creating
);

export const getPartnerCreated = createSelector(
  getPartnerState,
  (state) => state.created
);
export const getPartnerUpdating = createSelector(
  getPartnerState,
  (state) => state.updating
);

export const getPartnerUpdated = createSelector(
  getPartnerState,
  (state) => state.updated
);
export const getPartnerDeleting = createSelector(
  getPartnerState,
  (state) => state.deleting
);

export const getPartnerDeleted = createSelector(
  getPartnerState,
  (state) => state.deleted
);

export const getPartnerError = createSelector(
  getPartnerState,
  (state) => state.error
);
