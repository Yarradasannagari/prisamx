import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionClassFormComponent } from './inspection-class-form.component';

describe('InspectionClassFormComponent', () => {
  let component: InspectionClassFormComponent;
  let fixture: ComponentFixture<InspectionClassFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionClassFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionClassFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
