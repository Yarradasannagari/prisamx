import * as InspectionclassActions from "./inspection-class.actions";
import { InspectionclassEffects } from "./inspection-class.effects";
import { INITIAL_STATE, InspectionclassReducer, InspectionclassState } from "./inspection-class.store";

export { InspectionclassActions, InspectionclassReducer, InspectionclassState, INITIAL_STATE, InspectionclassEffects };
