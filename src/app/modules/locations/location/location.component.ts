import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Location, LocationService } from "src/app/services/locations/location.service";
import { GetLocation } from "src/app/store/stores/locations/location.actions";
import {getLocationError, getLocation,getLocationLoading} from "src/app/store/stores/locations/location.store";
import { State, Store } from "../../../store";
import { DeleteLocationModelComponent } from "../delete-location-model/delete-location-model/delete-location-model.component";

@Component({
  selector: "app-location",
  templateUrl: "./location.component.html",
  styleUrls: ["./location.component.scss"],
})
export class LocationComponent implements OnDestroy, OnInit {
  closeResult: string;
  location$: Observable<Location[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  location: Location[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private locationService: LocationService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getLocationLoading);
    this.store.dispatch(new GetLocation());
    this.location$ = this.store.select(getLocation);
    this.store.select(getLocationError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }  
  
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getLocationLoading);
    this.store.dispatch(new GetLocation());
    this.location$ = this.store.select(getLocation);
    this.store.select(getLocationError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteLocationModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }


  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
