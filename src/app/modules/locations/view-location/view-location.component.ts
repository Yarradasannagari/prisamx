import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Location } from "src/app/services/locations/location.service";
import { CreateLocation, UpdateLocation } from "src/app/store/stores/locations/location.actions";
import { getLocationCreated, getLocationUpdated } from "src/app/store/stores/locations/location.store";
import { State, Store } from "../../../store";

import { InspectionPoint, InspectionPointService } from "src/app/services/inspection-point/inspection-point.service";
import { GetInspectionPoint1 } from "src/app/store/stores/inspection-point/inspection-point.actions";
import { getInspectionPoint } from "src/app/store/stores/inspection-point/inspection-point.store";

@Component({
  selector: "app-view-location",
  templateUrl: "./view-location.component.html",
  styleUrls: ["./view-location.component.scss"],
})
export class ViewLocationComponent implements OnInit {
  routeId: any;
  isLoading = false;
  inspectionPoint: InspectionPoint[];
  formGroup: FormGroup;
  formData = {
    id: null,
    locationId: "", 
    // name: "",
    // category: "",
    // type: "",
    // plant: "",
    status: "",
    startPoint: "",
    // strno: "",
    pltxt: "",
    typtx: "",
    eartx: "",
    swerk: "",
    points: "",
    latitude: "",
    longitude: "",
    addr1: "",
    addr2: "",
    addr3: "",
    landMark: "",
    zipcode: "",
    city: "",
    regoin: "",
    country: "",
    // abckz: "",
    // aedat: "",
    // arbpl: "",
    // arbplname: "",
    // baujj: "",
    // baumm: "",
    // beber: "",
    // bukrs: "",
    // changedBy: "",

    point: "",
    position: "",
    // name: "",
    objtyp: "",
    catergory: "",

    active: true,
  };
  location_id: any;
  location: Location;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private inspectionPointService: InspectionPointService
  ) {
    this.store.dispatch(new GetInspectionPoint1(this.location_id));
    this.store.select(getInspectionPoint).subscribe((inspectionPoint) => {
      this.inspectionPoint = inspectionPoint;
    });
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.locationId = this.route.snapshot.data["report"].locationId;
        // this.formData.name =  this.route.snapshot.data["report"].name;
        // this.formData.category =  this.route.snapshot.data["report"].category;
        // this.formData.type =  this.route.snapshot.data["report"].type;
        // this.formData.plant =  this.route.snapshot.data["report"].plant;
        this.formData.status =  this.route.snapshot.data["report"].status;
        this.formData.startPoint =  this.route.snapshot.data["report"].startPoint;
        // this.formData.strno =  this.route.snapshot.data["report"].strno;
        this.formData.pltxt =  this.route.snapshot.data["report"].pltxt;
        this.formData.typtx =  this.route.snapshot.data["report"].typtx;
        this.formData.eartx =  this.route.snapshot.data["report"].eartx;
        this.formData.swerk =  this.route.snapshot.data["report"].swerk;
        // this.formData.points =  this.route.snapshot.data["report"].points;
        this.formData.latitude =  this.route.snapshot.data["report"].latitude;
        this.formData.longitude =  this.route.snapshot.data["report"].longitude;
        this.formData.addr1 =  this.route.snapshot.data["report"].addr1;
        this.formData.addr2 =  this.route.snapshot.data["report"].addr2;
        this.formData.addr3 =  this.route.snapshot.data["report"].addr3;
        this.formData.landMark =  this.route.snapshot.data["report"].landMark;
        this.formData.zipcode =  this.route.snapshot.data["report"].zipcode;
        this.formData.city =  this.route.snapshot.data["report"].city;
        this.formData.regoin =  this.route.snapshot.data["report"].regoin;
        this.formData.country =  this.route.snapshot.data["report"].country;

        // this.formData.abckz =  this.route.snapshot.data["report"].abckz;
        // this.formData.aedat =  this.route.snapshot.data["report"].aedat;
        // this.formData.arbpl =  this.route.snapshot.data["report"].arbpl;
        // this.formData.arbplname =  this.route.snapshot.data["report"].arbplname;
        // this.formData.baujj =  this.route.snapshot.data["report"].baujj;
        // this.formData.baumm =  this.route.snapshot.data["report"].baumm;
        // this.formData.beber =  this.route.snapshot.data["report"].beber;
        // this.formData.bukrs =  this.route.snapshot.data["report"].bukrs;

        // this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      locationId: [
        this.formData.locationId,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      // name: [
      //   this.formData.name,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(2),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // category: [
      //   this.formData.category,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // type: [
      //   this.formData.type,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // plant: [
      //   this.formData.plant,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(1),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      status: [
        this.formData.status,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      startPoint: [
        this.formData.startPoint,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      // strno: [
      //   this.formData.strno,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      pltxt: [
        this.formData.pltxt,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      typtx: [
        this.formData.typtx,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      eartx: [
        this.formData.eartx,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      swerk: [
        this.formData.swerk,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      // points: [
      //   this.formData.points,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      latitude: [
        this.formData.latitude,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      longitude: [
        this.formData.longitude,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      addr1: [
        this.formData.addr1,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      addr2: [
        this.formData.addr2,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      addr3: [
        this.formData.addr3,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      landMark: [
        this.formData.landMark,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      zipcode: [
        this.formData.zipcode,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      city: [
        this.formData.city,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      regoin: [
        this.formData.regoin,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      country: [
        this.formData.country,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],

      // abckz: [
      //   this.formData.abckz,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // aedat: [
      //   this.formData.aedat,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // arbpl: [
      //   this.formData.arbpl,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // arbplname: [
      //   this.formData.arbplname,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // baujj: [
      //   this.formData.baujj,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // baumm: [
      //   this.formData.baumm,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // beber: [
      //   this.formData.beber,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // bukrs: [
      //   this.formData.bukrs,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(4),
      //     Validators.maxLength(255),
      //   ]),
      // ],
      // active: [this.formData.active],
    });
  }
  submit() {
    const location: Location = {
      id: this.formData.id,
      locationId: this.formData.locationId,
      // name: this.formData.name,
      // category: this.formData.category,
      // type: this.formData.type,
      // plant: this.formData.plant,
      status: this.formData.status,
      startPoint: this.formData.startPoint,
      // strno: this.formData.strno,
      pltxt: this.formData.pltxt,
      typtx: this.formData.typtx,
      eartx: this.formData.eartx,
      swerk: this.formData.swerk,
      // points: this.formData.points,
      latitude: this.formData.latitude,
      longitude: this.formData.longitude,
      addr1: this.formData.addr1,
      addr2: this.formData.addr2,
      addr3: this.formData.addr3,
      landMark: this.formData.landMark,
      zipcode: this.formData.zipcode,
      city: this.formData.city,
      regoin: this.formData.addr3,
      country: this.formData.country,
      // abckz: this.formData.abckz,
      // aedat: this.formData.aedat,
      // arbpl: this.formData.arbpl,
      // arbplname: this.formData.arbplname,
      // baujj: this.formData.baujj,
      // baumm: this.formData.baumm,
      // beber: this.formData.beber,
      // bukrs: this.formData.bukrs,
     
      // active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateLocation(location);
    } else {
      this.createLocation(location);
    }
  }
  private createLocation(location: Location): void {
    this.store.dispatch(new CreateLocation(location));
    this.store
      .select(getLocationCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateLocation(location: Location): void {
    this.store.dispatch(new UpdateLocation(location));
    this.store
      .select(getLocationUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["locations", "location"]);
  }
}
