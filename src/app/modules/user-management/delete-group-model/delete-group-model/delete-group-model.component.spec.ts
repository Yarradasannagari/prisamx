import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteGroupModelComponent } from './delete-group-model.component';

describe('DeleteGroupModelComponent', () => {
  let component: DeleteGroupModelComponent;
  let fixture: ComponentFixture<DeleteGroupModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteGroupModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteGroupModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
