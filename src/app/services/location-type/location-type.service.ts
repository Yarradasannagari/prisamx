import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Locationtype {
  id:string;
  active: boolean;
  name: string;
  type: string;
}


@Injectable({
  providedIn: 'root',
})
export class LocationtypeService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Locationtype> {
    return this.entitySvc.get(Url.locationtype, id);
  }

  list(params?: Record<string, any>): Observable<Locationtype[]> {
    return this.entitySvc.list(Url.locationtype, params);
  }

  create(aclRule: Locationtype): Observable<Locationtype> {
    return this.entitySvc.create(Url.locationtype, aclRule);
  }

  update(aclRule: Locationtype): Observable<Locationtype> {
    return this.entitySvc.update(Url.locationtype, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.locationtype, id);
  }
  
}
