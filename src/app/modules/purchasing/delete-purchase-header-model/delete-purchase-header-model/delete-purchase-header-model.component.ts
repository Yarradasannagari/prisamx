import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeletePurchaseHeader } from "src/app/store/stores/purchase -header/purchase-header.actions";
import { getPurchaseHeaderDeleted } from "src/app/store/stores/purchase -header/purchase-header.store";
@Component({
  selector: "app-delete-purchase-header-model",
  templateUrl: "./delete-purchase-header-model.component.html",
  styleUrls: ["./delete-purchase-header-model.component.scss"],
})
export class DeletePurchaseHeaderModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deletePurchaseHeader() {
    this.isLoading = true; 
    this.store.dispatch(new DeletePurchaseHeader(this.id));
    this.store
      .select(getPurchaseHeaderDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
