import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCountryModelComponent } from './delete-country-model.component';

describe('DeleteCountryModelComponent', () => {
  let component: DeleteCountryModelComponent;
  let fixture: ComponentFixture<DeleteCountryModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCountryModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCountryModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
