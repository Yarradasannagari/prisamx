import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionGroupFormComponent } from './inspection-group-form.component';

describe('InspectionGroupFormComponent', () => {
  let component: InspectionGroupFormComponent;
  let fixture: ComponentFixture<InspectionGroupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionGroupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
