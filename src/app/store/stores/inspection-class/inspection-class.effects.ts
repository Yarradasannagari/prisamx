import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { InspectionclassService } from "../../../services/inspection-class/inspection-class.service";  

import {
  GetInspectionclass,
  GetInspectionclassSuccess,
  GetInspectionclassError,
  CreateInspectionclass,
  CreateInspectionclasssuccess,
  CreateInspectionclassError,
  DeleteInspectionclass,
  DeleteInspectionclasssuccess,
  DeleteInspectionclassError,
  InspectionclassTypes,
  UpdateInspectionclass,
  UpdateInspectionclasssuccess,
  UpdateInspectionclassError,
} from "./inspection-class.actions";

@Injectable()
export class InspectionclassEffects {
  constructor(
    private actions: Actions,
    private inspectionclassService: InspectionclassService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectionclass$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectionclass>(InspectionclassTypes.GetInspectionclass),
    exhaustMap(() =>
      from(this.inspectionclassService.list()).pipe(
        map((payload) => {
          return new GetInspectionclassSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Class", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectionclassError(true));
        })
      )
    )
  );

  @Effect()
  createInspectionclass$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectionclass>(InspectionclassTypes.CreateInspectionclass),
    concatMap((action) =>
      from(this.inspectionclassService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Class has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectionclasssuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Class", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectionclassError());
        })
      )
    )
  );
  @Effect()
  updateInspectionclass$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectionclass>(InspectionclassTypes.UpdateInspectionclass),
    concatMap((action) =>
      from(this.inspectionclassService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Class has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectionclasssuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Class", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectionclassError());
        })
      )
    )
  );
  @Effect()
  deleteInspectionclass$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectionclass>(InspectionclassTypes.DeleteInspectionclass),
    concatMap((action) =>
      from(this.inspectionclassService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Class has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectionclasssuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Class:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectionclassError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
