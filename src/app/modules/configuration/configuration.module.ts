import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2OrderModule } from 'ng2-order-pipe';

import { WidgetsModule } from '../../_metronic/partials/content/widgets/widgets.module';
// import { OrganizationComponent } from "./organization/organization.component";
import { OrganizationComponent } from "./organization/organization.component";
import { ConfigurationComponent } from "./configuration.component";
import { ConfigurationRoutingModule } from "./configuration-routing.module";
import { OrganizationFormComponent } from "./organization-form/organization-form.component";
import { PlantComponent } from "./plant/plant.component";
import { PlantFormComponent } from "./plant-form/plant-form.component";
import { LocationCategoryComponent } from "./location-category/location-category.component";
import { LocationCategoryFormComponent } from "./location-category-form/location-category-form.component";
import { LocationTypeComponent } from "./location-type/location-type.component";
import { LocationTypeFormComponent } from "./location-type-form/location-type-form.component";
import { AssetCategoryComponent } from "./aaset-category/asset-category.component";
import { AssetCategoryFormComponent } from "./asset-category-form/asset-category-form.component";
import { AssetTypeComponent } from "./asset-type/asset-type.component"; 
import { AssetTypeFormComponent } from "./asset-type-form/asset-type-form.component";
import { InspectionCategoryComponent } from "./inspection-category/inspection-category.component";
import { InspectionCategoryFormComponent } from "./inspection-category-form/inspection-category-form.component";
import { InspectionClassComponent } from "./inspection-class/inspection-class.component";
import { InspectionClassFormComponent } from "./inspection-class-form/inspection-class-form.component";
import { InspectionCodeComponent } from "./inspection-code/inspection-code.component";
import { InspectionCodeFormComponent } from "./inspection-code-form/inspection-code-form.component";
import { InspectionGroupComponent } from "./inspection-group/inspection-group.component";
import { InspectionGroupFormComponent } from "./inspection-group-form/inspection-group-form.component";
import { InspectionTypeComponent } from "./inspection-type/inspection-type.component";
import { InspectionTypeFormComponent } from "./inspection-type-form/inspection-type-form.component";
import { UnitComponent } from "./units/unit.component";
import { UnitFormComponent } from "./unit-form/unit-form.component";
import { CountryComponent } from "./country/country.component";
import { CountryFormComponent } from "./country-form/country-form.component";
import { DeleteAssetCategoryModelComponent } from './delete-asset-category-model/delete-asset-category-model/delete-asset-category-model.component';
import { DeleteAssetTypeModelComponent } from './delete-asset-type-model/delete-asset-type-model/delete-asset-type-model.component';
import { DeleteCountryModelComponent } from './delete-country-model/delete-country-model/delete-country-model.component';
import { DeleteInspectionCategoryModelComponent } from './delete-inspection-category-model/delete-inspection-category-model/delete-inspection-category-model.component';
import { DeleteInspectionClassModelComponent } from './delete-inspection-class-model/delete-inspection-class-model/delete-inspection-class-model.component';
import { DeleteInspectionCodeModelComponent } from './delete-inspection-code-model/delete-inspection-code-model/delete-inspection-code-model.component';
import { DeleteInspectionGroupModelComponent } from './delete-inspection-group-model/delete-inspection-group-model/delete-inspection-group-model.component';
import { DeleteInspectionTypeModelComponent } from './delete-inspection-type-model/delete-inspection-type-model/delete-inspection-type-model.component';
import { DeleteLocationCategoryModelComponent } from './delete-location-category-model/delete-location-category-model/delete-location-category-model.component';
import { DeleteLocationTypeModelComponent } from './delete-location-type-model/delete-location-type-model/delete-location-type-model.component';
import { DeletePlantModelComponent } from './delete-plant-model/delete-plant-model/delete-plant-model.component';
import { DeleteUnitModelComponent } from './delete-unit-model/delete-unit-model/delete-unit-model.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { DeleteOrganizationModelComponent } from './delete-organization-model/delete-organization-model/delete-organization-model.component';
import { ActiveRolesPipe } from './pipes/active-roles.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { from } from 'rxjs'; 


@NgModule({
  declarations: [
    OrganizationComponent,
    ConfigurationComponent,
    OrganizationFormComponent,
    PlantComponent,
    PlantFormComponent,
    DeleteOrganizationModelComponent,
    LocationCategoryComponent,
    LocationCategoryFormComponent,
    LocationTypeComponent,
    LocationTypeFormComponent,
    AssetCategoryComponent,
    AssetCategoryFormComponent,
    AssetTypeComponent,
    AssetTypeFormComponent,
    InspectionCategoryComponent,
    InspectionCategoryFormComponent,
    InspectionClassComponent,
    InspectionClassFormComponent,
    InspectionCodeComponent,
    InspectionCodeFormComponent,
    InspectionGroupComponent,
    InspectionGroupFormComponent,
    InspectionTypeComponent,
    InspectionTypeFormComponent,
    UnitComponent,
    UnitFormComponent,
    CountryComponent,
    CountryFormComponent,
    DeleteAssetCategoryModelComponent,
    DeleteAssetTypeModelComponent,
    DeleteCountryModelComponent,
    DeleteInspectionCategoryModelComponent,
    DeleteInspectionClassModelComponent,
    DeleteInspectionCodeModelComponent,
    DeleteInspectionGroupModelComponent,
    DeleteInspectionTypeModelComponent,
    DeleteLocationCategoryModelComponent,
    DeleteLocationTypeModelComponent,
    DeletePlantModelComponent,
    DeleteUnitModelComponent,
    ActiveRolesPipe,
    SearchPipe,
  ],
  imports: [
    CommonModule,
    Ng2OrderModule,
    ConfigurationRoutingModule,
    CommonModule,
    NgxPaginationModule,
    CommonModule,
    FormsModule,
    InlineSVGModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    NgbModule,
    ReactiveFormsModule,
    WidgetsModule,
  ],
})
export class ConfigurationModule {}
