import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { PurchaseStatus, PurchaseStatusService } from './purchase-status.service';

@Injectable({ providedIn: 'root' })
export class PurchaseStatusResolverService implements Resolve<PurchaseStatus> {
  constructor(private purchaseStatusService: PurchaseStatusService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<PurchaseStatus> | Promise<PurchaseStatus> | PurchaseStatus {
    const id = route.paramMap.get('id');
    return this.purchaseStatusService.get(id);
  }
}
