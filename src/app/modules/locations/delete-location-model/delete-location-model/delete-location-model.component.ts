import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteLocation } from "src/app/store/stores/locations/location.actions";
import { getLocationDeleted } from "src/app/store/stores/locations/location.store";
@Component({
  selector: "app-delete-location-model",
  templateUrl: "./delete-location-model.component.html",
  styleUrls: ["./delete-location-model.component.scss"],
})
export class DeleteLocationModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteLocation() {
    this.isLoading = true;
    this.store.dispatch(new DeleteLocation(this.id));
    this.store
      .select(getLocationDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
