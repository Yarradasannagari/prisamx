import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { OrganizationService } from "../../../services/organization/organization.service";

import {
  GetOrganizations,
  GetOrganizationsSuccess,
  GetOrganizationsError,
  CreateOrganization,
  Createorganizationsuccess,
  CreateOrganizationError,
  DeleteOrganization,
  DeleteOrganizationsuccess,
  DeleteOrganizationError,
  OrganizationTypes,
  UpdateOrganization,
  UpdateOrganizationsuccess,
  UpdateOrganizationError,
} from "./organizations.actions";
import { GroupService } from "src/app/services/groups/group.service";

@Injectable()
export class OrganizationEffects {
  constructor(
    private actions: Actions,
    private organizationService: OrganizationService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getOrganization$: Observable<Action> = this.actions.pipe(
    ofType<GetOrganizations>(OrganizationTypes.GetOrganizations),
    exhaustMap(() =>
      from(this.organizationService.list()).pipe(
        map((payload) => {
          return new GetOrganizationsSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Organization", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetOrganizationsError(true));
        })
      )
    )
  );

  @Effect()
  createOrganization$: Observable<Action> = this.actions.pipe(
    ofType<CreateOrganization>(OrganizationTypes.CreateOrganization),
    concatMap((action) =>
      from(this.organizationService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Organization has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new Createorganizationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Organization", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateOrganizationError());
        })
      )
    )
  );
  @Effect()
  updateOrganization$: Observable<Action> = this.actions.pipe(
    ofType<UpdateOrganization>(OrganizationTypes.UpdateOrganization),
    concatMap((action) =>
      from(this.organizationService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Organization has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateOrganizationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Organization", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateOrganizationError());
        })
      )
    )
  );
  @Effect()
  deleteOrganization$: Observable<Action> = this.actions.pipe(
    ofType<DeleteOrganization>(OrganizationTypes.DeleteOrganization),
    concatMap((action) =>
      from(this.organizationService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Organization has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteOrganizationsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Organization:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteOrganizationError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
