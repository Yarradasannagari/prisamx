import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Group {
  id:string;
  usgrp: string;
  name: string;
  active: boolean;
  createdBy?: string;
  createdDate?: string;
  changedBy?: number;
  changedDate?: string;
  
}


@Injectable({
  providedIn: 'root',
})
export class GroupService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Group> {
    return this.entitySvc.get(Url.groups, id);
  }

  list(params?: Record<string, any>): Observable<Group[]> {
    return this.entitySvc.list(Url.groups, params);
  }

  create(group: Group): Observable<Group> {
    return this.entitySvc.create(Url.groups, group);
  }

  update(group: Group): Observable<Group> {
    return this.entitySvc.update(Url.groups, group);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.groups, id);
  }
  
}
