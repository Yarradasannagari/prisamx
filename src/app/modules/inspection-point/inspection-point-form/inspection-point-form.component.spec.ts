import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionPointFormComponent } from './inspection-point-form.component';

describe('InspectionPointFormComponent', () => {
  let component: InspectionPointFormComponent;
  let fixture: ComponentFixture<InspectionPointFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionPointFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionPointFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
