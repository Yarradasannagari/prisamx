import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Plant, PlantService } from "src/app/services/plant/plant.service";
import { GetPlant } from "src/app/store/stores/plant/plant.actions";
import {
  getPlantError,
  getPlant,
  getPlantLoading,
} from "src/app/store/stores/plant/plant.store";
import { State, Store } from "../../../store";
import { DeletePlantModelComponent } from "../delete-plant-model/delete-plant-model/delete-plant-model.component";


@Component({
  selector: "app-plant",
  templateUrl: "./plant.component.html",
  styleUrls: ["./plant.component.scss"],
})
export class PlantComponent implements OnDestroy, OnInit { 
  plant$: Observable<Plant[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  // plant: Plant[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private plantService: PlantService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getPlantLoading);
    this.store.dispatch(new GetPlant());
    this.plant$ = this.store.select(getPlant);
    this.store.select(getPlantError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 
  
  reTry() {
    this.isLoading$ = this.store.select(getPlantLoading);
    this.store.dispatch(new GetPlant());
    this.plant$ = this.store.select(getPlant);
    this.store.select(getPlantError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeletePlantModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
