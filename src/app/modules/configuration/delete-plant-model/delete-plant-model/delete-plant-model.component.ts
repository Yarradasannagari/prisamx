import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeletePlant} from "src/app/store/stores/plant/plant.actions";
import { getPlantDeleted } from "src/app/store/stores/plant/plant.store";
@Component({
  selector: "app-delete-plant-model",
  templateUrl: "./delete-plant-model.component.html",
  styleUrls: ["./delete-plant-model.component.scss"],
})
export class DeletePlantModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deletePlant() {
    this.isLoading = true;
    this.store.dispatch(new DeletePlant(this.id));
    this.store
      .select(getPlantDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
