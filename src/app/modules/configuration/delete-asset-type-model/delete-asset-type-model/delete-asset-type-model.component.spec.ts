import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAssetTypeModelComponent } from './delete-asset-type-model.component';

describe('DeleteAssetTypeModelComponent', () => {
  let component: DeleteAssetTypeModelComponent;
  let fixture: ComponentFixture<DeleteAssetTypeModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAssetTypeModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAssetTypeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
