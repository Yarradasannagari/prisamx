import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { AssetService } from "../../../services/assets/asset.service"; 

import {
  GetAsset,
  GetAssetSuccess,
  GetAssetError,
  CreateAsset,
  CreateAssetsuccess,
  CreateAssetError,
  DeleteAsset,
  DeleteAssetsuccess,
  DeleteAssetError,
  AssetTypes,
  UpdateAsset,
  UpdateAssetsuccess,
  UpdateAssetError,
} from "./asset.actions";

@Injectable()
export class AssetEffects {
  constructor(
    private actions: Actions,
    private assetService: AssetService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getAsset$: Observable<Action> = this.actions.pipe(
    ofType<GetAsset>(AssetTypes.GetAsset),
    exhaustMap(() =>
      from(this.assetService.list()).pipe(
        map((payload) => {
          return new GetAssetSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Asset", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetAssetError(true));
        })
      )
    )
  );

  @Effect()
  createAsset$: Observable<Action> = this.actions.pipe(
    ofType<CreateAsset>(AssetTypes.CreateAsset),
    concatMap((action) =>
      from(this.assetService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateAssetsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Asset", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateAssetError());
        })
      )
    )
  );
  @Effect()
  updateAsset$: Observable<Action> = this.actions.pipe(
    ofType<UpdateAsset>(AssetTypes.UpdateAsset),
    concatMap((action) =>
      from(this.assetService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateAssetsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Asset", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateAssetError());
        })
      )
    )
  );
  @Effect()
  deleteAsset$: Observable<Action> = this.actions.pipe(
    ofType<DeleteAsset>(AssetTypes.DeleteAsset),
    concatMap((action) =>
      from(this.assetService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Asset has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteAssetsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Asset:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteAssetError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
