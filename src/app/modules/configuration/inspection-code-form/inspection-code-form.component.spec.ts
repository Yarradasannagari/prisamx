import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionCodeFormComponent } from './inspection-code-form.component';

describe('InspectionCodeFormComponent', () => {
  let component: InspectionCodeFormComponent;
  let fixture: ComponentFixture<InspectionCodeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionCodeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionCodeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
