import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Inspectioncode } from "src/app/services/inspection-code/inspection-code.service";
import {
  CreateInspectioncode,
  UpdateInspectioncode,
} from "src/app/store/stores/inspection-code/inspection-code.actions";
import {
  getInspectioncodeCreated,
  getInspectioncodeUpdated,
} from "src/app/store/stores/inspection-code/inspection-code.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-inspection-code-form",
  templateUrl: "./inspection-code-form.component.html",
  styleUrls: ["./inspection-code-form.component.scss"],
})
export class InspectionCodeFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    group: "",
    nameSpace: "",
    vcode: "",
    active: true,
  };
  inspectioncode: Inspectioncode;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.group = this.route.snapshot.data["report"].group;
        this.formData.nameSpace = this.route.snapshot.data["report"].nameSpace;
        this.formData.vcode = this.route.snapshot.data["report"].vcode;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      group: [
        this.formData.group,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
        ]),
      ],
      nameSpace: [
        this.formData.nameSpace,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ]),
      ],
      vcode: [
        this.formData.vcode,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectioncode: Inspectioncode = {
      id: this.formData.id,
      group: this.formData.group,
      nameSpace: this.formData.nameSpace,
      vcode: this.formData.vcode,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectioncode(inspectioncode);
    } else {
      this.createInspectioncode(inspectioncode);
    }
  }
  private createInspectioncode(inspectioncode: Inspectioncode): void {
    this.store.dispatch(new CreateInspectioncode(inspectioncode));
    this.store
      .select(getInspectioncodeCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectioncode(inspectioncode: Inspectioncode): void {
    this.store.dispatch(new UpdateInspectioncode(inspectioncode));
    this.store
      .select(getInspectioncodeUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "inspection-code"]);
  }
}
