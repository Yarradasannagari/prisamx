import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionCategoryModelComponent } from './delete-inspection-category-model.component';

describe('DeleteInspectionCategoryModelComponent', () => {
  let component: DeleteInspectionCategoryModelComponent;
  let fixture: ComponentFixture<DeleteInspectionCategoryModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionCategoryModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionCategoryModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
