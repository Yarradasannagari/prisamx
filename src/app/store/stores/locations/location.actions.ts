import { Action } from "@ngrx/store";
import { Location } from "../../../services/locations/location.service";

export enum LocationTypes {
  GetLocation = "[Location] GetLocation",
  GetLocationSuccess = "[Location] GetLocationSuccess",
  GetLocationError = "[Location] GetLocationError",
  CreateLocation = "[Location] CreateLocation",
  CreateLocationsuccess = "[Location] CreateLocationsuccess",
  CreateLocationError = "[Location] CreateLocationError",
  UpdateLocation = "[Location] UpdateLocation",
  UpdateLocationsuccess = "[Location] UpdateLocationsuccess",
  UpdateLocationError = "[Location] UpdateLocationError",
  DeleteLocation = "[Location] DeleteLocation",
  DeleteLocationError = "[Location] DeleteLocationError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteLocationsuccess = "[AclRule] DeleteLocationsuccess",
}

export class GetLocation implements Action { 
  readonly type = LocationTypes.GetLocation;
}

export class GetLocationSuccess implements Action {
  readonly type = LocationTypes.GetLocationSuccess;
  constructor(readonly payload: Location[]) {}
}
export class GetLocationError implements Action {
  readonly type = LocationTypes.GetLocationError;
  constructor(readonly payload: boolean) {}
}

export class CreateLocation implements Action {
  readonly type = LocationTypes.CreateLocation;
  constructor(readonly payload: Location) {}
}

export class CreateLocationsuccess implements Action {
  readonly type = LocationTypes.CreateLocationsuccess;
  constructor(readonly payload: Location) {}
}

export class CreateLocationError implements Action {
  readonly type = LocationTypes.CreateLocationError;
}
export class UpdateLocation implements Action {
  readonly type = LocationTypes.UpdateLocation;
  constructor(readonly payload: Location) {}
}

export class UpdateLocationsuccess implements Action {
  readonly type = LocationTypes.UpdateLocationsuccess;
  constructor(readonly payload: Location) {}
}

export class UpdateLocationError implements Action {
  readonly type = LocationTypes.UpdateLocationError;
}

export class DeleteLocation implements Action {
  readonly type = LocationTypes.DeleteLocation;
  constructor(readonly payload: string) {}
}

export class DeleteLocationsuccess implements Action {
  readonly type = LocationTypes.DeleteLocationsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteLocationError implements Action {
  readonly type = LocationTypes.DeleteLocationError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetLocation
  | GetLocationSuccess
  | GetLocationError
  | CreateLocation
  | CreateLocationsuccess
  | CreateLocationError
  | DeleteLocation
  | DeleteLocationsuccess
  | DeleteLocationError
  | UpdateLocation
  | UpdateLocationsuccess
  | UpdateLocationError
