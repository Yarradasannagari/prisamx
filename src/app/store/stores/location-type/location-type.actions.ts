import { Action } from "@ngrx/store";
import { Locationtype } from "../../../services/location-type/location-type.service";

export enum LocationtypeTypes {
  GetLocationtype = "[Locationtype] GetLocationtype",
  GetLocationtypeSuccess = "[Locationtype] GetLocationtypeSuccess",
  GetLocationtypeError = "[Locationtype] GetLocationtypeError",
  CreateLocationtype = "[Locationtype] CreateLocationtype",
  CreateLocationtypesuccess = "[Locationtype] CreateLocationtypesuccess",
  CreateLocationtypeError = "[Locationtype] CreateLocationtypeError",
  UpdateLocationtype = "[Locationtype] UpdateLocationtype",
  UpdateLocationtypesuccess = "[Locationtype] UpdateLocationtypesuccess",
  UpdateLocationtypeError = "[Locationtype] UpdateLocationtypeError",
  DeleteLocationtype = "[Locationtype] DeleteLocationtype",
  DeleteLocationtypeError = "[Locationtype] DeleteLocationtypeError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteLocationtypesuccess = "[AclRule] DeleteLocationtypesuccess",
}

export class GetLocationtype implements Action { 
  readonly type = LocationtypeTypes.GetLocationtype;
}

export class GetLocationtypeSuccess implements Action {
  readonly type = LocationtypeTypes.GetLocationtypeSuccess;
  constructor(readonly payload: Locationtype[]) {}
}
export class GetLocationtypeError implements Action {
  readonly type = LocationtypeTypes.GetLocationtypeError;
  constructor(readonly payload: boolean) {}
}

export class CreateLocationtype implements Action {
  readonly type = LocationtypeTypes.CreateLocationtype;
  constructor(readonly payload: Locationtype) {}
}

export class CreateLocationtypesuccess implements Action {
  readonly type = LocationtypeTypes.CreateLocationtypesuccess;
  constructor(readonly payload: Locationtype) {}
}

export class CreateLocationtypeError implements Action {
  readonly type = LocationtypeTypes.CreateLocationtypeError;
}
export class UpdateLocationtype implements Action {
  readonly type = LocationtypeTypes.UpdateLocationtype;
  constructor(readonly payload: Locationtype) {}
}

export class UpdateLocationtypesuccess implements Action {
  readonly type = LocationtypeTypes.UpdateLocationtypesuccess;
  constructor(readonly payload: Locationtype) {}
}

export class UpdateLocationtypeError implements Action {
  readonly type = LocationtypeTypes.UpdateLocationtypeError;
}

export class DeleteLocationtype implements Action {
  readonly type = LocationtypeTypes.DeleteLocationtype;
  constructor(readonly payload: string) {}
}

export class DeleteLocationtypesuccess implements Action {
  readonly type = LocationtypeTypes.DeleteLocationtypesuccess;
  constructor(readonly payload: string) {}
}

export class DeleteLocationtypeError implements Action {
  readonly type = LocationtypeTypes.DeleteLocationtypeError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetLocationtype
  | GetLocationtypeSuccess
  | GetLocationtypeError
  | CreateLocationtype
  | CreateLocationtypesuccess
  | CreateLocationtypeError
  | DeleteLocationtype
  | DeleteLocationtypesuccess
  | DeleteLocationtypeError
  | UpdateLocationtype
  | UpdateLocationtypesuccess
  | UpdateLocationtypeError
