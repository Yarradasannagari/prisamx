import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Locationcategory {
  id:string;
  active: boolean;
  category: string;
  name: string;
}


@Injectable({
  providedIn: 'root',
})
export class LocationcategoryService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Locationcategory> {
    return this.entitySvc.get(Url.locationcat, id);
  }

  list(params?: Record<string, any>): Observable<Locationcategory[]> {
    return this.entitySvc.list(Url.locationcat, params);
  }

  create(aclRule: Locationcategory): Observable<Locationcategory> {
    return this.entitySvc.create(Url.locationcat, aclRule);
  }

  update(aclRule: Locationcategory): Observable<Locationcategory> {
    return this.entitySvc.update(Url.locationcat, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.locationcat, id);
  }
  
}
