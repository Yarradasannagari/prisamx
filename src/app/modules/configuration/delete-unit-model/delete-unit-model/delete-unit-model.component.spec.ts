import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteUnitModelComponent } from './delete-unit-model.component';

describe('DeleteUnitModelComponent', () => {
  let component: DeleteUnitModelComponent;
  let fixture: ComponentFixture<DeleteUnitModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteUnitModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteUnitModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
