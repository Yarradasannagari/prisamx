import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { LocationcategoryService } from "../../../services/location-category/location-category.service"; 

import {
  GetLocationcategory,
  GetLocationcategorySuccess,
  GetLocationcategoryError,
  CreateLocationcategory,
  CreateLocationcategorysuccess,
  CreateLocationcategoryError,
  DeleteLocationcategory,
  DeleteLocationcategorysuccess,
  DeleteLocationcategoryError,
  LocationcategoryTypes,
  UpdateLocationcategory,
  UpdateLocationcategorysuccess,
  UpdateLocationcategoryError,
} from "./location-category.actions";

@Injectable()
export class LocationcategoryEffects {
  constructor(
    private actions: Actions,
    private locationcategoryService: LocationcategoryService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getLocationcategory$: Observable<Action> = this.actions.pipe(
    ofType<GetLocationcategory>(LocationcategoryTypes.GetLocationcategory),
    exhaustMap(() =>
      from(this.locationcategoryService.list()).pipe(
        map((payload) => {
          return new GetLocationcategorySuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Location Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetLocationcategoryError(true));
        })
      )
    )
  );

  @Effect()
  createLocationcategory$: Observable<Action> = this.actions.pipe(
    ofType<CreateLocationcategory>(LocationcategoryTypes.CreateLocationcategory),
    concatMap((action) =>
      from(this.locationcategoryService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location Category has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateLocationcategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Location Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateLocationcategoryError());
        })
      )
    )
  );
  @Effect()
  updateLocationcategory$: Observable<Action> = this.actions.pipe(
    ofType<UpdateLocationcategory>(LocationcategoryTypes.UpdateLocationcategory),
    concatMap((action) =>
      from(this.locationcategoryService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location Category has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateLocationcategorysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Location Category", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateLocationcategoryError());
        })
      )
    )
  );
  @Effect()
  deleteLocationcategory$: Observable<Action> = this.actions.pipe(
    ofType<DeleteLocationcategory>(LocationcategoryTypes.DeleteLocationcategory),
    concatMap((action) =>
      from(this.locationcategoryService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Location Category has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteLocationcategorysuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Location Category:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteLocationcategoryError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
