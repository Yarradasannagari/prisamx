import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Assetcategory, AssetcategoryService } from './asset-category.service';

@Injectable({ providedIn: 'root' })
export class AssetcategoryResolverService implements Resolve<Assetcategory> {
  constructor(private assetcategoryService: AssetcategoryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Assetcategory> | Promise<Assetcategory> | Assetcategory {
    const id = route.paramMap.get('id');
    return this.assetcategoryService.get(id);
  }
}
