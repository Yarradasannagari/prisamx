import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Inspectiongroup, InspectiongroupService } from './inspection-group.service';

@Injectable({ providedIn: 'root' })
export class InspectiongroupResolverService implements Resolve<Inspectiongroup> {
  constructor(private inspectiongroupService: InspectiongroupService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Inspectiongroup> | Promise<Inspectiongroup> | Inspectiongroup {
    const id = route.paramMap.get('id');
    return this.inspectiongroupService.get(id);
  }
}
