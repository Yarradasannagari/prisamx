import { Action } from "@ngrx/store";
import { Group } from "src/app/services/groups/group.service";

export enum GroupTypes {
  GetGroups = "[Group] GetGroups",
  GetGroupsSuccess = "[Group] GetGroupsSuccess",
  GetGroupsError = "[Group] GetGroupsError",
  CreateGroup = "[Group] CreateGroup",
  CreateGroupsuccess = "[Group] CreateGroupsuccess",
  CreateGroupError = "[Group] CreateGroupError",
  UpdateGroup = "[Group] UpdateGroup",
  UpdateGroupsuccess = "[Group] UpdateGroupsuccess",
  UpdateGroupError = "[Group] UpdateGroupError",
  DeleteGroup = "[Group] DeleteGroup",
  DeleteGroupError = "[Group] DeleteGroupError",
  DeleteGroupsuccess = "[Group] DeleteGroupsuccess",
}

export class GetGroups implements Action {
  readonly type = GroupTypes.GetGroups;
}

export class GetGroupsSuccess implements Action {
  readonly type = GroupTypes.GetGroupsSuccess;
  constructor(readonly payload: Group[]) {}
}
export class GetGroupsError implements Action {
  readonly type = GroupTypes.GetGroupsError;
  constructor(readonly payload: boolean) {}
}

export class CreateGroup implements Action {
  readonly type = GroupTypes.CreateGroup;
  constructor(readonly payload: Group) {}
}

export class CreateGroupsuccess implements Action {
  readonly type = GroupTypes.CreateGroupsuccess;
  constructor(readonly payload: Group) {}
}

export class CreateGroupError implements Action {
  readonly type = GroupTypes.CreateGroupError;
}
export class UpdateGroup implements Action {
  readonly type = GroupTypes.UpdateGroup;
  constructor(readonly payload: Group) {}
}

export class UpdateGroupsuccess implements Action {
  readonly type = GroupTypes.UpdateGroupsuccess;
  constructor(readonly payload: Group) {}
}

export class UpdateGroupError implements Action {
  readonly type = GroupTypes.UpdateGroupError;
}

export class DeleteGroup implements Action {
  readonly type = GroupTypes.DeleteGroup;
  constructor(readonly payload: string) {}
}

export class DeleteGroupsuccess implements Action {
  readonly type = GroupTypes.DeleteGroupsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteGroupError implements Action {
  readonly type = GroupTypes.DeleteGroupError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetGroups
  | GetGroupsSuccess
  | GetGroupsError
  | CreateGroup
  | CreateGroupsuccess
  | CreateGroupError
  | UpdateGroup
  | UpdateGroupsuccess
  | UpdateGroupError
  | DeleteGroup
  | DeleteGroupError
  | DeleteGroupsuccess;
