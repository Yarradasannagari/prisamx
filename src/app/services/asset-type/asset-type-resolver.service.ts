import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Assettype, AssettypeService } from './asset-type.service';

@Injectable({ providedIn: 'root' })
export class AssettypeResolverService implements Resolve<Assettype> {
  constructor(private assettypeService: AssettypeService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Assettype> | Promise<Assettype> | Assettype {
    const id = route.paramMap.get('id');
    return this.assettypeService.get(id);
  }
}
