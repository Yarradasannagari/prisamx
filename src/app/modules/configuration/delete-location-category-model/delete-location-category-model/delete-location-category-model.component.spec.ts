import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteLocationCategoryModelComponent } from './delete-location-category-model.component';

describe('DeleteLocationCategoryModelComponent', () => {
  let component: DeleteLocationCategoryModelComponent;
  let fixture: ComponentFixture<DeleteLocationCategoryModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteLocationCategoryModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteLocationCategoryModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
