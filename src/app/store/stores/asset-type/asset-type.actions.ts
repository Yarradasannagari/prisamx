import { Action } from "@ngrx/store";
import { Assettype } from "../../../services/asset-type/asset-type.service";

export enum AssettypeTypes {
  GetAssettype = "[Assettype] GetAssettype",
  GetAssettypeSuccess = "[Assettype] GetAssettypeSuccess",
  GetAssettypeError = "[Assettype] GetAssettypeError",
  CreateAssettype = "[Assettype] CreateAssettype",
  CreateAssettypesuccess = "[Assettype] CreateAssettypesuccess",
  CreateAssettypeError = "[Assettype] CreateAssettypeError",
  UpdateAssettype = "[Assettype] UpdateAssettype",
  UpdateAssettypesuccess = "[Assettype] UpdateAssettypesuccess",
  UpdateAssettypeError = "[Assettype] UpdateAssettypeError",
  DeleteAssettype = "[Assettype] DeleteAssettype",
  DeleteAssettypeError = "[Assettype] DeleteAssettypeError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteAssettypesuccess = "[AclRule] DeleteAssettypesuccess",
}

export class GetAssettype implements Action { 
  readonly type = AssettypeTypes.GetAssettype;
}

export class GetAssettypeSuccess implements Action {
  readonly type = AssettypeTypes.GetAssettypeSuccess;
  constructor(readonly payload: Assettype[]) {}
}
export class GetAssettypeError implements Action {
  readonly type = AssettypeTypes.GetAssettypeError;
  constructor(readonly payload: boolean) {}
}

export class CreateAssettype implements Action {
  readonly type = AssettypeTypes.CreateAssettype;
  constructor(readonly payload: Assettype) {}
}

export class CreateAssettypesuccess implements Action {
  readonly type = AssettypeTypes.CreateAssettypesuccess;
  constructor(readonly payload: Assettype) {}
}

export class CreateAssettypeError implements Action {
  readonly type = AssettypeTypes.CreateAssettypeError;
}
export class UpdateAssettype implements Action {
  readonly type = AssettypeTypes.UpdateAssettype;
  constructor(readonly payload: Assettype) {}
}

export class UpdateAssettypesuccess implements Action {
  readonly type = AssettypeTypes.UpdateAssettypesuccess;
  constructor(readonly payload: Assettype) {}
}

export class UpdateAssettypeError implements Action {
  readonly type = AssettypeTypes.UpdateAssettypeError;
}

export class DeleteAssettype implements Action {
  readonly type = AssettypeTypes.DeleteAssettype;
  constructor(readonly payload: string) {}
}

export class DeleteAssettypesuccess implements Action {
  readonly type = AssettypeTypes.DeleteAssettypesuccess;
  constructor(readonly payload: string) {}
}

export class DeleteAssettypeError implements Action {
  readonly type = AssettypeTypes.DeleteAssettypeError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetAssettype
  | GetAssettypeSuccess
  | GetAssettypeError
  | CreateAssettype
  | CreateAssettypesuccess
  | CreateAssettypeError
  | DeleteAssettype
  | DeleteAssettypesuccess
  | DeleteAssettypeError
  | UpdateAssettype
  | UpdateAssettypesuccess
  | UpdateAssettypeError
