import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Group } from "src/app/services/groups/group.service";
import { CreateGroup, UpdateGroup } from "src/app/store/stores/groups/groups.actions";
import { getGroupCreated, getGroupUpdated } from "src/app/store/stores/groups/groups.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-group-form",
  templateUrl: "./group-form.component.html",
  styleUrls: ["./group-form.component.scss"],
})
export class GroupFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    usgrp: "", 
    name: "",
    active: true,
  };
  group: Group;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.usgrp = this.route.snapshot.data["report"].usgrp;
        this.formData.name =
          this.route.snapshot.data["report"].name;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      usgrp: [
        this.formData.usgrp,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_ ]+"),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
          // Validators.pattern("[a-zA-Z0-9_ ]+"),s
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const group: Group = {
      id: this.formData.id,
      usgrp: this.formData.usgrp,
      name: this.formData.name,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateGroup(group);
    } else {
      this.createGroup(group);
    }
  }
  private createGroup(group: Group): void {
    this.store.dispatch(new CreateGroup(group));
    this.store
      .select(getGroupCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateGroup(group: Group): void {
    this.store.dispatch(new UpdateGroup(group));
    this.store
      .select(getGroupUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["user-management", "group"]);
  }
}
