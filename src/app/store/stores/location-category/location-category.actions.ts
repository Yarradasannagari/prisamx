import { Action } from "@ngrx/store";
import { Locationcategory } from "../../../services/location-category/location-category.service";

export enum LocationcategoryTypes {
  GetLocationcategory = "[Locationcategory] GetLocationcategory",
  GetLocationcategorySuccess = "[Locationcategory] GetLocationcategorySuccess",
  GetLocationcategoryError = "[Locationcategory] GetLocationcategoryError",
  CreateLocationcategory = "[Locationcategory] CreateLocationcategory",
  CreateLocationcategorysuccess = "[Locationcategory] CreateLocationcategorysuccess",
  CreateLocationcategoryError = "[Locationcategory] CreateLocationcategoryError",
  UpdateLocationcategory = "[Locationcategory] UpdateLocationcategory",
  UpdateLocationcategorysuccess = "[Locationcategory] UpdateLocationcategorysuccess",
  UpdateLocationcategoryError = "[Locationcategory] UpdateLocationcategoryError",
  DeleteLocationcategory = "[Locationcategory] DeleteLocationcategory",
  DeleteLocationcategoryError = "[Locationcategory] DeleteLocationcategoryError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteLocationcategorysuccess = "[AclRule] DeleteLocationcategorysuccess",
}

export class GetLocationcategory implements Action { 
  readonly type = LocationcategoryTypes.GetLocationcategory;
}

export class GetLocationcategorySuccess implements Action {
  readonly type = LocationcategoryTypes.GetLocationcategorySuccess;
  constructor(readonly payload: Locationcategory[]) {}
}
export class GetLocationcategoryError implements Action {
  readonly type = LocationcategoryTypes.GetLocationcategoryError;
  constructor(readonly payload: boolean) {}
}

export class CreateLocationcategory implements Action {
  readonly type = LocationcategoryTypes.CreateLocationcategory;
  constructor(readonly payload: Locationcategory) {}
}

export class CreateLocationcategorysuccess implements Action {
  readonly type = LocationcategoryTypes.CreateLocationcategorysuccess;
  constructor(readonly payload: Locationcategory) {}
}

export class CreateLocationcategoryError implements Action {
  readonly type = LocationcategoryTypes.CreateLocationcategoryError;
}
export class UpdateLocationcategory implements Action {
  readonly type = LocationcategoryTypes.UpdateLocationcategory;
  constructor(readonly payload: Locationcategory) {}
}

export class UpdateLocationcategorysuccess implements Action {
  readonly type = LocationcategoryTypes.UpdateLocationcategorysuccess;
  constructor(readonly payload: Locationcategory) {}
}

export class UpdateLocationcategoryError implements Action {
  readonly type = LocationcategoryTypes.UpdateLocationcategoryError;
}

export class DeleteLocationcategory implements Action {
  readonly type = LocationcategoryTypes.DeleteLocationcategory;
  constructor(readonly payload: string) {}
}

export class DeleteLocationcategorysuccess implements Action {
  readonly type = LocationcategoryTypes.DeleteLocationcategorysuccess;
  constructor(readonly payload: string) {}
}

export class DeleteLocationcategoryError implements Action {
  readonly type = LocationcategoryTypes.DeleteLocationcategoryError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetLocationcategory
  | GetLocationcategorySuccess
  | GetLocationcategoryError
  | CreateLocationcategory
  | CreateLocationcategorysuccess
  | CreateLocationcategoryError
  | DeleteLocationcategory
  | DeleteLocationcategorysuccess
  | DeleteLocationcategoryError
  | UpdateLocationcategory
  | UpdateLocationcategorysuccess
  | UpdateLocationcategoryError
