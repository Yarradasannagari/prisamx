import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Classification, ClassificationService } from './classification.service';

@Injectable({ providedIn: 'root' })
export class ClassificationResolverService implements Resolve<Classification> {
  constructor(private classificationService: ClassificationService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Classification> | Promise<Classification> | Classification {
    const id = route.paramMap.get('id');
    return this.classificationService.get(id);
  }
}
