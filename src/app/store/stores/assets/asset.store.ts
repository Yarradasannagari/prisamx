import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Asset } from "src/app/services/assets/asset.service";

import { State } from "../..";

import { AssetTypes, Union } from "./asset.actions";

export interface AssetState {
  loading: boolean;
  entities: Asset[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: AssetState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function AssetReducer(
  state: AssetState = INITIAL_STATE,
  action: Union
): AssetState {
  switch (action.type) {
    case AssetTypes.GetAsset:
      return {
        ...state,
        loading: true,
      };
    case AssetTypes.GetAssetSuccess:
      const asset: Asset[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: asset,
      };
    case AssetTypes.GetAssetError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case AssetTypes.CreateAsset:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case AssetTypes.CreateAssetsuccess:
      const createdAsset: Asset = action.payload;
      const entitiesAppended = [...state.entities, createdAsset];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case AssetTypes.UpdateAsset:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case AssetTypes.UpdateAssetsuccess:
      const updatedAsset: Asset = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedAsset];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case AssetTypes.UpdateAssetError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case AssetTypes.CreateAssetError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case AssetTypes.DeleteAsset:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case AssetTypes.DeleteAssetsuccess:
      const afterDelete = state.entities.filter((asset) => asset.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case AssetTypes.DeleteAssetError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getAssetState = createFeatureSelector<State, AssetState>("asset");

export const getAssetLoading = createSelector(
  getAssetState,
  (state) => state.loading
);

export const getAsset = createSelector(getAssetState, (state) => state.entities);

export const getAssetCreating = createSelector(
  getAssetState,
  (state) => state.creating
);

export const getAssetCreated = createSelector(
  getAssetState,
  (state) => state.created
);
export const getAssetUpdating = createSelector(
  getAssetState,
  (state) => state.updating
);

export const getAssetUpdated = createSelector(
  getAssetState,
  (state) => state.updated
);
export const getAssetDeleting = createSelector(
  getAssetState,
  (state) => state.deleting
);

export const getAssetDeleted = createSelector(
  getAssetState,
  (state) => state.deleted
);

export const getAssetError = createSelector(
  getAssetState,
  (state) => state.error
);
