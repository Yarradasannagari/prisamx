import { Action } from "@ngrx/store";
import { Partner } from "../../../services/partner/partner.service";

export enum PartnerTypes {
  GetPartner = "[Partner] GetPartner",
  GetPartnerSuccess = "[Partner] GetPartnerSuccess",
  GetPartnerError = "[Partner] GetPartnerError",
  CreatePartner = "[Partner] CreatePartner",
  CreatePartnersuccess = "[Partner] CreatePartnersuccess",
  CreatePartnerError = "[Partner] CreatePartnerError",
  UpdatePartner = "[Partner] UpdatePartner",
  UpdatePartnersuccess = "[Partner] UpdatePartnersuccess",
  UpdatePartnerError = "[Partner] UpdatePartnerError",
  DeletePartner = "[Partner] DeletePartner",
  DeletePartnerError = "[Partner] DeletePartnerError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeletePartnersuccess = "[AclRule] DeletePartnersuccess",
}

export class GetPartner implements Action { 
  readonly type = PartnerTypes.GetPartner;
}

export class GetPartnerSuccess implements Action {
  readonly type = PartnerTypes.GetPartnerSuccess;
  constructor(readonly payload: Partner[]) {}
}
export class GetPartnerError implements Action {
  readonly type = PartnerTypes.GetPartnerError;
  constructor(readonly payload: boolean) {}
}

export class CreatePartner implements Action {
  readonly type = PartnerTypes.CreatePartner;
  constructor(readonly payload: Partner) {}
}

export class CreatePartnersuccess implements Action {
  readonly type = PartnerTypes.CreatePartnersuccess;
  constructor(readonly payload: Partner) {}
}

export class CreatePartnerError implements Action {
  readonly type = PartnerTypes.CreatePartnerError;
}
export class UpdatePartner implements Action {
  readonly type = PartnerTypes.UpdatePartner;
  constructor(readonly payload: Partner) {}
}

export class UpdatePartnersuccess implements Action {
  readonly type = PartnerTypes.UpdatePartnersuccess;
  constructor(readonly payload: Partner) {}
}

export class UpdatePartnerError implements Action {
  readonly type = PartnerTypes.UpdatePartnerError;
}

export class DeletePartner implements Action {
  readonly type = PartnerTypes.DeletePartner;
  constructor(readonly payload: string) {}
}

export class DeletePartnersuccess implements Action {
  readonly type = PartnerTypes.DeletePartnersuccess;
  constructor(readonly payload: string) {}
}

export class DeletePartnerError implements Action {
  readonly type = PartnerTypes.DeletePartnerError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetPartner
  | GetPartnerSuccess
  | GetPartnerError
  | CreatePartner
  | CreatePartnersuccess
  | CreatePartnerError
  | DeletePartner
  | DeletePartnersuccess
  | DeletePartnerError
  | UpdatePartner
  | UpdatePartnersuccess
  | UpdatePartnerError
