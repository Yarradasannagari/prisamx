import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionGroupModelComponent } from './delete-inspection-group-model.component';

describe('DeleteInspectionGroupModelComponent', () => {
  let component: DeleteInspectionGroupModelComponent;
  let fixture: ComponentFixture<DeleteInspectionGroupModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionGroupModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionGroupModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
