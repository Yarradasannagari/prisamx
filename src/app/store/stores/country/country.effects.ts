import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { CountryService } from "../../../services/country/country.service"; 

import {
  GetCountry,
  GetCountrySuccess,
  GetCountryError,
  CreateCountry,
  CreateCountrysuccess,
  CreateCountryError,
  DeleteCountry,
  DeleteCountrysuccess,
  DeleteCountryError,
  CountryTypes,
  UpdateCountry,
  UpdateCountrysuccess,
  UpdateCountryError,
} from "./country.actions";

@Injectable()
export class CountryEffects {
  constructor(
    private actions: Actions,
    private countryService: CountryService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getCountry$: Observable<Action> = this.actions.pipe(
    ofType<GetCountry>(CountryTypes.GetCountry),
    exhaustMap(() =>
      from(this.countryService.list()).pipe(
        map((payload) => {
          return new GetCountrySuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Country", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetCountryError(true));
        })
      )
    )
  );

  @Effect()
  createCountry$: Observable<Action> = this.actions.pipe(
    ofType<CreateCountry>(CountryTypes.CreateCountry),
    concatMap((action) =>
      from(this.countryService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Country has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateCountrysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Country", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateCountryError());
        })
      )
    )
  );
  @Effect()
  updateCountry$: Observable<Action> = this.actions.pipe(
    ofType<UpdateCountry>(CountryTypes.UpdateCountry),
    concatMap((action) =>
      from(this.countryService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Country has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateCountrysuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Country", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateCountryError());
        })
      )
    )
  );
  @Effect()
  deleteCountry$: Observable<Action> = this.actions.pipe(
    ofType<DeleteCountry>(CountryTypes.DeleteCountry),
    concatMap((action) =>
      from(this.countryService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Country has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteCountrysuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Country:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteCountryError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
