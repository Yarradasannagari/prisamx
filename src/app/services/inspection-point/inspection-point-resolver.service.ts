import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { InspectionPoint, InspectionPointService } from './inspection-point.service';

@Injectable({ providedIn: 'root' })
export class InspectionPointResolverService implements Resolve<InspectionPoint> {
  constructor(private inspectionPointService: InspectionPointService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<InspectionPoint> | Promise<InspectionPoint> | InspectionPoint {
    const id = route.paramMap.get('id');
    return this.inspectionPointService.get(id);
  }
}
