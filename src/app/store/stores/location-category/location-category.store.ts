import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Locationcategory } from "src/app/services/location-category/location-category.service";

import { State } from "../..";

import { LocationcategoryTypes, Union } from "./location-category.actions";

export interface LocationcategoryState {
  loading: boolean;
  entities: Locationcategory[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: LocationcategoryState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function LocationcategoryReducer(
  state: LocationcategoryState = INITIAL_STATE,
  action: Union
): LocationcategoryState {
  switch (action.type) {
    case LocationcategoryTypes.GetLocationcategory:
      return {
        ...state,
        loading: true,
      };
    case LocationcategoryTypes.GetLocationcategorySuccess:
      const locationcategory: Locationcategory[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: locationcategory,
      };
    case LocationcategoryTypes.GetLocationcategoryError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case LocationcategoryTypes.CreateLocationcategory:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case LocationcategoryTypes.CreateLocationcategorysuccess:
      const createdLocationcategory: Locationcategory = action.payload;
      const entitiesAppended = [...state.entities, createdLocationcategory];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case LocationcategoryTypes.UpdateLocationcategory:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case LocationcategoryTypes.UpdateLocationcategorysuccess:
      const updatedLocationcategory: Locationcategory = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedLocationcategory];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case LocationcategoryTypes.UpdateLocationcategoryError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case LocationcategoryTypes.CreateLocationcategoryError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case LocationcategoryTypes.DeleteLocationcategory:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case LocationcategoryTypes.DeleteLocationcategorysuccess:
      const afterDelete = state.entities.filter((locationcategory) => locationcategory.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case LocationcategoryTypes.DeleteLocationcategoryError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getLocationcategoryState = createFeatureSelector<State, LocationcategoryState>("locationcategory");

export const getLocationcategoryLoading = createSelector(
  getLocationcategoryState,
  (state) => state.loading
);

export const getLocationcategory = createSelector(getLocationcategoryState, (state) => state.entities);

export const getLocationcategoryCreating = createSelector(
  getLocationcategoryState,
  (state) => state.creating
);

export const getLocationcategoryCreated = createSelector(
  getLocationcategoryState,
  (state) => state.created
);
export const getLocationcategoryUpdating = createSelector(
  getLocationcategoryState,
  (state) => state.updating
);

export const getLocationcategoryUpdated = createSelector(
  getLocationcategoryState,
  (state) => state.updated
);
export const getLocationcategoryDeleting = createSelector(
  getLocationcategoryState,
  (state) => state.deleting
);

export const getLocationcategoryDeleted = createSelector(
  getLocationcategoryState,
  (state) => state.deleted
);

export const getLocationcategoryError = createSelector(
  getLocationcategoryState,
  (state) => state.error
);
