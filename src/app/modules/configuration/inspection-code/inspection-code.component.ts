import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Inspectioncode, InspectioncodeService } from "src/app/services/inspection-code/inspection-code.service";
import { GetInspectioncode } from "src/app/store/stores/inspection-code/inspection-code.actions";
import {
  getInspectioncodeError,
  getInspectioncode,
  getInspectioncodeLoading,
} from "src/app/store/stores/inspection-code/inspection-code.store";
import { State, Store } from "../../../store";
import { DeleteInspectionCodeModelComponent } from "../delete-inspection-code-model/delete-inspection-code-model/delete-inspection-code-model.component";

@Component({
  selector: "app-inspection-code",
  templateUrl: "./inspection-code.component.html",
  styleUrls: ["./inspection-code.component.scss"],
})
export class InspectionCodeComponent implements OnDestroy, OnInit {
  inspectioncode$: Observable<Inspectioncode[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  inspectioncode: Inspectioncode[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private inspectioncodeService: InspectioncodeService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectioncodeLoading);
    this.store.dispatch(new GetInspectioncode());
    this.inspectioncode$ = this.store.select(getInspectioncode);
    this.store.select(getInspectioncodeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }
  
  reTry() {
    this.isLoading$ = this.store.select(getInspectioncodeLoading);
    this.store.dispatch(new GetInspectioncode());
    this.inspectioncode$ = this.store.select(getInspectioncode);
    this.store.select(getInspectioncodeError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionCodeModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
