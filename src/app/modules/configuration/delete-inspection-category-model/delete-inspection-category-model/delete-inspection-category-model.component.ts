import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteInspectioncategory} from "src/app/store/stores/inspection-category/inspection-category.actions";
import { getInspectioncategoryDeleted } from "src/app/store/stores/inspection-category/inspection-category.store";
@Component({
  selector: "app-delete-inspection-category-model",
  templateUrl: "./delete-inspection-category-model.component.html",
  styleUrls: ["./delete-inspection-category-model.component.scss"],
})
export class DeleteInspectionCategoryModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteInspectioncategory() {
    this.isLoading = true;
    this.store.dispatch(new DeleteInspectioncategory(this.id));
    this.store
      .select(getInspectioncategoryDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
