import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { PurchaseService } from "../../../services/purchase/purchase.service"; 

import {
  GetPurchase,
  GetPurchaseSuccess,
  GetPurchaseError,
  CreatePurchase,
  CreatePurchasesuccess,
  CreatePurchaseError,
  DeletePurchase,
  DeletePurchasesuccess,
  DeletePurchaseError,
  PurchaseTypes,
  UpdatePurchase,
  UpdatePurchasesuccess,
  UpdatePurchaseError,
} from "./purchase.actions";

@Injectable()
export class PurchaseEffects {
  constructor(
    private actions: Actions,
    private purchaseService: PurchaseService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getPurchase$: Observable<Action> = this.actions.pipe(
    ofType<GetPurchase>(PurchaseTypes.GetPurchase),
    exhaustMap(() =>
      from(this.purchaseService.list()).pipe(
        map((payload) => {
          return new GetPurchaseSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Purchase", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetPurchaseError(true));
        })
      )
    )
  );

  @Effect()
  createPurchase$: Observable<Action> = this.actions.pipe(
    ofType<CreatePurchase>(PurchaseTypes.CreatePurchase),
    concatMap((action) =>
      from(this.purchaseService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreatePurchasesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Purchase", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreatePurchaseError());
        })
      )
    )
  );
  @Effect()
  updatePurchase$: Observable<Action> = this.actions.pipe(
    ofType<UpdatePurchase>(PurchaseTypes.UpdatePurchase),
    concatMap((action) =>
      from(this.purchaseService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdatePurchasesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Purchase", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdatePurchaseError());
        })
      )
    )
  );
  @Effect()
  deletePurchase$: Observable<Action> = this.actions.pipe(
    ofType<DeletePurchase>(PurchaseTypes.DeletePurchase),
    concatMap((action) =>
      from(this.purchaseService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Purchase has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeletePurchasesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Purchase:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeletePurchaseError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
