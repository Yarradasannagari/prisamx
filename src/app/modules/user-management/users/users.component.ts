import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { User } from "src/app/services/users/user.service";
import { GetUsers } from "src/app/store/stores/users/users.actions";
import {
  getUsers,
  getUsersLoading,
} from "src/app/store/stores/users/users.store";
import { State, Store } from "../../../store";
@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  private readonly destroy = new Subject<void>();
  constructor(private store: Store<State>, private modalService: NgbModal) {
    this.isLoading$ = this.store.select(getUsersLoading);
    this.store.dispatch(new GetUsers());
    this.users$ = this.store.select(getUsers);

    // this.store.select(getUserError).subscribe((flag) => {
    //   if (flag) {
    //     this.isError = flag;
    //   }
    // });
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 
  
  ngOnInit(): void {}
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {}

  delete(id: any) {
    // const modalRef = this.modalService.open(DeleteRoleModelComponent);
    // modalRef.componentInstance.id = id;
    // modalRef.result.then(
    //   () => {
    //   },
    //   () => {}
    // );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
