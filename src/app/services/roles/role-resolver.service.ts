import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Role, RoleService } from './role.service';

@Injectable({ providedIn: 'root' })
export class RoleResolverService implements Resolve<Role> {
  constructor(private roleService: RoleService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Role> | Promise<Role> | Role {
    const id = route.paramMap.get('id');
    return this.roleService.get(id);
  }
}
