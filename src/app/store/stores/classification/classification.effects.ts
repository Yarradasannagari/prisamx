import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { ClassificationService } from "../../../services/classification/classification.service"; 

import {
  GetClassification,
  GetClassificationSuccess,
  GetClassificationError,
  CreateClassification,
  CreateClassificationsuccess,
  CreateClassificationError,
  DeleteClassification,
  DeleteClassificationsuccess,
  DeleteClassificationError,
  ClassificationTypes,
  UpdateClassification,
  UpdateClassificationsuccess,
  UpdateClassificationError,
} from "./classification.actions";

@Injectable()
export class ClassificationEffects {
  constructor(
    private actions: Actions,
    private classificationService: ClassificationService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getClassification$: Observable<Action> = this.actions.pipe(
    ofType<GetClassification>(ClassificationTypes.GetClassification),
    exhaustMap(() =>
      from(this.classificationService.list()).pipe(
        map((payload) => {
          return new GetClassificationSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Classification", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetClassificationError(true));
        })
      )
    )
  );

  @Effect()
  createClassification$: Observable<Action> = this.actions.pipe(
    ofType<CreateClassification>(ClassificationTypes.CreateClassification),
    concatMap((action) =>
      from(this.classificationService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Classification has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateClassificationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Classification", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateClassificationError());
        })
      )
    )
  );
  @Effect()
  updateClassification$: Observable<Action> = this.actions.pipe(
    ofType<UpdateClassification>(ClassificationTypes.UpdateClassification),
    concatMap((action) =>
      from(this.classificationService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Classification has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateClassificationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Classification", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateClassificationError());
        })
      )
    )
  );
  @Effect()
  deleteClassification$: Observable<Action> = this.actions.pipe(
    ofType<DeleteClassification>(ClassificationTypes.DeleteClassification),
    concatMap((action) =>
      from(this.classificationService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Classification has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteClassificationsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Classification:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteClassificationError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
