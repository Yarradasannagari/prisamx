import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteInspectiontype} from "src/app/store/stores/inspection-type/inspection-type.actions";
import { getInspectiontypeDeleted } from "src/app/store/stores/inspection-type/inspection-type.store";
@Component({
  selector: "app-delete-inspection-type-model",
  templateUrl: "./delete-inspection-type-model.component.html",
  styleUrls: ["./delete-inspection-type-model.component.scss"],
})
export class DeleteInspectionTypeModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteInspectiontype() {
    this.isLoading = true;
    this.store.dispatch(new DeleteInspectiontype(this.id));
    this.store
      .select(getInspectiontypeDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
