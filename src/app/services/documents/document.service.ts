import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Document {
  // active: boolean;
  changedDate: string;
  chnagedBy: string;
  content: string;
  contentContentType: string;
  createdBy: string;
  createdDate: string;
  docId: string;
  docType: string;
  fileName: string;
  fileSize: string;
  fileType: string;
  id: string;
  keyId: string;
  objectId: string;
  objectTable: string;
  objectType: string;
  url: string;
  urlType: string;

}


@Injectable({
  providedIn: 'root',
})
export class DocumentService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Document> {
    return this.entitySvc.get(Url.document, id);
  }

  list(params?: Record<string, any>): Observable<Document[]> {
    return this.entitySvc.list(Url.document, params);
  }

  create(aclRule: Document): Observable<Document> {
    return this.entitySvc.create(Url.document, aclRule); 
  }

  update(aclRule: Document): Observable<Document> {
    return this.entitySvc.update(Url.document, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.document, id);
  }
  
}
