import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ConfigurationComponent } from "./configuration.component";
import { OrganizationComponent } from "./organization/organization.component";
import { OrganizationFormComponent } from "./organization-form/organization-form.component";
import { OrganizationResolverService } from "src/app/services/organization/organization-resolver.service";
import { PlantComponent } from "./plant/plant.component";
import { PlantFormComponent } from "./plant-form/plant-form.component";
import { PlantResolverService } from "src/app/services/plant/plant-resolver.service";
import { LocationCategoryComponent } from "./location-category/location-category.component";
import { LocationCategoryFormComponent } from "./location-category-form/location-category-form.component";
import { LocationcategoryResolverService } from "src/app/services/location-category/location-category-resolver.service";
import { LocationTypeComponent } from "./location-type/location-type.component";
import { LocationTypeFormComponent } from "./location-type-form/location-type-form.component";
import { LocationtypeResolverService } from "src/app/services/location-type/location-type-resolver.service";
import { AssetCategoryComponent } from "./aaset-category/asset-category.component";
import { AssetCategoryFormComponent } from "./asset-category-form/asset-category-form.component";
import { AssetcategoryResolverService } from "src/app/services/asset-category/asset-category-resolver.service";
import { AssetTypeComponent } from "./asset-type/asset-type.component"; 
import { AssetTypeFormComponent } from "./asset-type-form/asset-type-form.component";
import { AssettypeResolverService } from "src/app/services/asset-type/asset-type-resolver.service";
import { InspectionCategoryComponent } from "./inspection-category/inspection-category.component";
import { InspectionCategoryFormComponent } from "./inspection-category-form/inspection-category-form.component";
import { InspectioncategoryResolverService } from "src/app/services/inspection-category/inspection-category-resolver.service";
import { InspectionClassComponent } from "./inspection-class/inspection-class.component";
import { InspectionClassFormComponent } from "./inspection-class-form/inspection-class-form.component";
import { InspectionclassResolverService } from "src/app/services/inspection-class/inspection-class-resolver.service";
import { InspectionCodeComponent } from "./inspection-code/inspection-code.component";
import { InspectionCodeFormComponent } from "./inspection-code-form/inspection-code-form.component";
import { InspectioncodeResolverService } from "src/app/services/inspection-code/inspection-code-resolver.service";
import { InspectionGroupComponent } from "./inspection-group/inspection-group.component";
import { InspectionGroupFormComponent } from "./inspection-group-form/inspection-group-form.component";
import { InspectiongroupResolverService } from "src/app/services/inspection-group/inspection-group-resolver.service";
import { InspectionTypeComponent } from "./inspection-type/inspection-type.component";
import { InspectionTypeFormComponent } from "./inspection-type-form/inspection-type-form.component";
import { InspectiontypeResolverService } from "src/app/services/inspection-types/inspection-type-resolver.service";
import { UnitComponent } from "./units/unit.component";
import { UnitFormComponent } from "./unit-form/unit-form.component";
import { UnitResolverService } from "src/app/services/units/uint-resolver.service";
import { CountryComponent } from "./country/country.component";
import { CountryFormComponent } from "./country-form/country-form.component";
import { CountryResolverService } from "src/app/services/country/country-resolver.service";
import { UserResolverService } from "src/app/services/users/user-resolver.service";


const routes: Routes = [
  {
    path: "",
    component: ConfigurationComponent,
    children: [
      {
        path: "organization",
        component: OrganizationComponent,
      },
      {
        path: "add-organization",
        component: OrganizationFormComponent,
      },
      {
        path: ":id/edit-organization",
        component: OrganizationFormComponent,
        pathMatch: "full",
        resolve: {
          report: OrganizationResolverService,
        },
      },
      {
        path: "plant",
        component: PlantComponent,
      },
      {
        path: "add-plant",
        component: PlantFormComponent,
      },
      {
        path: ":id/edit-plant",
        component: PlantFormComponent,
        pathMatch: "full",
        resolve: {
          report: PlantResolverService,
        },
      },
      {
        path: "location-category",
        component: LocationCategoryComponent,
      },
      {
        path: "add-location-category",
        component: LocationCategoryFormComponent,
      },
      {
        path: ":id/edit-location-category",
        component: LocationCategoryFormComponent,
        pathMatch: "full",
        resolve: {
          report: LocationcategoryResolverService,
        },
      },
      {
        path: "location-type",
        component: LocationTypeComponent,
      },
      {
        path: "add-location-type",
        component: LocationTypeFormComponent,
      },
      {
        path: ":id/edit-location-type",
        component: LocationTypeFormComponent,
        pathMatch: "full",
        resolve: {
          report: LocationtypeResolverService,
        },
      },
      {
        path: "asset-category",
        component: AssetCategoryComponent,
      },
      {
        path: "add-asset-category",
        component: AssetCategoryFormComponent,
      },
      {
        path: ":id/edit-asset-category",
        component: AssetCategoryFormComponent,
        pathMatch: "full",
        resolve: {
          report: AssetcategoryResolverService,
        },
      },
      {
        path: "asset-type",
        component: AssetTypeComponent,
      },
      {
        path: "add-asset-type",
        component: AssetTypeFormComponent,
      },
      {
        path: ":id/edit-asset-type",
        component: AssetTypeFormComponent,
        pathMatch: "full",
        resolve: {
          report: AssettypeResolverService,
        },
      },
      {
        path: "inspection-category",
        component: InspectionCategoryComponent,
      },
      {
        path: "add-inspection-category",
        component: InspectionCategoryFormComponent,
      },
      {
        path: ":id/edit-inspection-category",
        component: InspectionCategoryFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectioncategoryResolverService,
        },
      },
      {
        path: "inspection-type",
        component: InspectionTypeComponent,
      },
      {
        path: "add-inspection-type",
        component: InspectionTypeFormComponent,
      },
      {
        path: ":id/edit-inspection-type",
        component: InspectionTypeFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectiontypeResolverService,
        },
      },
      {
        path: "inspection-class",
        component: InspectionClassComponent,
      },
      {
        path: "add-inspection-class",
        component: InspectionClassFormComponent,
      },
      {
        path: ":id/edit-inspection-class",
        component: InspectionClassFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectionclassResolverService,
        },
      },
      {
        path: "inspection-code",
        component: InspectionCodeComponent,
      },
      {
        path: "add-inspection-code",
        component: InspectionCodeFormComponent,
      },
      {
        path: ":id/edit-inspection-code",
        component: InspectionCodeFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectioncodeResolverService,
        },
      },
      {
        path: "inspection-group",
        component: InspectionGroupComponent,
      },
      {
        path: "add-inspection-group",
        component: InspectionGroupFormComponent,
      },
      {
        path: ":id/edit-inspection-group",
        component: InspectionGroupFormComponent,
        pathMatch: "full",
        resolve: {
          report: InspectiongroupResolverService,
        },
      },
      {
        path: "country",
        component: CountryComponent,
      },
      {
        path: "add-country",
        component: CountryFormComponent,
      },
      {
        path: ":id/edit-country",
        component: CountryFormComponent,
        pathMatch: "full",
        resolve: {
          report: CountryResolverService,
        },
      },
      {
        path: "unit",
        component: UnitComponent,
      },
      {
        path: "add-unit",
        component: UnitFormComponent,
      },
      {
        path: ":id/edit-unit",
        component: UnitFormComponent,
        pathMatch: "full",
        resolve: {
          report: UnitResolverService,
        },
      },
      
  
      { path: "", redirectTo: "organization", pathMatch: "full" },
      { path: "**", redirectTo: "organization", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationRoutingModule {}
