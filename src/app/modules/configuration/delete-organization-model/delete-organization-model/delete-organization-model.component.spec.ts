import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteOrganizationModelComponent } from './delete-organization-model.component';

describe('DeleteOrganizationModelComponent', () => {
  let component: DeleteOrganizationModelComponent;
  let fixture: ComponentFixture<DeleteOrganizationModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteOrganizationModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteOrganizationModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
