import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Unit } from "src/app/services/units/uint.service";

import { State } from "../..";

import { UnitTypes, Union } from "./unit.actions";

export interface UnitState {
  loading: boolean;
  entities: Unit[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: UnitState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function UnitReducer(
  state: UnitState = INITIAL_STATE,
  action: Union
): UnitState {
  switch (action.type) {
    case UnitTypes.GetUnit:
      return {
        ...state,
        loading: true,
      };
    case UnitTypes.GetUnitSuccess:
      const unit: Unit[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: unit,
      };
    case UnitTypes.GetUnitError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case UnitTypes.CreateUnit:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case UnitTypes.CreateUnitsuccess:
      const createdUnit: Unit = action.payload;
      const entitiesAppended = [...state.entities, createdUnit];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case UnitTypes.UpdateUnit:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case UnitTypes.UpdateUnitsuccess:
      const updatedUnit: Unit = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedUnit];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case UnitTypes.UpdateUnitError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case UnitTypes.CreateUnitError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case UnitTypes.DeleteUnit:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case UnitTypes.DeleteUnitsuccess:
      const afterDelete = state.entities.filter((unit) => unit.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case UnitTypes.DeleteUnitError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getUnitState = createFeatureSelector<State, UnitState>("unit");

export const getUnitLoading = createSelector(
  getUnitState,
  (state) => state.loading
);

export const getUnit = createSelector(getUnitState, (state) => state.entities);

export const getUnitCreating = createSelector(
  getUnitState,
  (state) => state.creating
);

export const getUnitCreated = createSelector(
  getUnitState,
  (state) => state.created
);
export const getUnitUpdating = createSelector(
  getUnitState,
  (state) => state.updating
);

export const getUnitUpdated = createSelector(
  getUnitState,
  (state) => state.updated
);
export const getUnitDeleting = createSelector(
  getUnitState,
  (state) => state.deleting
);

export const getUnitDeleted = createSelector(
  getUnitState,
  (state) => state.deleted
);

export const getUnitError = createSelector(
  getUnitState,
  (state) => state.error
);
