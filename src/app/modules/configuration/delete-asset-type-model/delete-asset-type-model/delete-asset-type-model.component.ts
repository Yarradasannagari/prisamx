import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteAssettype} from "src/app/store/stores/asset-type/asset-type.actions";
import { getAssettypeDeleted } from "src/app/store/stores/asset-type/asset-type.store";
@Component({
  selector: "app-delete-asset-type-model",
  templateUrl: "./delete-asset-type-model.component.html",
  styleUrls: ["./delete-asset-type-model.component.scss"],
})
export class DeleteAssetTypeModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteAssettype() {
    this.isLoading = true;
    this.store.dispatch(new DeleteAssettype(this.id));
    this.store
      .select(getAssettypeDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
