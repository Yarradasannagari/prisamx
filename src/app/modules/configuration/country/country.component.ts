import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Country, CountryService } from "src/app/services/country/country.service";
import { GetCountry } from "src/app/store/stores/country/country.actions";
import {
  getCountryError,
  getCountry,
  getCountryLoading,
} from "src/app/store/stores/country/country.store";
import { State, Store } from "../../../store";
import { DeleteCountryModelComponent } from "../delete-country-model/delete-country-model/delete-country-model.component";

@Component({
  selector: "app-country",
  templateUrl: "./country.component.html",
  styleUrls: ["./country.component.scss"],
})
export class CountryComponent implements OnDestroy, OnInit {
  country$: Observable<Country[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  country: Country[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private countryService: CountryService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getCountryLoading);
    this.store.dispatch(new GetCountry());
    this.country$ = this.store.select(getCountry);
    this.store.select(getCountryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  }

  reTry() {
    this.isLoading$ = this.store.select(getCountryLoading);
    this.store.dispatch(new GetCountry());
    this.country$ = this.store.select(getCountry);
    this.store.select(getCountryError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteCountryModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
