import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Partner, PartnerService } from './partner.service';

@Injectable({ providedIn: 'root' })
export class PartnerResolverService implements Resolve<Partner> {
  constructor(private partnerService: PartnerService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Partner> | Promise<Partner> | Partner {
    const id = route.paramMap.get('id');
    return this.partnerService.get(id);
  }
}
