import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { DocumentService } from "../../../services/documents/document.service"; 

import {
  GetDocument,
  GetDocumentSuccess,
  GetDocumentError,
  CreateDocument,
  CreateDocumentsuccess,
  CreateDocumentError,
  DeleteDocument,
  DeleteDocumentsuccess,
  DeleteDocumentError,
  DocumentTypes,
  UpdateDocument,
  UpdateDocumentsuccess,
  UpdateDocumentError,
} from "./document.actions";

@Injectable()
export class DocumentEffects {
  constructor(
    private actions: Actions,
    private documentService: DocumentService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getDocument$: Observable<Action> = this.actions.pipe(
    ofType<GetDocument>(DocumentTypes.GetDocument),
    exhaustMap(() =>
      from(this.documentService.list()).pipe(
        map((payload) => {
          return new GetDocumentSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Document", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetDocumentError(true));
        })
      )
    )
  );

  @Effect()
  createDocument$: Observable<Action> = this.actions.pipe(
    ofType<CreateDocument>(DocumentTypes.CreateDocument),
    concatMap((action) =>
      from(this.documentService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Document has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateDocumentsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Document", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateDocumentError());
        })
      )
    )
  );
  @Effect()
  updateDocument$: Observable<Action> = this.actions.pipe(
    ofType<UpdateDocument>(DocumentTypes.UpdateDocument),
    concatMap((action) =>
      from(this.documentService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Document has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateDocumentsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Document", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateDocumentError());
        })
      )
    )
  );
  @Effect()
  deleteDocument$: Observable<Action> = this.actions.pipe(
    ofType<DeleteDocument>(DocumentTypes.DeleteDocument),
    concatMap((action) =>
      from(this.documentService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Document has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteDocumentsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Document:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteDocumentError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
