import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';

import { PlantService } from './plant.service';

describe('PlantService', () => {
  let service: PlantService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PlantService],
    });
    service = TestBed.inject(PlantService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    // Finally, assert that there are no outstanding requests.
    httpMock.verify();
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
