import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Inspectiontype } from "src/app/services/inspection-types/inspection-type.service";

import { State } from "../..";

import { InspectiontypeTypes, Union } from "./inspection-type.actions";

export interface InspectiontypeState {
  loading: boolean;
  entities: Inspectiontype[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectiontypeState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectiontypeReducer(
  state: InspectiontypeState = INITIAL_STATE,
  action: Union
): InspectiontypeState {
  switch (action.type) {
    case InspectiontypeTypes.GetInspectiontype:
      return {
        ...state,
        loading: true,
      };
    case InspectiontypeTypes.GetInspectiontypeSuccess:
      const inspectiontype: Inspectiontype[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectiontype,
      };
    case InspectiontypeTypes.GetInspectiontypeError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectiontypeTypes.CreateInspectiontype:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectiontypeTypes.CreateInspectiontypesuccess:
      const createdInspectiontype: Inspectiontype = action.payload;
      const entitiesAppended = [...state.entities, createdInspectiontype];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectiontypeTypes.UpdateInspectiontype:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectiontypeTypes.UpdateInspectiontypesuccess:
      const updatedInspectiontype: Inspectiontype = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectiontype];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectiontypeTypes.UpdateInspectiontypeError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectiontypeTypes.CreateInspectiontypeError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectiontypeTypes.DeleteInspectiontype:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectiontypeTypes.DeleteInspectiontypesuccess:
      const afterDelete = state.entities.filter((inspectiontype) => inspectiontype.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectiontypeTypes.DeleteInspectiontypeError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectiontypeState = createFeatureSelector<State, InspectiontypeState>("inspectiontype");

export const getInspectiontypeLoading = createSelector(
  getInspectiontypeState,
  (state) => state.loading
);

export const getInspectiontype = createSelector(getInspectiontypeState, (state) => state.entities);

export const getInspectiontypeCreating = createSelector(
  getInspectiontypeState,
  (state) => state.creating
);

export const getInspectiontypeCreated = createSelector(
  getInspectiontypeState,
  (state) => state.created
);
export const getInspectiontypeUpdating = createSelector(
  getInspectiontypeState,
  (state) => state.updating
);

export const getInspectiontypeUpdated = createSelector(
  getInspectiontypeState,
  (state) => state.updated
);
export const getInspectiontypeDeleting = createSelector(
  getInspectiontypeState,
  (state) => state.deleting
);

export const getInspectiontypeDeleted = createSelector(
  getInspectiontypeState,
  (state) => state.deleted
);

export const getInspectiontypeError = createSelector(
  getInspectiontypeState,
  (state) => state.error
);
