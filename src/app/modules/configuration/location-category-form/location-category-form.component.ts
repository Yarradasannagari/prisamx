import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Locationcategory } from "src/app/services/location-category/location-category.service";
import { CreateLocationcategory, UpdateLocationcategory } from "src/app/store/stores/location-category/location-category.actions";
import { getLocationcategoryCreated, getLocationcategoryUpdated } from "src/app/store/stores/location-category/location-category.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-location-category-form",
  templateUrl: "./location-category-form.component.html",
  styleUrls: ["./location-category-form.component.scss"],
})
export class LocationCategoryFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    category: "",
    name: "",
    active: true,
  };
  locationcategory: Locationcategory;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.category = this.route.snapshot.data["report"].category;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      category: [
        this.formData.category,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const locationcategory: Locationcategory = {
      id: this.formData.id,
      category: this.formData.category,
      name: this.formData.name,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateLocationcategory(locationcategory);
    } else {
      this.createLocationcategory(locationcategory);
    }
  }
  private createLocationcategory(locationcategory: Locationcategory): void {
    this.store.dispatch(new CreateLocationcategory(locationcategory));
    this.store
      .select(getLocationcategoryCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateLocationcategory(locationcategory: Locationcategory): void {
    this.store.dispatch(new UpdateLocationcategory(locationcategory));
    this.store
      .select(getLocationcategoryUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "location-category"]);
  }
}
