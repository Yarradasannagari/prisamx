import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Role } from "src/app/services/roles/role.service";

import { State } from "../..";

import { RoleTypes, Union } from "./roles.actions";

export interface RoleState {
  loading: boolean;
  entities: Role[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: RoleState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function RoleReducer(
  state: RoleState = INITIAL_STATE,
  action: Union
): RoleState {
  switch (action.type) {
    case RoleTypes.GetRoles:
      return {
        ...state,
        loading: true,
      };
    case RoleTypes.GetRolesSuccess:
      const roles: Role[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: roles,
      };
    case RoleTypes.GetRolesError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case RoleTypes.CreateRole:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case RoleTypes.CreateRolesuccess:
      const createdRole: Role = action.payload;
      const entitiesAppended = [...state.entities, createdRole];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case RoleTypes.UpdateRole:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case RoleTypes.UpdateRolesuccess:
      const updatedRole: Role = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedRole];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case RoleTypes.UpdateRoleError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case RoleTypes.CreateRoleError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case RoleTypes.DeleteRole:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case RoleTypes.DeleteRolesuccess:
      const afterDelete = state.entities.filter((role) => role.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case RoleTypes.DeleteRoleError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getRoleState = createFeatureSelector<State, RoleState>("role");

export const getRolesLoading = createSelector(
  getRoleState,
  (state) => state.loading
);

export const getRoles = createSelector(getRoleState, (state) => state.entities);

export const getRoleCreating = createSelector(
  getRoleState,
  (state) => state.creating
);

export const getRoleCreated = createSelector(
  getRoleState,
  (state) => state.created
);
export const getRoleUpdating = createSelector(
  getRoleState,
  (state) => state.updating
);

export const getRoleUpdated = createSelector(
  getRoleState,
  (state) => state.updated
);
export const getRoleDeleting = createSelector(
  getRoleState,
  (state) => state.deleting
);

export const getRoleDeleted = createSelector(
  getRoleState,
  (state) => state.deleted
);

export const getRoleError = createSelector(
  getRoleState,
  (state) => state.error
);
