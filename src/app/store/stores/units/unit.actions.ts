import { Action } from "@ngrx/store";
import { Unit } from "../../../services/units/uint.service"; 

export enum UnitTypes {
  GetUnit = "[Unit] GetUnit",
  GetUnitSuccess = "[Unit] GetUnitSuccess",
  GetUnitError = "[Unit] GetUnitError",
  CreateUnit = "[Unit] CreateUnit",
  CreateUnitsuccess = "[Unit] CreateUnitsuccess",
  CreateUnitError = "[Unit] CreateUnitError",
  UpdateUnit = "[Unit] UpdateUnit",
  UpdateUnitsuccess = "[Unit] UpdateUnitsuccess",
  UpdateUnitError = "[Unit] UpdateUnitError",
  DeleteUnit = "[Unit] DeleteUnit",
  DeleteUnitError = "[Unit] DeleteUnitError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteUnitsuccess = "[AclRule] DeleteUnitsuccess",
}

export class GetUnit implements Action { 
  readonly type = UnitTypes.GetUnit;
}

export class GetUnitSuccess implements Action {
  readonly type = UnitTypes.GetUnitSuccess;
  constructor(readonly payload: Unit[]) {}
}
export class GetUnitError implements Action {
  readonly type = UnitTypes.GetUnitError;
  constructor(readonly payload: boolean) {}
}

export class CreateUnit implements Action {
  readonly type = UnitTypes.CreateUnit;
  constructor(readonly payload: Unit) {}
}

export class CreateUnitsuccess implements Action {
  readonly type = UnitTypes.CreateUnitsuccess;
  constructor(readonly payload: Unit) {}
}

export class CreateUnitError implements Action {
  readonly type = UnitTypes.CreateUnitError;
}
export class UpdateUnit implements Action {
  readonly type = UnitTypes.UpdateUnit;
  constructor(readonly payload: Unit) {}
}

export class UpdateUnitsuccess implements Action {
  readonly type = UnitTypes.UpdateUnitsuccess;
  constructor(readonly payload: Unit) {}
}

export class UpdateUnitError implements Action {
  readonly type = UnitTypes.UpdateUnitError;
}

export class DeleteUnit implements Action {
  readonly type = UnitTypes.DeleteUnit;
  constructor(readonly payload: string) {}
}

export class DeleteUnitsuccess implements Action {
  readonly type = UnitTypes.DeleteUnitsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteUnitError implements Action {
  readonly type = UnitTypes.DeleteUnitError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetUnit
  | GetUnitSuccess
  | GetUnitError
  | CreateUnit
  | CreateUnitsuccess
  | CreateUnitError
  | DeleteUnit
  | DeleteUnitsuccess
  | DeleteUnitError
  | UpdateUnit
  | UpdateUnitsuccess
  | UpdateUnitError
