import { Action } from "@ngrx/store";
import { Inspectioncode } from "../../../services/inspection-code/inspection-code.service"; 

export enum InspectioncodeTypes {
  GetInspectioncode = "[Inspectioncode] GetInspectioncode",
  GetInspectioncodeSuccess = "[Inspectioncode] GetInspectioncodeSuccess",
  GetInspectioncodeError = "[Inspectioncode] GetInspectioncodeError",
  CreateInspectioncode = "[Inspectioncode] CreateInspectioncode",
  CreateInspectioncodesuccess = "[Inspectioncode] CreateInspectioncodesuccess",
  CreateInspectioncodeError = "[Inspectioncode] CreateInspectioncodeError",
  UpdateInspectioncode = "[Inspectioncode] UpdateInspectioncode",
  UpdateInspectioncodesuccess = "[Inspectioncode] UpdateInspectioncodesuccess",
  UpdateInspectioncodeError = "[Inspectioncode] UpdateInspectioncodeError",
  DeleteInspectioncode = "[Inspectioncode] DeleteInspectioncode",
  DeleteInspectioncodeError = "[Inspectioncode] DeleteInspectioncodeError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectioncodesuccess = "[AclRule] DeleteInspectioncodesuccess",
}

export class GetInspectioncode implements Action { 
  readonly type = InspectioncodeTypes.GetInspectioncode;
}

export class GetInspectioncodeSuccess implements Action {
  readonly type = InspectioncodeTypes.GetInspectioncodeSuccess;
  constructor(readonly payload: Inspectioncode[]) {}
}
export class GetInspectioncodeError implements Action {
  readonly type = InspectioncodeTypes.GetInspectioncodeError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectioncode implements Action {
  readonly type = InspectioncodeTypes.CreateInspectioncode;
  constructor(readonly payload: Inspectioncode) {}
}

export class CreateInspectioncodesuccess implements Action {
  readonly type = InspectioncodeTypes.CreateInspectioncodesuccess;
  constructor(readonly payload: Inspectioncode) {}
}

export class CreateInspectioncodeError implements Action {
  readonly type = InspectioncodeTypes.CreateInspectioncodeError;
}
export class UpdateInspectioncode implements Action {
  readonly type = InspectioncodeTypes.UpdateInspectioncode;
  constructor(readonly payload: Inspectioncode) {}
}

export class UpdateInspectioncodesuccess implements Action {
  readonly type = InspectioncodeTypes.UpdateInspectioncodesuccess;
  constructor(readonly payload: Inspectioncode) {}
}

export class UpdateInspectioncodeError implements Action {
  readonly type = InspectioncodeTypes.UpdateInspectioncodeError;
}

export class DeleteInspectioncode implements Action {
  readonly type = InspectioncodeTypes.DeleteInspectioncode;
  constructor(readonly payload: string) {}
}

export class DeleteInspectioncodesuccess implements Action {
  readonly type = InspectioncodeTypes.DeleteInspectioncodesuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectioncodeError implements Action {
  readonly type = InspectioncodeTypes.DeleteInspectioncodeError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectioncode
  | GetInspectioncodeSuccess
  | GetInspectioncodeError
  | CreateInspectioncode
  | CreateInspectioncodesuccess
  | CreateInspectioncodeError
  | DeleteInspectioncode
  | DeleteInspectioncodesuccess
  | DeleteInspectioncodeError
  | UpdateInspectioncode
  | UpdateInspectioncodesuccess
  | UpdateInspectioncodeError
