import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Inspectioncategory } from "src/app/services/inspection-category/inspection-category.service";

import { State } from "../..";

import { InspectioncategoryTypes, Union } from "./inspection-category.actions";

export interface InspectioncategoryState {
  loading: boolean;
  entities: Inspectioncategory[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectioncategoryState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectioncategoryReducer(
  state: InspectioncategoryState = INITIAL_STATE,
  action: Union
): InspectioncategoryState {
  switch (action.type) {
    case InspectioncategoryTypes.GetInspectioncategory:
      return {
        ...state,
        loading: true,
      };
    case InspectioncategoryTypes.GetInspectioncategorySuccess:
      const inspectioncategory: Inspectioncategory[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectioncategory,
      };
    case InspectioncategoryTypes.GetInspectioncategoryError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectioncategoryTypes.CreateInspectioncategory:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectioncategoryTypes.CreateInspectioncategorysuccess:
      const createdInspectioncategory: Inspectioncategory = action.payload;
      const entitiesAppended = [...state.entities, createdInspectioncategory];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectioncategoryTypes.UpdateInspectioncategory:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectioncategoryTypes.UpdateInspectioncategorysuccess:
      const updatedInspectioncategory: Inspectioncategory = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectioncategory];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectioncategoryTypes.UpdateInspectioncategoryError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectioncategoryTypes.CreateInspectioncategoryError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectioncategoryTypes.DeleteInspectioncategory:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectioncategoryTypes.DeleteInspectioncategorysuccess:
      const afterDelete = state.entities.filter((inspectioncategory) => inspectioncategory.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectioncategoryTypes.DeleteInspectioncategoryError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectioncategoryState = createFeatureSelector<State, InspectioncategoryState>("inspectioncategory");

export const getInspectioncategoryLoading = createSelector(
  getInspectioncategoryState,
  (state) => state.loading
);

export const getInspectioncategory = createSelector(getInspectioncategoryState, (state) => state.entities);

export const getInspectioncategoryCreating = createSelector(
  getInspectioncategoryState,
  (state) => state.creating
);

export const getInspectioncategoryCreated = createSelector(
  getInspectioncategoryState,
  (state) => state.created
);
export const getInspectioncategoryUpdating = createSelector(
  getInspectioncategoryState,
  (state) => state.updating
);

export const getInspectioncategoryUpdated = createSelector(
  getInspectioncategoryState,
  (state) => state.updated
);
export const getInspectioncategoryDeleting = createSelector(
  getInspectioncategoryState,
  (state) => state.deleting
);

export const getInspectioncategoryDeleted = createSelector(
  getInspectioncategoryState,
  (state) => state.deleted
);

export const getInspectioncategoryError = createSelector(
  getInspectioncategoryState,
  (state) => state.error
);
