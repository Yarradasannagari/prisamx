import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { InspectionPoint } from "src/app/services/inspection-point/inspection-point.service";
import { CreateInspectionPoint, UpdateInspectionPoint } from "src/app/store/stores/inspection-point/inspection-point.actions";
import { getInspectionPointCreated, getInspectionPointUpdated } from "src/app/store/stores/inspection-point/inspection-point.store";
import { State, Store } from "../../../store";
import { Location } from "src/app/services/locations/location.service";
import { GetLocation } from "src/app/store/stores/locations/location.actions";
import { getLocation } from "src/app/store/stores/locations/location.store";
import { Asset } from "src/app/services/assets/asset.service";
import { GetAsset } from "src/app/store/stores/assets/asset.actions";
import { getAsset } from "src/app/store/stores/assets/asset.store";

@Component({
  selector: "app-view-inspection-point",
  templateUrl: "./view-inspection-point.component.html",
  styleUrls: ["./view-inspection-point.component.scss"],
})
export class ViewInspectionPointComponent implements OnInit {
  routeId: any;
  isLoading = false;
  location: Location[];
  asset: Asset[];
  formGroup: FormGroup;
  formData = {
    id: null,
    objtyp: "", 
    point: "",
    position: "",
    name: "",
    catergory: "",
    locationId: "",
    assetId: "",

    active: true,
  };
  inspectionPoint: InspectionPoint;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(new GetLocation());
    this.store.select(getLocation).subscribe((location) => {
      this.location = location;
    });

    this.store.dispatch(new GetAsset());
    this.store.select(getAsset).subscribe((asset) => {
      this.asset = asset;
    });
   }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.objtyp = this.route.snapshot.data["report"].objtyp;
        this.formData.point = this.route.snapshot.data["report"].point;
        this.formData.position = this.route.snapshot.data["report"].position;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.catergory = this.route.snapshot.data["report"].catergory;
        this.formData.locationId = this.route.snapshot.data["report"].locationId;
        this.formData.assetId = this.route.snapshot.data["report"].assetId;

        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      objtyp: [
        this.formData.objtyp,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      point: [
        this.formData.point,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      position: [
        this.formData.position,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      catergory: [
        this.formData.catergory,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],   
      locationId: [
        this.formData.locationId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      assetId: [
        this.formData.assetId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const inspectionPoint: InspectionPoint = {
      id: this.formData.id,
      objtyp: this.formData.objtyp,
      point: this.formData.point,
      position: this.formData.position,
      name: this.formData.name,
      catergory: this.formData.catergory,
      locationId: this.formData.locationId,
      assetId: this.formData.assetId,

      // active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateInspectionpoint(inspectionPoint);
    } else {
      this.createInspectionpoint(inspectionPoint);
    }
  }
  private createInspectionpoint(inspectionPoint: InspectionPoint): void {
    this.store.dispatch(new CreateInspectionPoint(inspectionPoint));
    this.store
      .select(getInspectionPointCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateInspectionpoint(inspectionPoint: InspectionPoint): void {
    this.store.dispatch(new UpdateInspectionPoint(inspectionPoint));
    this.store
      .select(getInspectionPointUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["inspection-points", "inspection-point"]);
  }
}
