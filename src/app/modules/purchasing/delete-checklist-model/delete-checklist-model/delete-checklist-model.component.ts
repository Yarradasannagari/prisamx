import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteCheckList } from "src/app/store/stores/checklist/checklist.actions";
import { getCheckListDeleted } from "src/app/store/stores/checklist/checklist.store";
@Component({
  selector: "app-delete-checklist-model",
  templateUrl: "./delete-checklist-model.component.html",
  styleUrls: ["./delete-checklist-model.component.scss"],
})
export class DeleteCheckListModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteCheckList() {
    this.isLoading = true;
    this.store.dispatch(new DeleteCheckList(this.id));
    this.store
      .select(getCheckListDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
