import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionCategoryFormComponent } from './inspection-category-form.component';

describe('InspectionCategoryFormComponent', () => {
  let component: InspectionCategoryFormComponent;
  let fixture: ComponentFixture<InspectionCategoryFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionCategoryFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
