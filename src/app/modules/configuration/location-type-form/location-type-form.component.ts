import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Locationtype } from "src/app/services/location-type/location-type.service";
import { CreateLocationtype, UpdateLocationtype } from "src/app/store/stores/location-type/location-type.actions";
import { getLocationtypeCreated, getLocationtypeUpdated } from "src/app/store/stores/location-type/location-tye.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-location-type-form", 
  templateUrl: "./location-type-form.component.html",
  styleUrls: ["./location-type-form.component.scss"],
})
export class LocationTypeFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    name: "",
    type: "",
    active: true,
  };
  locationtype: Locationtype;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.type = this.route.snapshot.data["report"].type;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      type: [
        this.formData.type,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const locationtype: Locationtype = {
      id: this.formData.id,
      name: this.formData.name,
      type: this.formData.type,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateLocationtype(locationtype);
    } else {
      this.createLocationtype(locationtype);
    }
  }
  private createLocationtype(locationtype: Locationtype): void {
    this.store.dispatch(new CreateLocationtype(locationtype));
    this.store
      .select(getLocationtypeCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateLocationtype(locationtype: Locationtype): void {
    this.store.dispatch(new UpdateLocationtype(locationtype));
    this.store
      .select(getLocationtypeUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "location-type"]);
  }
}
