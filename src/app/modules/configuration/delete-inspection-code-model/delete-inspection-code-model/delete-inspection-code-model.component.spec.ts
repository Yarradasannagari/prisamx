import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionCodeModelComponent } from './delete-inspection-code-model.component';

describe('DeleteInspectionCodeModelComponent', () => {
  let component: DeleteInspectionCodeModelComponent;
  let fixture: ComponentFixture<DeleteInspectionCodeModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionCodeModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionCodeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
