import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Assetcategory } from "src/app/services/asset-category/asset-category.service";

import { State } from "../..";

import { AssetcategoryTypes, Union } from "./asset-category.actions";

export interface AssetcategoryState {
  loading: boolean;
  entities: Assetcategory[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: AssetcategoryState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function AssetcategoryReducer(
  state: AssetcategoryState = INITIAL_STATE,
  action: Union
): AssetcategoryState {
  switch (action.type) {
    case AssetcategoryTypes.GetAssetcategory:
      return {
        ...state,
        loading: true,
      };
    case AssetcategoryTypes.GetAssetcategorySuccess:
      const assetcategory: Assetcategory[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: assetcategory,
      };
    case AssetcategoryTypes.GetAssetcategoryError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case AssetcategoryTypes.CreateAssetcategory:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case AssetcategoryTypes.CreateAssetcategorysuccess:
      const createdAssetcategory: Assetcategory = action.payload;
      const entitiesAppended = [...state.entities, createdAssetcategory];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case AssetcategoryTypes.UpdateAssetcategory:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case AssetcategoryTypes.UpdateAssetcategorysuccess:
      const updatedAssetcategory: Assetcategory = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedAssetcategory];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case AssetcategoryTypes.UpdateAssetcategoryError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case AssetcategoryTypes.CreateAssetcategory:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case AssetcategoryTypes.DeleteAssetcategory:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case AssetcategoryTypes.DeleteAssetcategorysuccess:
      const afterDelete = state.entities.filter((assetcategory) => assetcategory.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case AssetcategoryTypes.DeleteAssetcategoryError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getAssetcategoryState = createFeatureSelector<State, AssetcategoryState>("assetcategory");

export const getAssetcategoryLoading = createSelector(
  getAssetcategoryState,
  (state) => state.loading
);

export const getAssetcategory = createSelector(getAssetcategoryState, (state) => state.entities);

export const getAssetcategoryCreating = createSelector(
  getAssetcategoryState,
  (state) => state.creating
);

export const getAssetcategoryCreated = createSelector(
  getAssetcategoryState,
  (state) => state.created
);
export const getAssetcategoryUpdating = createSelector(
  getAssetcategoryState,
  (state) => state.updating
);

export const getAssetcategoryUpdated = createSelector(
  getAssetcategoryState,
  (state) => state.updated
);
export const getAssetcategoryDeleting = createSelector(
  getAssetcategoryState,
  (state) => state.deleting
);

export const getAssetcategoryDeleted = createSelector(
  getAssetcategoryState,
  (state) => state.deleted
);

export const getAssetcategoryError = createSelector(
  getAssetcategoryState,
  (state) => state.error
);
