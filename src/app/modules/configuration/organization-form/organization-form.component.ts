import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Organization } from "src/app/services/organization/organization.service";
import { CreateOrganization, UpdateOrganization } from "src/app/store/stores/organizations/organizations.actions";
import { getOrganizationCreated, getOrganizationUpdated } from "src/app/store/stores/organizations/organizations.store";
import { State, Store } from "../../../store";
import { Country } from "src/app/services/country/country.service";
import { GetCountry } from "src/app/store/stores/country/country.actions";
import { getCountry } from "src/app/store/stores/country/country.store";

@Component({
  selector: "app-organization-form",
  templateUrl: "./organization-form.component.html",
  styleUrls: ["./organization-form.component.scss"],
})
export class OrganizationFormComponent implements OnInit { 
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    ordId: " ",
    name: " ",
    country: " ",
    currency: " ",
    active: true, 

  };
  organization: Organization;
  country: Country[];
  toppings1 = new FormControl();

  toppings2 = new FormControl();
  toppingList: string[] = [
    "INR",
    "USD",
    "Euro",
    "AED",
    "MYR",
  ];

  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(new GetCountry());
    this.store.select(getCountry).subscribe((country) => {
      this.country = country;
    });
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["plant"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.ordId = this.route.snapshot.data["report"].ordId;
        this.formData.name =
          this.route.snapshot.data["report"].name;
          this.formData.country = this.route.snapshot.data["report"].country;

        this.formData.currency = this.route.snapshot.data["report"].currency;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.plantId);
    this.formGroup = this.fb.group({
      ordId: [
        this.formData.ordId,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(10),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(50),
        ]),
      ],
      country: [
        this.formData.country,
        Validators.compose([
          Validators.required,
          
        ]),
      ],
      currency: [
        this.formData.currency,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const organization: Organization = {
      id: this.formData.id,
      ordId: this.formData.ordId,
      name: this.formData.name,
      country: this.formData.country,
      currency: this.formData.currency,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateOrganization(organization);
    } else {
      this.createOrganization(organization);
    }
  }
  private createOrganization(organization: Organization): void {
    this.store.dispatch(new CreateOrganization(organization));
    this.store
      .select(getOrganizationCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateOrganization(organization: Organization): void {
    this.store.dispatch(new UpdateOrganization(organization));
    this.store
      .select(getOrganizationUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "organization"]);
  }
}
