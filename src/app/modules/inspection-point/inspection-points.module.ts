import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxPaginationModule } from 'ngx-pagination';

import { WidgetsModule } from '../../_metronic/partials/content/widgets/widgets.module';
import { InspectionPointsComponent } from "./inspection-points.component";
import { InspectionPointRoutingModule } from "./inspection-points-routing.module";
import { InlineSVGModule } from 'ng-inline-svg';
import { ActiveRolesPipe } from './pipes/active-roles.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { Ng2OrderModule } from 'ng2-order-pipe';

import { InspectionPointComponent } from "./inspection-point/inspection-point.component";
import { InspectionPointFormComponent } from "./inspection-point-form/inspection-point-form.component";
import { DeleteInspectionPointModelComponent } from './delete-inspection-point-model/delete-inspection-point-model/delete-inspection-point-model.component';
import { ViewInspectionPointComponent } from "./view-inspection-point/view-inspection-point.component";


@NgModule({
  declarations: [
    InspectionPointsComponent,
    ActiveRolesPipe,
    SearchPipe,
    InspectionPointComponent,
    InspectionPointFormComponent,
    DeleteInspectionPointModelComponent,
    ViewInspectionPointComponent
  ],
  imports: [
    CommonModule,
    Ng2OrderModule,
    InspectionPointRoutingModule,
    CommonModule,
    NgxPaginationModule,
    CommonModule,
    FormsModule,
    InlineSVGModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    NgbModule,
    ReactiveFormsModule,
    WidgetsModule,
  ],
})
export class InspectionPointModule {}
