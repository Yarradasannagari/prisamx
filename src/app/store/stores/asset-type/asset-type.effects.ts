import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { AssettypeService } from "../../../services/asset-type/asset-type.service"; 

import {
  GetAssettype,
  GetAssettypeSuccess,
  GetAssettypeError,
  CreateAssettype,
  CreateAssettypesuccess,
  CreateAssettypeError,
  DeleteAssettype,
  DeleteAssettypesuccess,
  DeleteAssettypeError,
  AssettypeTypes,
  UpdateAssettype,
  UpdateAssettypesuccess,
  UpdateAssettypeError,
} from "./asset-type.actions";

@Injectable()
export class AssettypeEffects {
  constructor(
    private actions: Actions,
    private assettypeService: AssettypeService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getAssettype$: Observable<Action> = this.actions.pipe(
    ofType<GetAssettype>(AssettypeTypes.GetAssettype),
    exhaustMap(() =>
      from(this.assettypeService.list()).pipe(
        map((payload) => {
          return new GetAssettypeSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Asset Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetAssettypeError(true));
        })
      )
    )
  );

  @Effect()
  createAssettype$: Observable<Action> = this.actions.pipe(
    ofType<CreateAssettype>(AssettypeTypes.CreateAssettype),
    concatMap((action) =>
      from(this.assettypeService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset Type has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateAssettypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Asset Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateAssettypeError());
        })
      )
    )
  );
  @Effect()
  updateAssettype$: Observable<Action> = this.actions.pipe(
    ofType<UpdateAssettype>(AssettypeTypes.UpdateAssettype),
    concatMap((action) =>
      from(this.assettypeService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Asset Type has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateAssettypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Asset Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateAssettypeError());
        })
      )
    )
  );
  @Effect()
  deleteAssettype$: Observable<Action> = this.actions.pipe(
    ofType<DeleteAssettype>(AssettypeTypes.DeleteAssettype),
    concatMap((action) =>
      from(this.assettypeService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Asset Type has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteAssettypesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Asset Type:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteAssettypeError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
