import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Checklist {
  // active: boolean;
  changedBy: string;
  changedDate: string;
  changedTime: string;
  clqtr: string;
  createdBy: string;
  createdDate: string;
  createdTime: string;
  fc: string; 
  group: string;
  id: string;
  nc: string;
  orgid: string;
  pc: string;
  pd_num: string;
  plant: string;
  quantity: string;
  sequence: string;
  store_room: string;
  tran_id: string;
  vendor: string;
  year: string;
  

}


@Injectable({
  providedIn: 'root',
})
export class ChecklistService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Checklist> {
    return this.entitySvc.get(Url.checklist, id);
  }

  list(params?: Record<string, any>): Observable<Checklist[]> {
    return this.entitySvc.list(Url.checklist, params);
  }

  create(aclRule: Checklist): Observable<Checklist> {
    return this.entitySvc.create(Url.checklist, aclRule); 
  }

  update(aclRule: Checklist): Observable<Checklist> {
    return this.entitySvc.update(Url.checklist, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.checklist, id);
  }
  
}
