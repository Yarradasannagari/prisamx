import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteUnit} from "src/app/store/stores/units/unit.actions";
import { getUnitDeleted } from "src/app/store/stores/units/unit.store";
@Component({
  selector: "app-delete-unit-model",
  templateUrl: "./delete-unit-model.component.html",
  styleUrls: ["./delete-unit-model.component.scss"],
})
export class DeleteUnitModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteUnit() {
    this.isLoading = true;
    this.store.dispatch(new DeleteUnit(this.id));
    this.store
      .select(getUnitDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
