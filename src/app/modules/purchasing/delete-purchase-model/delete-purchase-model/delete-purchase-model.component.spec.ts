import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePurchaseModelComponent } from './delete-purchase-model.component';

describe('DeletePurchaseModelComponent', () => {
  let component: DeletePurchaseModelComponent;
  let fixture: ComponentFixture<DeletePurchaseModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletePurchaseModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePurchaseModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
