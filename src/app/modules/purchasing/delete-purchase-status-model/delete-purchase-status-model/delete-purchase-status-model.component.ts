import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeletePurchaseStatus } from "src/app/store/stores/purchase -status/purchase-status.actions";
import { getPurchaseStatusDeleted } from "src/app/store/stores/purchase -status/purchase-status.store";
@Component({
  selector: "app-delete-purchase-status-model",
  templateUrl: "./delete-purchase-status-model.component.html",
  styleUrls: ["./delete-purchase-status-model.component.scss"],
})
export class DeletePurchaseStatusModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deletePurchaseStatus() {
    this.isLoading = true;
    this.store.dispatch(new DeletePurchaseStatus(this.id));
    this.store
      .select(getPurchaseStatusDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
