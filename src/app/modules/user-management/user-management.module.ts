import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2OrderModule } from 'ng2-order-pipe';

import { WidgetsModule } from '../../_metronic/partials/content/widgets/widgets.module';
import { UsersComponent } from "./users/users.component";
import { RolesComponent } from "./roles/roles.component";
import { UserManagementComponent } from "./user-management.component";
import { UserManagementRoutingModule } from "./user-management-routing.module";
import { RolesFormComponent } from "./roles-form/roles-form.component";
import { InlineSVGModule } from 'ng-inline-svg';
import { DeleteRoleModelComponent } from './delete-role-model/delete-role-model/delete-role-model.component';
import { UserFormComponent } from './user-form/user-form/user-form.component';
import { ActiveRolesPipe } from './pipes/active-roles.pipe';
import { SearchPipe } from './pipes/search.pipe';

import { GroupComponent } from "./group/group.component";
import { GroupFormComponent } from "./group-form/group-form.component";
import { DeleteGroupModelComponent } from './delete-group-model/delete-group-model/delete-group-model.component';


@NgModule({
  declarations: [
    UsersComponent,
    RolesComponent,
    UserManagementComponent,
    RolesFormComponent,
    DeleteRoleModelComponent,
    UserFormComponent,
    ActiveRolesPipe,
    SearchPipe,
    GroupComponent,
    GroupFormComponent,
    DeleteGroupModelComponent
  ],
  imports: [
    CommonModule,
    Ng2OrderModule,
    UserManagementRoutingModule,
    CommonModule,
    NgxPaginationModule,
    CommonModule,
    FormsModule,
    InlineSVGModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMomentDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    NgbModule,
    ReactiveFormsModule,
    WidgetsModule,
  ],
})
export class UserManagementModule {}
