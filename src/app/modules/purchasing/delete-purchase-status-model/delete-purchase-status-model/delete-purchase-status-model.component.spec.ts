import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePurchaseStatusModelComponent } from './delete-purchase-status-model.component';

describe('DeletePurchaseStatusModelComponent', () => {
  let component: DeletePurchaseStatusModelComponent;
  let fixture: ComponentFixture<DeletePurchaseStatusModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletePurchaseStatusModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePurchaseStatusModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
