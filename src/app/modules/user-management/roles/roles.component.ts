import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Role, RoleService } from "src/app/services/roles/role.service";
import { GetRoles } from "src/app/store/stores/roles/roles.actions";
import {
  getRoleError,
  getRoles,
  getRolesLoading,
} from "src/app/store/stores/roles/roles.store";
import { State, Store } from "../../../store";
import { DeleteRoleModelComponent } from "../delete-role-model/delete-role-model/delete-role-model.component";

@Component({
  selector: "app-roles",
  templateUrl: "./roles.component.html",
  styleUrls: ["./roles.component.scss"],
})
export class RolesComponent implements OnDestroy, OnInit {
  roles$: Observable<Role[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  roles: Role[];
  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];
  
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private roleService: RoleService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getRolesLoading);
    this.store.dispatch(new GetRoles());
    this.roles$ = this.store.select(getRoles);
    this.store.select(getRoleError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getRolesLoading);
    this.store.dispatch(new GetRoles());
    this.roles$ = this.store.select(getRoles);
    this.store.select(getRoleError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteRoleModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
