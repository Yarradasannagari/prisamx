import { createFeatureSelector, createSelector } from "@ngrx/store";
import { InspectionPoint } from "src/app/services/inspection-point/inspection-point.service";

import { State } from "../..";

import { InspectionPointTypes, Union } from "./inspection-point.actions";

export interface InspectionPointState {
  loading: boolean;
  entities: InspectionPoint[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectionPointState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectionPointReducer(
  state: InspectionPointState = INITIAL_STATE,
  action: Union
): InspectionPointState {
  switch (action.type) {
    case InspectionPointTypes.GetInspectionPoint:
      return {
        ...state,
        loading: true,
      };
    case InspectionPointTypes.GetInspectionPointSuccess:
      const inspectionPoint: InspectionPoint[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectionPoint,
      };
    case InspectionPointTypes.GetInspectionPointError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectionPointTypes.CreateInspectionPoint:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectionPointTypes.CreateInspectionPointsuccess:
      const createdInspectionPoint: InspectionPoint = action.payload;
      const entitiesAppended = [...state.entities, createdInspectionPoint];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectionPointTypes.UpdateInspectionPoint:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectionPointTypes.UpdateInspectionPointsuccess:
      const updatedInspectionPoint: InspectionPoint = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectionPoint];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectionPointTypes.UpdateInspectionPointError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectionPointTypes.CreateInspectionPointError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectionPointTypes.DeleteInspectionPoint:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectionPointTypes.DeleteInspectionPointsuccess:
      const afterDelete = state.entities.filter((inspectionpoint) => inspectionpoint.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectionPointTypes.DeleteInspectionPointError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectionPointState = createFeatureSelector<State, InspectionPointState>("inspectionpoint");

export const getInspectionPointLoading = createSelector(
  getInspectionPointState,
  (state) => state.loading
);

export const getInspectionPoint = createSelector(getInspectionPointState, (state) => state.entities);

export const getInspectionPointCreating = createSelector(
  getInspectionPointState,
  (state) => state.creating
);

export const getInspectionPointCreated = createSelector(
  getInspectionPointState,
  (state) => state.created
);
export const getInspectionPointUpdating = createSelector(
  getInspectionPointState,
  (state) => state.updating
);

export const getInspectionPointUpdated = createSelector(
  getInspectionPointState,
  (state) => state.updated
);
export const getInspectionPointDeleting = createSelector(
  getInspectionPointState,
  (state) => state.deleting
);

export const getInspectionPointDeleted = createSelector(
  getInspectionPointState,
  (state) => state.deleted
);

export const getInspectionPointError = createSelector(
  getInspectionPointState,
  (state) => state.error
);
