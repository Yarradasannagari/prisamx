import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PurchaseStatus } from "src/app/services/purchase -status/purchase-status.service";

import { State } from "../..";

import { PurchaseStatusTypes, Union } from "./purchase-status.actions";

export interface PurchaseStatusState {
  loading: boolean;
  entities: PurchaseStatus[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: PurchaseStatusState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function PurchaseStatusReducer(
  state: PurchaseStatusState = INITIAL_STATE,
  action: Union
): PurchaseStatusState {
  switch (action.type) {
    case PurchaseStatusTypes.GetPurchaseStatus:
      return {
        ...state,
        loading: true,
      };
    case PurchaseStatusTypes.GetPurchaseStatusSuccess:
      const purchaseStatus: PurchaseStatus[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: purchaseStatus,
      };
    case PurchaseStatusTypes.GetPurchaseStatusError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case PurchaseStatusTypes.CreatePurchaseStatus:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case PurchaseStatusTypes.CreatePurchaseStatussuccess:
      const createdPurchaseStatus: PurchaseStatus = action.payload;
      const entitiesAppended = [...state.entities, createdPurchaseStatus];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case PurchaseStatusTypes.UpdatePurchaseStatus:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case PurchaseStatusTypes.UpdatePurchaseStatussuccess:
      const updatedPurchaseStatus: PurchaseStatus = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedPurchaseStatus];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case PurchaseStatusTypes.UpdatePurchaseStatusError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case PurchaseStatusTypes.CreatePurchaseStatusError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case PurchaseStatusTypes.DeletePurchaseStatus:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case PurchaseStatusTypes.DeletePurchaseStatussuccess:
      const afterDelete = state.entities.filter((purchasestatus) => purchasestatus.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case PurchaseStatusTypes.DeletePurchaseStatusError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getPurchaseStatusState = createFeatureSelector<State, PurchaseStatusState>("otfitems");

export const getPurchaseStatusLoading = createSelector(
  getPurchaseStatusState,
  (state) => state.loading
);

export const getPurchaseStatus = createSelector(getPurchaseStatusState, (state) => state.entities);

export const getPurchaseCreating = createSelector(
  getPurchaseStatusState,
  (state) => state.creating
);

export const getPurchaseStatusCreated = createSelector(
  getPurchaseStatusState,
  (state) => state.created
);
export const getPurchaseStatusUpdating = createSelector(
  getPurchaseStatusState,
  (state) => state.updating
);

export const getPurchaseStatusUpdated = createSelector(
  getPurchaseStatusState,
  (state) => state.updated
);
export const getPurchaseStatusDeleting = createSelector(
  getPurchaseStatusState,
  (state) => state.deleting
);

export const getPurchaseStatusDeleted = createSelector(
  getPurchaseStatusState,
  (state) => state.deleted
);

export const getPurchaseStatusError = createSelector(
  getPurchaseStatusState,
  (state) => state.error
);
