import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json',
  }),
};

export interface Entity {
  id?: number;
}

@Injectable({ providedIn: 'root' })
export class EntityService<T extends Entity> {
  constructor(private httpClient: HttpClient) {}

  get(url: string, id: string) {
    return this.httpClient.get<T>(`${url}/${id}`);
  }

  list(url: string, params?: Record<string, any>) {
    return this.httpClient.get<T[]>(url, { params });
  }

  create(url: string, entity: Partial<T>) {
    return this.httpClient.post<T>(url, entity, httpOptions);
  }
  upload(url: string, entity: Partial<T>) {
    return this.httpClient.post<T>(url, entity, httpOptions);
  }

  update(url: string, entity: T) {
    return this.httpClient.put<T>(`${url}`, entity, httpOptions);
  }

  delete(url: string, id: string) {
    return this.httpClient.delete<void>(`${url}/${id}`, httpOptions);
  }
  
}
