import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Location } from "src/app/services/locations/location.service";

import { State } from "../..";

import { LocationTypes, Union } from "./location.actions";

export interface LocationState {
  loading: boolean;
  entities: Location[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: LocationState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function LocationReducer(
  state: LocationState = INITIAL_STATE,
  action: Union
): LocationState {
  switch (action.type) {
    case LocationTypes.GetLocation:
      return {
        ...state,
        loading: true,
      };
    case LocationTypes.GetLocationSuccess:
      const location: Location[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: location,
      };
    case LocationTypes.GetLocationError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case LocationTypes.CreateLocation:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case LocationTypes.CreateLocationsuccess:
      const createdLocation: Location = action.payload;
      const entitiesAppended = [...state.entities, createdLocation];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case LocationTypes.UpdateLocation:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case LocationTypes.UpdateLocationsuccess:
      const updatedLocation: Location = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedLocation];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case LocationTypes.UpdateLocationError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case LocationTypes.CreateLocationError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case LocationTypes.DeleteLocation:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case LocationTypes.DeleteLocationsuccess:
      const afterDelete = state.entities.filter((location) => location.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case LocationTypes.DeleteLocationError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getLocationState = createFeatureSelector<State, LocationState>("location");

export const getLocationLoading = createSelector(
  getLocationState,
  (state) => state.loading
);

export const getLocation = createSelector(getLocationState, (state) => state.entities);

export const getLocationCreating = createSelector(
  getLocationState,
  (state) => state.creating
);

export const getLocationCreated = createSelector(
  getLocationState,
  (state) => state.created
);
export const getLocationUpdating = createSelector(
  getLocationState,
  (state) => state.updating
);

export const getLocationUpdated = createSelector(
  getLocationState,
  (state) => state.updated
);
export const getLocationDeleting = createSelector(
  getLocationState,
  (state) => state.deleting
);

export const getLocationDeleted = createSelector(
  getLocationState,
  (state) => state.deleted
);

export const getLocationError = createSelector(
  getLocationState,
  (state) => state.error
);
