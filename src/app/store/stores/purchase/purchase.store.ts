import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Purchase } from "src/app/services/purchase/purchase.service";

import { State } from "../..";

import { PurchaseTypes, Union } from "./purchase.actions";

export interface PurchaseState {
  loading: boolean;
  entities: Purchase[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: PurchaseState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function PurchaseReducer(
  state: PurchaseState = INITIAL_STATE,
  action: Union
): PurchaseState {
  switch (action.type) {
    case PurchaseTypes.GetPurchase:
      return {
        ...state,
        loading: true,
      };
    case PurchaseTypes.GetPurchaseSuccess:
      const purchase: Purchase[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: purchase,
      };
    case PurchaseTypes.GetPurchaseError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case PurchaseTypes.CreatePurchase:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case PurchaseTypes.CreatePurchasesuccess:
      const createdPurchase: Purchase = action.payload;
      const entitiesAppended = [...state.entities, createdPurchase];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case PurchaseTypes.UpdatePurchase:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case PurchaseTypes.UpdatePurchasesuccess:
      const updatedPurchase: Purchase = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedPurchase];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case PurchaseTypes.UpdatePurchaseError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case PurchaseTypes.CreatePurchaseError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case PurchaseTypes.DeletePurchase:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case PurchaseTypes.DeletePurchasesuccess:
      const afterDelete = state.entities.filter((purchase) => purchase.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case PurchaseTypes.DeletePurchaseError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getPurchaseState = createFeatureSelector<State, PurchaseState>("otfitems");

export const getPurchaseLoading = createSelector(
  getPurchaseState,
  (state) => state.loading
);

export const getPurchase = createSelector(getPurchaseState, (state) => state.entities);

export const getPurchaseCreating = createSelector(
  getPurchaseState,
  (state) => state.creating
);

export const getPurchaseCreated = createSelector(
  getPurchaseState,
  (state) => state.created
);
export const getPurchaseUpdating = createSelector(
  getPurchaseState,
  (state) => state.updating
);

export const getPurchaseUpdated = createSelector(
  getPurchaseState,
  (state) => state.updated
);
export const getPurchaseDeleting = createSelector(
  getPurchaseState,
  (state) => state.deleting
);

export const getPurchaseDeleted = createSelector(
  getPurchaseState,
  (state) => state.deleted
);

export const getPurchaseError = createSelector(
  getPurchaseState,
  (state) => state.error
);
