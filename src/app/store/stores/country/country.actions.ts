import { Action } from "@ngrx/store";
import { Country } from "src/app/services/country/country.service";

export enum CountryTypes {
  GetCountry = "[Country] GetCountry",
  GetCountrySuccess = "[Country] GetCountrySuccess",
  GetCountryError = "[Country] GetCountryError",
  CreateCountry= "[Country] CreateCountry",
  CreateCountrysuccess = "[Country] CreateCountrysuccess",
  CreateCountryError = "[Country] CreateCountryError",
  UpdateCountry = "[Country] UpdateCountry",
  UpdateCountrysuccess = "[Country] UpdateCountrysuccess",
  UpdateCountryError = "[Country] UpdateCountryError",
  DeleteCountry = "[Country] DeleteCountry",
  DeleteCountryError = "[Country] DeleteCountryError",
  DeleteCountrysuccess = "[Country] DeleteCountrysuccess",
}

export class GetCountry implements Action {
  readonly type = CountryTypes.GetCountry;
}

export class GetCountrySuccess implements Action {
  readonly type = CountryTypes.GetCountrySuccess;
  constructor(readonly payload: Country[]) {}
}
export class GetCountryError implements Action {
  readonly type = CountryTypes.GetCountryError;
  constructor(readonly payload: boolean) {}
}

export class CreateCountry implements Action {
  readonly type = CountryTypes.CreateCountry;
  constructor(readonly payload: Country) {}
}

export class CreateCountrysuccess implements Action {
  readonly type = CountryTypes.CreateCountrysuccess;
  constructor(readonly payload: Country) {}
}

export class CreateCountryError implements Action {
  readonly type = CountryTypes.CreateCountryError;
}
export class UpdateCountry implements Action {
  readonly type = CountryTypes.UpdateCountry;
  constructor(readonly payload: Country) {}
}

export class UpdateCountrysuccess implements Action {
  readonly type = CountryTypes.UpdateCountrysuccess;
  constructor(readonly payload: Country) {}
}

export class UpdateCountryError implements Action {
  readonly type = CountryTypes.UpdateCountryError;
}

export class DeleteCountry implements Action {
  readonly type = CountryTypes.DeleteCountry;
  constructor(readonly payload: string) {}
}

export class DeleteCountrysuccess implements Action {
  readonly type = CountryTypes.DeleteCountrysuccess;
  constructor(readonly payload: string) {}
}

export class DeleteCountryError implements Action {
  readonly type = CountryTypes.DeleteCountryError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetCountry
  | GetCountrySuccess
  |  GetCountryError
  | CreateCountry
  | CreateCountrysuccess
  | CreateCountryError
  | UpdateCountry
  | UpdateCountrysuccess
  | UpdateCountryError
  | DeleteCountry
  | DeleteCountryError
  | DeleteCountrysuccess;
