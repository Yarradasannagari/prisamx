import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './_layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'configurations',
        loadChildren: () =>
          import('./configuration/configurations.module').then((m) => m.ConfigurationModule),
      },
      {
        path: 'builder',
        loadChildren: () =>
          import('./builder/builder.module').then((m) => m.BuilderModule),
      },
      {
        path: 'ecommerce',
        loadChildren: () =>
          import('../modules/e-commerce/e-commerce.module').then(
            (m) => m.ECommerceModule
          ),
      },
      {
        path: 'user-management',
        loadChildren: () =>
          import('../modules/user-management/user-management.module').then(
            (m) => m.UserManagementModule
          ),
      },
      {
        path: 'user-profile',
        loadChildren: () =>
          import('../modules/user-profile/user-profile.module').then(
            (m) => m.UserProfileModule
          ),
      },
      {
        path: 'ngbootstrap',
        loadChildren: () =>
          import('../modules/ngbootstrap/ngbootstrap.module').then(
            (m) => m.NgbootstrapModule
          ),
      },
      {
        path: 'wizards',
        loadChildren: () =>
          import('../modules/wizards/wizards.module').then(
            (m) => m.WizardsModule
          ),
      },
      {
        path: 'material',
        loadChildren: () =>
          import('../modules/material/material.module').then(
            (m) => m.MaterialModule
          ),
      },
      {
        path: 'configuration',
        loadChildren: () =>
          import('../modules/configuration/configuration.module').then(
            (m) => m.ConfigurationModule
          ),
      },
      {
        path: 'aseets',
        loadChildren: () =>
          import('../modules/assets/asset.module').then(
            (m) => m.AssetModule
          ),
      },
      {
        path: 'locations',
        loadChildren: () =>
          import('../modules/locations/location.module').then(
            (m) => m.LocationModule
          ),
      },
      {
        path: 'inspection-points',
        loadChildren: () =>
          import('../modules/inspection-point/inspection-points.module').then(
            (m) => m.InspectionPointModule
          ),
      },
      {
        path: 'purchase',
        loadChildren: () =>
          import('../modules/purchasing/purchase.module').then(
            (m) => m.PurchaseModule
          ),
      },
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: '**',
        redirectTo: 'error/404',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }
