import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Country, CountryService } from './country.service';

@Injectable({ providedIn: 'root' })
export class CountryResolverService implements Resolve<Country> {
  constructor(private countryService: CountryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Country> | Promise<Country> | Country {
    const id = route.paramMap.get('id');
    return this.countryService.get(id);
  }
}
