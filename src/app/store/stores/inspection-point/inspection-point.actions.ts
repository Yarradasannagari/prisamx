import { Action } from "@ngrx/store";
import { InspectionPoint } from "../../../services/inspection-point/inspection-point.service";

export enum InspectionPointTypes {
  GetInspectionPoint = "[InspectionPoint] GetInspectionPoint",
  GetInspectionPoint1 = "[InspectionPoint] GetInspectionPoint1",
  GetInspectionPointSuccess = "[InspectionPoint] GetInspectionPointSuccess",
  GetInspectionPointError = "[InspectionPoint] GetInspectionPointError",
  CreateInspectionPoint = "[InspectionPoint] CreateInspectionPoint",
  CreateInspectionPointsuccess = "[InspectionPoint] CreateInspectionPointsuccess",
  CreateInspectionPointError = "[InspectionPoint] CreateInspectionPointError",
  UpdateInspectionPoint = "[InspectionPoint] UpdateInspectionPoint",
  UpdateInspectionPointsuccess = "[InspectionPoint] UpdateInspectionPointsuccess",
  UpdateInspectionPointError = "[InspectionPoint] UpdateInspectionPointError",
  DeleteInspectionPoint = "[InspectionPoint] DeleteInspectionPoint",
  DeleteInspectionPointError = "[InspectionPoint] DeleteInspectionPointError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectionPointsuccess = "[AclRule] DeleteInspectionPointsuccess",
}

export class GetInspectionPoint implements Action { 
  readonly type = InspectionPointTypes.GetInspectionPoint;
}

export class GetInspectionPointSuccess implements Action {
  readonly type = InspectionPointTypes.GetInspectionPointSuccess;
  constructor(readonly payload: InspectionPoint[]) {}
}
export class GetInspectionPointError implements Action {
  readonly type = InspectionPointTypes.GetInspectionPointError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectionPoint implements Action {
  readonly type = InspectionPointTypes.CreateInspectionPoint;
  constructor(readonly payload: InspectionPoint) {}
}

export class CreateInspectionPointsuccess implements Action {
  readonly type = InspectionPointTypes.CreateInspectionPointsuccess;
  constructor(readonly payload: InspectionPoint) {}
}

export class CreateInspectionPointError implements Action {
  readonly type = InspectionPointTypes.CreateInspectionPointError;
}
export class UpdateInspectionPoint implements Action {
  readonly type = InspectionPointTypes.UpdateInspectionPoint;
  constructor(readonly payload: InspectionPoint) {}
}

export class UpdateInspectionPointsuccess implements Action {
  readonly type = InspectionPointTypes.UpdateInspectionPointsuccess;
  constructor(readonly payload: InspectionPoint) {}
}

export class UpdateInspectionPointError implements Action {
  readonly type = InspectionPointTypes.UpdateInspectionPointError;
}

export class GetInspectionPoint1 implements Action {
  readonly type = InspectionPointTypes.GetInspectionPoint1;
  constructor(readonly payload: string) {}
}

export class DeleteInspectionPoint implements Action {
  readonly type = InspectionPointTypes.DeleteInspectionPoint;
  constructor(readonly payload: string) {}
}

export class DeleteInspectionPointsuccess implements Action {
  readonly type = InspectionPointTypes.DeleteInspectionPointsuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectionPointError implements Action {
  readonly type = InspectionPointTypes.DeleteInspectionPointError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectionPoint
  | GetInspectionPoint1
  | GetInspectionPointSuccess
  | GetInspectionPointError
  | CreateInspectionPoint
  | CreateInspectionPointsuccess
  | CreateInspectionPointError
  | DeleteInspectionPoint
  | DeleteInspectionPointsuccess
  | DeleteInspectionPointError
  | UpdateInspectionPoint
  | UpdateInspectionPointsuccess
  | UpdateInspectionPointError
