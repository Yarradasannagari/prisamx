import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { UnitService } from "../../../services/units/uint.service"; 

import { 
  GetUnit,
  GetUnitSuccess,
  GetUnitError,
  CreateUnit,
  CreateUnitsuccess,
  CreateUnitError,
  DeleteUnit,
  DeleteUnitsuccess,
  DeleteUnitError,
  UnitTypes,
  UpdateUnit,
  UpdateUnitsuccess,
  UpdateUnitError,
} from "./unit.actions";

@Injectable()
export class UnitEffects {
  constructor(
    private actions: Actions,
    private unitService: UnitService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getUnit$: Observable<Action> = this.actions.pipe(
    ofType<GetUnit>(UnitTypes.GetUnit),
    exhaustMap(() =>
      from(this.unitService.list()).pipe(
        map((payload) => {
          return new GetUnitSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Unit", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetUnitError(true));
        })
      )
    )
  );

  @Effect()
  createUnit$: Observable<Action> = this.actions.pipe(
    ofType<CreateUnit>(UnitTypes.CreateUnit),
    concatMap((action) =>
      from(this.unitService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Unit has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateUnitsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Unit", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateUnitError());
        })
      )
    )
  );
  @Effect()
  updateUnit$: Observable<Action> = this.actions.pipe(
    ofType<UpdateUnit>(UnitTypes.UpdateUnit),
    concatMap((action) =>
      from(this.unitService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Unit has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateUnitsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Unit", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateUnitError());
        })
      )
    )
  );
  @Effect()
  deleteUnit$: Observable<Action> = this.actions.pipe(
    ofType<DeleteUnit>(UnitTypes.DeleteUnit),
    concatMap((action) =>
      from(this.unitService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Unit has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteUnitsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Unit:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteUnitError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
