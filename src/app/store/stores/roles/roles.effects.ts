import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { RoleService } from "../../../services/roles/role.service";

import {
  GetRoles,
  GetRolesSuccess,
  GetRolesError,
  CreateRole,
  CreateRolesuccess,
  CreateRoleError,
  DeleteRole,
  DeleteRolesuccess,
  DeleteRoleError,
  RoleTypes,
  UpdateRole,
  UpdateRolesuccess,
  UpdateRoleError,
} from "./roles.actions";

@Injectable()
export class RoleEffects {
  constructor(
    private actions: Actions,
    private roleService: RoleService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getRoles$: Observable<Action> = this.actions.pipe(
    ofType<GetRoles>(RoleTypes.GetRoles),
    exhaustMap(() =>
      from(this.roleService.list()).pipe(
        map((payload) => {
          return new GetRolesSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Roles", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetRolesError(true));
        })
      )
    )
  );

  @Effect()
  createRole$: Observable<Action> = this.actions.pipe(
    ofType<CreateRole>(RoleTypes.CreateRole),
    concatMap((action) =>
      from(this.roleService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Role has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateRolesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Role", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateRoleError());
        })
      )
    )
  );
  @Effect()
  updateRole$: Observable<Action> = this.actions.pipe(
    ofType<UpdateRole>(RoleTypes.UpdateRole),
    concatMap((action) =>
      from(this.roleService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Role has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateRolesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Role", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateRoleError());
        })
      )
    )
  );
  @Effect()
  deleteRole$: Observable<Action> = this.actions.pipe(
    ofType<DeleteRole>(RoleTypes.DeleteRole),
    concatMap((action) =>
      from(this.roleService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "User Role is deleted!.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteRolesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Role:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteRoleError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
