import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInspectionTypeModelComponent } from './delete-inspection-type-model.component';

describe('DeleteInspectionTypeModelComponent', () => {
  let component: DeleteInspectionTypeModelComponent;
  let fixture: ComponentFixture<DeleteInspectionTypeModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteInspectionTypeModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInspectionTypeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
