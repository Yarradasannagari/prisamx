import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PurchasingComponent } from "./purchase.component";

import { PurchaseComponent } from "./purchase/purchase.component";
import { PurchaseFormComponent } from "./purchase-form/purchase-form.component";
import { ViewPurchaseComponent } from "./view-purchase/view-purchase.component";
import { PurchaseResolverService } from "src/app/services/purchase/purchase-resolver.service";
import { PurchaseHeaderResolverService } from "src/app/services/purchase -header/purchase-header-resolver.service";

const routes: Routes = [
  {
    path: "",
    component: PurchasingComponent,
    children: [
      {
        path: "purchase",
        component: PurchaseComponent,
      },
      {
        path: "add-purchase",
        component: PurchaseFormComponent,
      },
      {
        path: ":id/edit-purchase",
        component: PurchaseFormComponent,
        pathMatch: "full",
        resolve: {
          report: PurchaseHeaderResolverService,
        },
      },
      {
        path: ":id/view-purchase",
        component: ViewPurchaseComponent,
        pathMatch: "full",
        resolve: {
          report: PurchaseHeaderResolverService,
        },
      },
     
      { path: "", redirectTo: "purchase", pathMatch: "full" },
      { path: "**", redirectTo: "purchase", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseRoutingModule {}
