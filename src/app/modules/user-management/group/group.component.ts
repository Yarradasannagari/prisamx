import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { Group, GroupService } from "src/app/services/groups/group.service";
import { GetGroups } from "src/app/store/stores/groups/groups.actions";
import {
  getGroupError,
  getGroups,
  getGroupsLoading,
} from "src/app/store/stores/groups/groups.store";
import { State, Store } from "../../../store";
import { DeleteGroupModelComponent } from "../delete-group-model/delete-group-model/delete-group-model.component";

@Component({
  selector: "app-group",
  templateUrl: "./group.component.html",
  styleUrls: ["./group.component.scss"],
})
export class GroupComponent implements OnDestroy, OnInit {
  group$: Observable<Group[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  group: Group[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private groupService: GroupService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getGroupsLoading);
    this.store.dispatch(new GetGroups());
    this.group$ = this.store.select(getGroups);
    this.store.select(getGroupError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }
  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 
  
  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getGroupsLoading);
    this.store.dispatch(new GetGroups());
    this.group$ = this.store.select(getGroups);
    this.store.select(getGroupError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteGroupModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
