import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface InspectionPoint {
  objtyp: string;
    point: string;
    position: string;
    name: string;
    catergory: string;
  // active: boolean;
  assetId: string;
  // atbez: string;
  // atinn: string;
  // cahngedBy: string;
  // category: string;
  // categoryName: string;
  // cdsuf: string;
  // changedDate: string;
  // classId: string;
  // className: string;
  // codct: string;
  //   codgr: string;
  //   counter: string;
  //   createdBy: string; 
  //   createdDate: string;
  //   desir: string;
  //   equnr: string;
  //   group: string;
  //   groupName: string;
  //   groupseq: string;
    id: string;
  //   indct: string;
  //   inspct: string;
  //   inspgr: string;
  //   kltxt: string;
    locationId: string;
  //   locationid: string;
  //   lrange: string;
  //   mpttx: string;
  //   mptyp: string;
  //   mrmax: string;
  //   mrmin: string;
  //   mrngu: string;
  //   msehl: string;
    // projectId: string;
    // psort: string;  
    // pttxt: string;
    // tplnr: string;
    // tread: string;
    // type: string;
    // typeName: string;
    // uom: string;
    // uomt: string;
    // urange: string;
    // usr01: string;
    // usr02: string;  
    // usr03: string;
    // usr04: string;
    // usr05: string;
    // vacod: string;
    // zaehl: string;
}


@Injectable({
  providedIn: 'root',
})
export class InspectionPointService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<InspectionPoint> {
    return this.entitySvc.get(Url.inspectionpoint, id);
  }

  get1(asset_id: any): Observable<InspectionPoint> {
    return this.entitySvc.get(Url.inspectionpoint, asset_id);
  }
  get2(location_id: any): Observable<InspectionPoint> {
    return this.entitySvc.get(Url.inspectionpoint, location_id);
  }
  get3(asset_id: any, location_id: any): Observable<InspectionPoint> {
    return this.entitySvc.get(Url.inspectionpoint, asset_id);
  }


  list(params?: Record<string, any>): Observable<InspectionPoint[]> {
    return this.entitySvc.list(Url.inspectionpoint, params);
  }

  create(aclRule: InspectionPoint): Observable<InspectionPoint> {
    return this.entitySvc.create(Url.inspectionpoint, aclRule); 
  }

  update(aclRule: InspectionPoint): Observable<InspectionPoint> {
    return this.entitySvc.update(Url.inspectionpoint, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectionpoint, id);
  }
  
}
