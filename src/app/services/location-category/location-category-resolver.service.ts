import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Locationcategory, LocationcategoryService } from './location-category.service';

@Injectable({ providedIn: 'root' })
export class LocationcategoryResolverService implements Resolve<Locationcategory> {
  constructor(private locationcategoryService: LocationcategoryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Locationcategory> | Promise<Locationcategory> | Locationcategory {
    const id = route.paramMap.get('id');
    return this.locationcategoryService.get(id);
  }
}
