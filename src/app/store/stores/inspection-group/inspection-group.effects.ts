import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr"; 

import { InspectiongroupService } from "../../../services/inspection-group/inspection-group.service"; 

import {
  GetInspectiongroup,
  GetInspectiongroupSuccess,
  GetInspectiongroupError,
  CreateInspectiongroup,
  CreateInspectiongroupsuccess,
  CreateInspectiongroupError,
  DeleteInspectiongroup,
  DeleteInspectiongroupsuccess,
  DeleteInspectiongroupError,
  InspectiongroupTypes,
  UpdateInspectiongroup,
  UpdateInspectiongroupsuccess,
  UpdateInspectiongroupError,
} from "./inspection-group.actions";

@Injectable()
export class InspectiongroupEffects {
  constructor(
    private actions: Actions,
    private inspectiongroupService: InspectiongroupService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectiongroup$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectiongroup>(InspectiongroupTypes.GetInspectiongroup),
    exhaustMap(() =>
      from(this.inspectiongroupService.list()).pipe(
        map((payload) => {
          return new GetInspectiongroupSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Group", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectiongroupError(true));
        })
      )
    )
  );

  @Effect()
  createInspectiongroup$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectiongroup>(InspectiongroupTypes.CreateInspectiongroup),
    concatMap((action) =>
      from(this.inspectiongroupService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Group has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectiongroupsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Group", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectiongroupError());
        })
      )
    )
  );
  @Effect()
  updateInspectiongroup$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectiongroup>(InspectiongroupTypes.UpdateInspectiongroup),
    concatMap((action) =>
      from(this.inspectiongroupService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Group has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectiongroupsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Group", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectiongroupError());
        })
      )
    )
  );
  @Effect()
  deleteInspectiongroup$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectiongroup>(InspectiongroupTypes.DeleteInspectiongroup),
    concatMap((action) =>
      from(this.inspectiongroupService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Group has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectiongroupsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Group:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectiongroupError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
