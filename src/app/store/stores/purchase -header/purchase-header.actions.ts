import { Action } from "@ngrx/store";
import { PurchaseHeader } from "../../../services/purchase -header/purchase-header.service";

export enum PurchaseHeaderTypes {
  GetPurchaseHeader = "[PurchaseHeader] GetPurchaseHeader",
  GetPurchaseHeaderSuccess = "[PurchaseHeader] GetPurchaseHeaderSuccess",
  GetPurchaseHeaderError = "[PurchaseHeader] GetPurchaseHeaderError",
  CreatePurchaseHeader = "[PurchaseHeader] CreatePurchaseHeader",
  CreatePurchaseHeadersuccess = "[PurchaseHeader] CreatePurchaseHeadersuccess",
  CreatePurchaseHeaderError = "[PurchaseHeader] CreatePurchaseHeaderError",
  UplodPurchaseHeader = "[PurchaseHeader] UplodPurchaseHeader",
  UplodPurchaseHeadersuccess = "[PurchaseHeader] UplodPurchaseHeadersuccess",
  UplodPurchaseHeaderError = "[PurchaseHeader] UplodPurchaseHeaderError",
  UpdatePurchaseHeader = "[PurchaseHeader] UpdatePurchaseHeader",
  UpdatePurchaseHeadersuccess = "[PurchaseHeader] UpdatePurchaseHeadersuccess",
  UpdatePurchaseHeaderError = "[PurchaseHeader] UpdatePurchaseHeaderError",
  DeletePurchaseHeader = "[PurchaseHeader] DeletePurchaseHeader",
  DeletePurchaseHeaderError = "[PurchaseHeader] DeletePurchaseHeaderError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeletePurchaseHeadersuccess = "[AclRule] DeletePurchaseHeadersuccess",
}

export class GetPurchaseHeader implements Action { 
  readonly type = PurchaseHeaderTypes.GetPurchaseHeader;
}

export class GetPurchaseHeaderSuccess implements Action {
  readonly type = PurchaseHeaderTypes.GetPurchaseHeaderSuccess;
  constructor(readonly payload: PurchaseHeader[]) {}
}
export class GetPurchaseHeaderError implements Action {
  readonly type = PurchaseHeaderTypes.GetPurchaseHeaderError;
  constructor(readonly payload: boolean) {}
}

export class CreatePurchaseHeader implements Action {
  readonly type = PurchaseHeaderTypes.CreatePurchaseHeader;
  constructor(readonly payload: PurchaseHeader) {}
}

export class CreatePurchaseHeadersuccess implements Action {
  readonly type = PurchaseHeaderTypes.CreatePurchaseHeadersuccess;
  constructor(readonly payload: PurchaseHeader) {}
}

export class CreatePurchaseHeaderError implements Action {
  readonly type = PurchaseHeaderTypes.CreatePurchaseHeaderError;
}

export class UplodPurchaseHeader implements Action {
  readonly type = PurchaseHeaderTypes.UplodPurchaseHeader;
  constructor(readonly payload: PurchaseHeader[]) {}
}

export class UplodPurchaseHeadersuccess implements Action {
  readonly type = PurchaseHeaderTypes.UplodPurchaseHeadersuccess;
  constructor(readonly payload: PurchaseHeader) {}
}

export class UplodPurchaseHeaderError implements Action {
  readonly type = PurchaseHeaderTypes.UplodPurchaseHeaderError;
}


export class UpdatePurchaseHeader implements Action {
  readonly type = PurchaseHeaderTypes.UpdatePurchaseHeader;
  constructor(readonly payload: PurchaseHeader) {}
}

export class UpdatePurchaseHeadersuccess implements Action {
  readonly type = PurchaseHeaderTypes.UpdatePurchaseHeadersuccess;
  constructor(readonly payload: PurchaseHeader) {}
}

export class UpdatePurchaseHeaderError implements Action {
  readonly type = PurchaseHeaderTypes.UpdatePurchaseHeaderError;
}

export class DeletePurchaseHeader implements Action {
  readonly type = PurchaseHeaderTypes.DeletePurchaseHeader;
  constructor(readonly payload: string) {}
}

export class DeletePurchaseHeadersuccess implements Action {
  readonly type = PurchaseHeaderTypes.DeletePurchaseHeadersuccess;
  constructor(readonly payload: string) {}
}

export class DeletePurchaseHeaderError implements Action {
  readonly type = PurchaseHeaderTypes.DeletePurchaseHeaderError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetPurchaseHeader
  | GetPurchaseHeaderSuccess
  | GetPurchaseHeaderError
  | CreatePurchaseHeader
  | CreatePurchaseHeadersuccess
  | CreatePurchaseHeaderError
  | UplodPurchaseHeader
  | UplodPurchaseHeadersuccess
  | UplodPurchaseHeaderError
  | DeletePurchaseHeader
  | DeletePurchaseHeadersuccess
  | DeletePurchaseHeaderError
  | UpdatePurchaseHeader
  | UpdatePurchaseHeadersuccess
  | UpdatePurchaseHeaderError
