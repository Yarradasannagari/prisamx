import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Role } from "../../../../services/roles/role.service";
import { GetRoles } from "src/app/store/stores/roles/roles.actions";
import { getRoles } from "src/app/store/stores/roles/roles.store";
import { State, Store } from "../../../../store";
import { User } from "../../../../services/users/user.service";
import {
  CreateUser,
  GetUsers,
  UpdateUser,
} from "src/app/store/stores/users/users.actions";
import {
  getUserCreated,
  getUsers,
  getUserUpdated,
} from "src/app/store/stores/users/users.store";
import { filter, map, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { GetGroups } from "src/app/store/stores/groups/groups.actions";
import { getGroups } from "src/app/store/stores/groups/groups.store";
import { Group } from "src/app/services/groups/group.service";
@Component({
  selector: "app-user-form",
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.scss"],
})
export class UserFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    activated: true,
    authorities: [],
    email: "",
    firstName: "",
    lastName: "",
    login: "",
    personId: "",
    telmb: "",
    usgrp: "",
  };
  roles: Role[];
  users: User[];
  toppings1 = new FormControl();

  toppings2 = new FormControl();
  toppingList: string[] = [
    "50001",
    "50002",
    "50003",
    "50004",
    "50005",
  ];
  groups: Group[];
  user: User;
  private readonly destroy = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.store.dispatch(new GetRoles());
    this.store.select(getRoles).subscribe((roles) => {
      this.roles = roles;
    });
    this.store.dispatch(new GetGroups());
    this.store.select(getGroups).subscribe((groups) => {
      this.groups = groups;
    });
    this.store.dispatch(new GetUsers());
    this.store.select(getUsers).subscribe((users) => {
      this.users = users;
      console.log(users);
      this.users = users.filter(
        (item) => item.id == this.route.snapshot.data["report"].id
      );
      this.formData.authorities = this.users[0].authorities;
    });
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.activated = this.route.snapshot.data["report"].activated;
        this.formData.login = this.route.snapshot.data["report"].login;
        this.formData.firstName = this.route.snapshot.data["report"].firstName;
        this.formData.email = this.route.snapshot.data["report"].email;
        this.formData.usgrp = this.route.snapshot.data["report"].usgrp;
        this.formData.telmb = this.route.snapshot.data["report"].telmb;
        this.formData.personId = this.route.snapshot.data["report"].personId;
        this.formData.lastName = this.route.snapshot.data["report"].lastName;

        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    this.formGroup = this.fb.group({
      login: [
        this.formData.login,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      firstName: [
        this.formData.firstName,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(255),
        ]),
      ],
      email: [
        this.formData.email,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ]),
      ],
      lastName: [
        this.formData.lastName,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      personId: [
        this.formData.personId,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      telmb: [
        this.formData.telmb,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(12),
        ]),
      ],
      usgrp: [this.formData.usgrp, Validators.compose([Validators.required])],
      authorities: [
        this.formData.authorities,
        Validators.compose([Validators.required]),
      ],
      activated: [this.formData.activated],
    });
  }
  cancel(): void {
    this.router.navigate(["user-management", "users"]);
  }
  submit() {
    const user: User = {
      id: this.formData.id,
      authorities: this.formData.authorities,
      login: this.formData.login,
      firstName: this.formData.firstName,
      email: this.formData.email,
      activated: this.formData.activated,
      usgrp: this.formData.usgrp,
      telmb: this.formData.telmb,
      personId: this.formData.personId,
      lastName: this.formData.lastName,
    };
    if (this.routeId) {
      this.updateUser(user);
    } else {
      this.createRole(user);
    }
  }

  private createRole(user: User): void {
    this.store.dispatch(new CreateUser(user));
    this.store
      .select(getUserCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateUser(user: User): void {
    this.store.dispatch(new UpdateUser(user));
    this.store
      .select(getUserUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }

  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
}
