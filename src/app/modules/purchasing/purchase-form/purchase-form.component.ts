import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Purchase } from "src/app/services/purchase/purchase.service";
import { PurchaseHeader } from "src/app/services/purchase -header/purchase-header.service";
import {
  CreatePurchase,
  UpdatePurchase,
} from "src/app/store/stores/purchase/purchase.actions";
import {
  CreatePurchaseHeader,
  UpdatePurchaseHeader,
} from "src/app/store/stores/purchase -header/purchase-header.actions";
import {
  getPurchaseCreated,
  getPurchaseUpdated,
} from "src/app/store/stores/purchase/purchase.store";
import {
  getPurchaseHeaderCreated,
  getPurchaseHeaderUpdated,
} from "src/app/store/stores/purchase -header/purchase-header.store";

import { State, Store } from "../../../store";
@Component({
  selector: "app-purchase-form",
  templateUrl: "./purchase-form.component.html",
  styleUrls: ["./purchase-form.component.scss"],
})
export class PurchaseFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  location: Location[];

  formData = {
    id: null,
    name: "",
    pdDate: "",
    pdnum: "",
    pdstatus: "",
    trnid: "",
    vendor: "",
    email: "",
    mobile: "",
    country: "",
    city: "",
    tstatus: "",
    pdowner: "",
    prnum: "",
    rindicator: "",
    pdorg: "",
    orgid: "",
    item: "",
    material: "",
    matName: "",
    quantity: "",
    qunit: "",
    punit: "",
    gvalue: "",

    active: true,
  };
  purchase: Purchase;
  purchaseheader: PurchaseHeader;

  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.rindicator =
          this.route.snapshot.data["report"].rindicator;
        this.formData.prnum = this.route.snapshot.data["report"].prnum;
        this.formData.pdowner = this.route.snapshot.data["report"].pdowner;
        this.formData.tstatus = this.route.snapshot.data["report"].tstatus;
        this.formData.city = this.route.snapshot.data["report"].city;
        this.formData.country = this.route.snapshot.data["report"].country;
        this.formData.mobile = this.route.snapshot.data["report"].mobile;
        this.formData.email = this.route.snapshot.data["report"].email;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.pdDate = this.route.snapshot.data["report"].pdDate;
        this.formData.pdnum = this.route.snapshot.data["report"].pdnum;
        this.formData.pdstatus = this.route.snapshot.data["report"].pdstatus;
        this.formData.trnid = this.route.snapshot.data["report"].trnid;
        this.formData.vendor = this.route.snapshot.data["report"].vendor;
        this.formData.pdorg = this.route.snapshot.data["report"].pdorg;
        this.formData.orgid = this.route.snapshot.data["report"].orgid;

        this.formData.item = this.route.snapshot.data["report"].item;
        this.formData.material = this.route.snapshot.data["report"].material;
        this.formData.matName = this.route.snapshot.data["report"].matName;
        this.formData.quantity = this.route.snapshot.data["report"].quantity;
        this.formData.qunit = this.route.snapshot.data["report"].qunit;
        this.formData.punit = this.route.snapshot.data["report"].punit;
        this.formData.gvalue = this.route.snapshot.data["report"].gvalue;

        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      pdDate: [
        this.formData.pdDate,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      pdnum: [
        this.formData.pdnum,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(12),
        ]),
      ],
      pdstatus: [
        this.formData.pdstatus,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      trnid: [
        this.formData.trnid,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      vendor: [
        this.formData.vendor,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      email: [
        this.formData.email,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      mobile: [
        this.formData.mobile,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(255),
        ]),
      ],
      country: [
        this.formData.country,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]),
      ],
      city: [
        this.formData.city,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]),
      ],
      tstatus: [
        this.formData.tstatus,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      pdowner: [
        this.formData.pdowner,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      prnum: [
        this.formData.prnum,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      rindicator: [
        this.formData.rindicator,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],

      item: [
        this.formData.item,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      material: [
        this.formData.material,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      matName: [
        this.formData.matName,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      quantity: [
        this.formData.quantity,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      qunit: [
        this.formData.qunit,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      punit: [
        this.formData.punit,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      gvalue: [
        this.formData.gvalue,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      pdorg: [
        this.formData.pdorg,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],
      orgid: [
        this.formData.orgid,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
        ]),
      ],

      // active: [this.formData.active],
    });
  }
  submit() {
    const purchaseheader: PurchaseHeader = {
      id: this.formData.id,
      name: this.formData.name,
      pdDate: this.formData.pdDate,
      pdnum: this.formData.pdnum,
      pdstatus: this.formData.pdstatus,
      vendor: this.formData.vendor,
      email: this.formData.email,
      mobile: this.formData.mobile,
      country: this.formData.country,
      city: this.formData.city,
      prnum: this.formData.prnum,
      orgid: this.formData.orgid,
      trnid: this.formData.pdnum,
      fyear:this.formData.quantity,
      fmonth:this.formData.gvalue
    };
    const purchase: Purchase = {
      id: this.formData.id,
      item: this.formData.item,
      material: this.formData.material,
      matName: this.formData.matName,
      quantity: this.formData.quantity,
      qunit: this.formData.qunit,
      punit: this.formData.punit,
      gvalue: this.formData.gvalue,
    };

    if (this.formData.id) {
      this.updatePurchase(purchase);
      this.updatePurchaseHeader(purchaseheader);
    } else {
      this.createPurchase(purchase);
      this.createPurchaseHeader(purchaseheader);
    }
  }
  private createPurchaseHeader(Purchaseheader: PurchaseHeader): void {
    this.store.dispatch(new CreatePurchaseHeader(Purchaseheader));
    this.store
      .select(getPurchaseHeaderCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          this.cancel();
        }
      });
  }
  private updatePurchaseHeader(Purchaseheader: PurchaseHeader): void {
    this.store.dispatch(new UpdatePurchaseHeader(Purchaseheader));
    this.store
      .select(getPurchaseHeaderUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["purchase", "purchase"]);
  }

  submit1() {}
  private createPurchase(purchase: Purchase): void {
    this.store.dispatch(new CreatePurchase(purchase));
    this.store
      .select(getPurchaseCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {});
  }
  private updatePurchase(purchase: Purchase): void {
    this.store.dispatch(new UpdatePurchase(purchase));
    this.store
      .select(getPurchaseUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {});
  }
}
