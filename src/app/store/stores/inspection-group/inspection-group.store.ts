import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Inspectiongroup } from "src/app/services/inspection-group/inspection-group.service";

import { State } from "../..";

import { InspectiongroupTypes, Union } from "./inspection-group.actions";

export interface InspectiongroupState {
  loading: boolean;
  entities: Inspectiongroup[];
  creating: boolean;
  created: boolean;
  updating: boolean;
  updated: boolean;
  deleting: boolean;
  deleted: boolean;
  error: boolean;
}

export const INITIAL_STATE: InspectiongroupState = {
  loading: false,
  entities: [],
  creating: false,
  created: false,
  deleting: false,
  deleted: false,
  error: false,
  updating: false,
  updated: false,
};

export function InspectiongroupReducer(
  state: InspectiongroupState = INITIAL_STATE,
  action: Union
): InspectiongroupState {
  switch (action.type) {
    case InspectiongroupTypes.GetInspectiongroup:
      return {
        ...state,
        loading: true,
      };
    case InspectiongroupTypes.GetInspectiongroupSuccess:
      const inspectiongroup: Inspectiongroup[] = action.payload;
      return {
        ...state,
        loading: false,
        entities: inspectiongroup,
      };
    case InspectiongroupTypes.GetInspectiongroupError:
      const error1: boolean = action.payload;
      return {
        ...state,
        loading: false,
        error: error1,
      };
    case InspectiongroupTypes.CreateInspectiongroup:
      return {
        ...state,
        creating: true,
        created: false,
      };
    case InspectiongroupTypes.CreateInspectiongroupsuccess:
      const createdInspectiongroup: Inspectiongroup = action.payload;
      const entitiesAppended = [...state.entities, createdInspectiongroup];
      return {
        ...state,
        creating: false,
        entities: entitiesAppended,
        created: true,
      };
      case InspectiongroupTypes.UpdateInspectiongroup:
      return {
        ...state,
        updating: true,
        updated: false,
      };
    case InspectiongroupTypes.UpdateInspectiongroupsuccess:
      const updatedInspectiongroup: Inspectiongroup = action.payload;
      const updatedentitiesAppended = [...state.entities, updatedInspectiongroup];
      return {
        ...state,
        updating: false,
        entities: updatedentitiesAppended,
        updated: true,
      };
      case InspectiongroupTypes.UpdateInspectiongroupError:
      return {
        ...state,
        updated: false,
        updating: false,
      };
    case InspectiongroupTypes.CreateInspectiongroupError:
      return {
        ...state,
        creating: false,
        created: false,
      };

    case InspectiongroupTypes.DeleteInspectiongroup:
      return {
        ...state,
        deleting: false,
        deleted: true,
      };

    case InspectiongroupTypes.DeleteInspectiongroupsuccess:
      const afterDelete = state.entities.filter((inspectiongroup) => inspectiongroup.id !== action.payload);
      return {
        ...state,

        deleting: false,
        deleted: true,
        entities:afterDelete
      };

    case InspectiongroupTypes.DeleteInspectiongroupError:
      return {
        ...state,
        deleting: false,
        deleted: false,
      };

    default:
      return state;
  }
}

export const getInspectiongroupState = createFeatureSelector<State, InspectiongroupState>("inspectiongroup");

export const getInspectiongroupLoading = createSelector(
  getInspectiongroupState,
  (state) => state.loading
);

export const getInspectiongroup = createSelector(getInspectiongroupState, (state) => state.entities);

export const getInspectiongroupCreating = createSelector(
  getInspectiongroupState,
  (state) => state.creating
);

export const getInspectiongroupCreated = createSelector(
  getInspectiongroupState,
  (state) => state.created
);
export const getInspectiongroupUpdating = createSelector(
  getInspectiongroupState,
  (state) => state.updating
);

export const getInspectiongroupUpdated = createSelector(
  getInspectiongroupState,
  (state) => state.updated
);
export const getInspectiongroupDeleting = createSelector(
  getInspectiongroupState,
  (state) => state.deleting
);

export const getInspectiongroupDeleted = createSelector(
  getInspectiongroupState,
  (state) => state.deleted
);

export const getInspectiongroupError = createSelector(
  getInspectiongroupState,
  (state) => state.error
);
