import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Inspectioncategory, InspectioncategoryService } from './inspection-category.service';

@Injectable({ providedIn: 'root' })
export class InspectioncategoryResolverService implements Resolve<Inspectioncategory> {
  constructor(private inspectioncategoryService: InspectioncategoryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Inspectioncategory> | Promise<Inspectioncategory> | Inspectioncategory {
    const id = route.paramMap.get('id');
    return this.inspectioncategoryService.get(id);
  }
}
