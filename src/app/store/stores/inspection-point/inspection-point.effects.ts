import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { InspectionPointService } from "../../../services/inspection-point/inspection-point.service"; 
import {
  GetInspectionPoint,
  GetInspectionPointSuccess,
  GetInspectionPointError,
  CreateInspectionPoint,
  CreateInspectionPointsuccess,
  CreateInspectionPointError,
  DeleteInspectionPoint,
  DeleteInspectionPointsuccess,
  DeleteInspectionPointError,
  InspectionPointTypes,
  UpdateInspectionPoint,
  UpdateInspectionPointsuccess,
  UpdateInspectionPointError,
} from "./inspection-point.actions";

@Injectable()
export class InspectionPointEffects {
  constructor(
    private actions: Actions,
    private inspectionPointService: InspectionPointService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectionPoint$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectionPoint>(InspectionPointTypes.GetInspectionPoint),
    exhaustMap(() =>
      from(this.inspectionPointService.list()).pipe(
        map((payload) => {
          return new GetInspectionPointSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Point", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectionPointError(true));
        })
      )
    )
  );

  @Effect()
  createInspectionPoint$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectionPoint>(InspectionPointTypes.CreateInspectionPoint),
    concatMap((action) =>
      from(this.inspectionPointService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Point has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectionPointsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Point", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectionPointError());
        })
      )
    )
  );
  @Effect()
  updateInspectionPoint$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectionPoint>(InspectionPointTypes.UpdateInspectionPoint),
    concatMap((action) =>
      from(this.inspectionPointService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Point has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectionPointsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Point", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectionPointError());
        })
      )
    )
  );
  @Effect()
  deleteInspectionPoint$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectionPoint>(InspectionPointTypes.DeleteInspectionPoint),
    concatMap((action) =>
      from(this.inspectionPointService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Point has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectionPointsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Point:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectionPointError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
