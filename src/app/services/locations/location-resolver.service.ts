import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Location, LocationService } from './location.service';

@Injectable({ providedIn: 'root' })
export class LocationResolverService implements Resolve<Location> {
  constructor(private locationService: LocationService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Location> | Promise<Location> | Location {
    const id = route.paramMap.get('id');
    return this.locationService.get(id);
  }
}
