import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Inspectioncode {
  id:string;
  active: boolean;
  group: string;
  nameSpace: string;
  vcode: string;
}


@Injectable({
  providedIn: 'root',
})
export class InspectioncodeService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Inspectioncode> {
    return this.entitySvc.get(Url.inspectioncode, id);
  }

  list(params?: Record<string, any>): Observable<Inspectioncode[]> {
    return this.entitySvc.list(Url.inspectioncode, params);
  }

  create(aclRule: Inspectioncode): Observable<Inspectioncode> {
    return this.entitySvc.create(Url.inspectioncode, aclRule);
  }

  update(aclRule: Inspectioncode): Observable<Inspectioncode> {
    return this.entitySvc.update(Url.inspectioncode, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.inspectioncode, id);
  }
  
}
