import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { LocationtypeService } from "../../../services/location-type/location-type.service"; 

import {
  GetLocationtype,
  GetLocationtypeSuccess,
  GetLocationtypeError,
  CreateLocationtype,
  CreateLocationtypesuccess,
  CreateLocationtypeError,
  DeleteLocationtype,
  DeleteLocationtypesuccess,
  DeleteLocationtypeError,
  LocationtypeTypes,
  UpdateLocationtype,
  UpdateLocationtypesuccess,
  UpdateLocationtypeError,
} from "./location-type.actions";

@Injectable()
export class LocationtypeEffects {
  constructor(
    private actions: Actions,
    private locationtypeService: LocationtypeService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getLocationtype$: Observable<Action> = this.actions.pipe(
    ofType<GetLocationtype>(LocationtypeTypes.GetLocationtype),
    exhaustMap(() =>
      from(this.locationtypeService.list()).pipe(
        map((payload) => {
          return new GetLocationtypeSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Location type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetLocationtypeError(true));
        })
      )
    )
  );

  @Effect()
  createLocationtype$: Observable<Action> = this.actions.pipe(
    ofType<CreateLocationtype>(LocationtypeTypes.CreateLocationtype),
    concatMap((action) =>
      from(this.locationtypeService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location type has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateLocationtypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Location type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateLocationtypeError());
        })
      )
    )
  );
  @Effect()
  updateLocationtype$: Observable<Action> = this.actions.pipe(
    ofType<UpdateLocationtype>(LocationtypeTypes.UpdateLocationtype),
    concatMap((action) =>
      from(this.locationtypeService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location type has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateLocationtypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Location type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateLocationtypeError());
        })
      )
    )
  );
  @Effect()
  deleteLocationtype$: Observable<Action> = this.actions.pipe(
    ofType<DeleteLocationtype>(LocationtypeTypes.DeleteLocationtype),
    concatMap((action) =>
      from(this.locationtypeService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Location type has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteLocationtypesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Location type:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteLocationtypeError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
