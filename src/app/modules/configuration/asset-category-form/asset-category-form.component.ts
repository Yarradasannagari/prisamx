import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Assetcategory } from "src/app/services/asset-category/asset-category.service";
import { CreateAssetcategory, UpdateAssetcategory } from "src/app/store/stores/asset-category/asset-category.actions";
import { getAssetcategoryCreated, getAssetcategoryUpdated } from "src/app/store/stores/asset-category/asset-category.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-asset-category-form",
  templateUrl: "./asset-category-form.component.html",
  styleUrls: ["./asset-category-form.component.scss"],
})
export class AssetCategoryFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    category: "",
    name: "",
    active: true,
  }; 
  assetcategory: Assetcategory;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.category = this.route.snapshot.data["report"].category;
        this.formData.name =  this.route.snapshot.data["report"].name;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      category: [
        this.formData.category,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const assetcategory: Assetcategory = {
      id: this.formData.id,
      category: this.formData.category,
      name: this.formData.name,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateAssetcategory(assetcategory);
    } else {
      this.createAssetcategory(assetcategory);
    }
  }
  private createAssetcategory(assetcategory: Assetcategory): void {
    this.store.dispatch(new CreateAssetcategory(assetcategory));
    this.store
      .select(getAssetcategoryCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateAssetcategory(assetcategory: Assetcategory): void {
    this.store.dispatch(new UpdateAssetcategory(assetcategory));
    this.store
      .select(getAssetcategoryUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "asset-category"]);
  }
}
