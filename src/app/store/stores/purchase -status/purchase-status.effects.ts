import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { PurchaseStatusService } from "../../../services/purchase -status/purchase-status.service"; 

import {
  GetPurchaseStatus,
  GetPurchaseStatusSuccess,
  GetPurchaseStatusError,
  CreatePurchaseStatus,
  CreatePurchaseStatussuccess,
  CreatePurchaseStatusError,
  DeletePurchaseStatus,
  DeletePurchaseStatussuccess,
  DeletePurchaseStatusError,
  PurchaseStatusTypes,
  UpdatePurchaseStatus,
  UpdatePurchaseStatussuccess,
  UpdatePurchaseStatusError,
} from "./purchase-status.actions";

@Injectable()
export class PurchaseStatusEffects {
  constructor(
    private actions: Actions,
    private purchaseStatusService: PurchaseStatusService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getPurchaseStatus$: Observable<Action> = this.actions.pipe(
    ofType<GetPurchaseStatus>(PurchaseStatusTypes.GetPurchaseStatus),
    exhaustMap(() =>
      from(this.purchaseStatusService.list()).pipe(
        map((payload) => {
          return new GetPurchaseStatusSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Purchase Status", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetPurchaseStatusError(true));
        })
      )
    )
  );

  @Effect()
  createPurchaseStatus$: Observable<Action> = this.actions.pipe(
    ofType<CreatePurchaseStatus>(PurchaseStatusTypes.CreatePurchaseStatus),
    concatMap((action) =>
      from(this.purchaseStatusService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase Status has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreatePurchaseStatussuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Purchase Status", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreatePurchaseStatusError());
        })
      )
    )
  );
  @Effect()
  updatePurchaseStatus$: Observable<Action> = this.actions.pipe(
    ofType<UpdatePurchaseStatus>(PurchaseStatusTypes.UpdatePurchaseStatus),
    concatMap((action) =>
      from(this.purchaseStatusService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Purchase Status has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdatePurchaseStatussuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Purchase Status", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdatePurchaseStatusError());
        })
      )
    )
  );
  @Effect()
  deletePurchaseStatus$: Observable<Action> = this.actions.pipe(
    ofType<DeletePurchaseStatus>(PurchaseStatusTypes.DeletePurchaseStatus),
    concatMap((action) =>
      from(this.purchaseStatusService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Purchase Status has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeletePurchaseStatussuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Purchase Status:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeletePurchaseStatusError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
