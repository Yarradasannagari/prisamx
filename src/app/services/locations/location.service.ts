import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StringLiteralLike } from 'typescript';
import { EntityService } from '../entity.service';

import { Url } from '../urls';

export interface Location {
  // active: boolean;

    // abckz: string;
    addr1: string;
    addr2: string;
    addr3: string;
    // aedat: string;
    // arbpl: string;
    // arbplname: string;
    // baujj: string;
    // baumm: string;
    // beber: string;
    // bukrs: string;
    // category: string;
    // changedBy: string;
    // changedDate: string;
    city: string;
    // classId: string;
    // classname: string;
    country: string;
    // createdBy: string;
    // createdDate: string;
    eartx: string;
    // emailid: string;
    // endPiont: string;
    // endPoint: string;
    // eqart: string;
    // eqfnr: string;
    // fltyp: string;
    // gwldt: string;
    // gwlen: string;
    // haschild: string;
    // hasparent: string;
    // herst: string;
    // housenum: string;
    id: string;
    // ingrp: string;
    // ingrpname: string;
    // istat: string;
    // iwerk: string;
    // iwerkname: string;
    // kostl: string;
    landMark: string;
    latitude: string;
    // lclass: string;
    // length: string;
    // lengthUom: string;
    // lengthuom: string;
    // linear_length: string;
    // linear_unit: string;
    locationId: string;
    longitude: string;
    // mapar: string;
    // mobno: string;
    // msgrp: string;
    // name: string;
    // name1: string;
    // name2: string;
    // orgId: string;
    // parent: string;
    // parnr: string;
    // parnrname: string;
    // pgroup: string;
    // plant: string;
    pltxt: string;
    // pplant: string;
    // rbnr: string;
    // rbnrx: string;
    // region: string;
    regoin: string;
    // root: string;
    // serge: string;
    startPoint: string;
    status: string;
    // statusName: string;
    // stort: string;
    // street: string;
    // strno: string;
    swerk: string;
    // swerkname: string;
    // telno: string;
    // tplma: string;
    // tplmatxt: string;
    // tplnr: string;
    // typbz: string;
    // type: string;
    typtx: string;
    // userId: string;
    // usr01: string;
    // usr02: string;
    // usr03: string;
    // usr04: string;
    // usr05: string;
    // wkctr: string;
    zipcode: string;
}


@Injectable({
  providedIn: 'root',
})
export class LocationService {
  constructor(private readonly http: HttpClient, private entitySvc: EntityService<any>) {}
  get(id: any): Observable<Location> {
    return this.entitySvc.get(Url.locations, id);
  }

  list(params?: Record<string, any>): Observable<Location[]> {
    return this.entitySvc.list(Url.locations, params);
  }

  create(aclRule: Location): Observable<Location> {
    return this.entitySvc.create(Url.locations, aclRule); 
  }

  update(aclRule: Location): Observable<Location> {
    return this.entitySvc.update(Url.locations, aclRule);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.locations, id);
  }
  
}
