import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationCategoryFormComponent } from './location-category-form.component';

describe('LocationCategoryFormComponent', () => {
  let component: LocationCategoryFormComponent;
  let fixture: ComponentFixture<LocationCategoryFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationCategoryFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
