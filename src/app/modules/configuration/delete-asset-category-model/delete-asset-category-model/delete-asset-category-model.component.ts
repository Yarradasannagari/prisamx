import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { State, Store } from "../../../../store";
import { Subject, Subscription } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { DeleteAssetcategory} from "src/app/store/stores/asset-category/asset-category.actions";
import { getAssetcategoryDeleted } from "src/app/store/stores/asset-category/asset-category.store";
@Component({
  selector: "app-delete-asset-category-model",
  templateUrl: "./delete-asset-category-model.component.html",
  styleUrls: ["./delete-asset-category-model.component.scss"],
})
export class DeleteAssetCategoryModelComponent implements OnInit {
  @Input() id: string;
  isLoading = false;

  private subscriptions: Subscription[] = [];
  private readonly destroy = new Subject<void>();

  constructor(public modal: NgbActiveModal, private store: Store<State>) {}

  ngOnInit(): void {}
  deleteAssetcategory() {
    this.isLoading = true;
    this.store.dispatch(new DeleteAssetcategory(this.id));
    this.store
      .select(getAssetcategoryDeleted)
      .pipe(
        takeUntil(this.destroy),
        filter((deleted) => deleted)
      )
      .subscribe(() => {
        this.isLoading = false;
        this.modal.close();
      });
  }
}
