import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { InspectionPoint, InspectionPointService } from "src/app/services/inspection-point/inspection-point.service";
import { GetInspectionPoint } from "src/app/store/stores/inspection-point/inspection-point.actions";
import {
  getInspectionPointError,
  getInspectionPoint,
  getInspectionPointLoading,
} from "src/app/store/stores/inspection-point/inspection-point.store";
import { State, Store } from "../../../store";
import { DeleteInspectionPointModelComponent } from "../delete-inspection-point-model/delete-inspection-point-model/delete-inspection-point-model.component";

@Component({
  selector: "app-inspection-point",
  templateUrl: "./inspection-point.component.html",
  styleUrls: ["./inspection-point.component.scss"],
})
export class InspectionPointComponent implements OnDestroy, OnInit {
  inspectionPoint$: Observable<InspectionPoint[]>;
  isLoading$: Observable<boolean>;
  isError: boolean;
  searchText:any;

  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  inspectionPoint: InspectionPoint[];
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private inspectionPointService: InspectionPointService,
    private modalService: NgbModal
  ) {
    this.isLoading$ = this.store.select(getInspectionPointLoading);
    this.store.dispatch(new GetInspectionPoint());
    this.inspectionPoint$ = this.store.select(getInspectionPoint);
    this.store.select(getInspectionPointError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }

  ngOnInit(): void {
    // this.roleService.list().subscribe(
    //   data =>{
    //     this.roles = data;
    //   }
    // )
  }

  onTableDataChange(event){
    this.page = event;
    this.reTry();
  } 

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
  reTry() {
    this.isLoading$ = this.store.select(getInspectionPointLoading);
    this.store.dispatch(new GetInspectionPoint());
    this.inspectionPoint$ = this.store.select(getInspectionPoint);
    this.store.select(getInspectionPointError).subscribe((flag) => {
      if (flag) {
        this.isError = flag;
      }
    });
  }
  delete(id: any) {
    const modalRef = this.modalService.open(DeleteInspectionPointModelComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => {
        
      },
      () => {}
    );
  }

  key: string = 'id';
  reverse: boolean = false;
  sort(key) {
        this.key = key;
        this.reverse = !this.reverse;
  }
}
