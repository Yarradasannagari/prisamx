import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePurchaseHeaderModelComponent } from './delete-purchase-header-model.component';

describe('DeletePurchaseHeaderModelComponent', () => {
  let component: DeletePurchaseHeaderModelComponent;
  let fixture: ComponentFixture<DeletePurchaseHeaderModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletePurchaseHeaderModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePurchaseHeaderModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
