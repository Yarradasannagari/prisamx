import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "activeRoles",
})
export class ActiveRolesPipe implements PipeTransform {
  array = [];
  transform(items: any[]): any[] {
    if (!items) {
      return [];
    }
    this.array = [];
    console.log("roles"+JSON.stringify(items))
    items.filter((item) => {
      console.log(item.active)
      if (item.active) {
        this.array.push(item);
      }
    });
    return this.array;
  }
}
