import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { PartnerService } from "../../../services/partner/partner.service"; 

import {
  GetPartner,
  GetPartnerSuccess,
  GetPartnerError,
  CreatePartner,
  CreatePartnersuccess,
  CreatePartnerError,
  DeletePartner,
  DeletePartnersuccess,
  DeletePartnerError,
  PartnerTypes,
  UpdatePartner,
  UpdatePartnersuccess,
  UpdatePartnerError,
} from "./partner.actions";

@Injectable()
export class PartnerEffects {
  constructor(
    private actions: Actions,
    private partnerService: PartnerService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getPartner$: Observable<Action> = this.actions.pipe(
    ofType<GetPartner>(PartnerTypes.GetPartner),
    exhaustMap(() =>
      from(this.partnerService.list()).pipe(
        map((payload) => {
          return new GetPartnerSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Partner", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetPartnerError(true));
        })
      )
    )
  );

  @Effect()
  createPartner$: Observable<Action> = this.actions.pipe(
    ofType<CreatePartner>(PartnerTypes.CreatePartner),
    concatMap((action) =>
      from(this.partnerService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Partner has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreatePartnersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Partner", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreatePartnerError());
        })
      )
    )
  );
  @Effect()
  updatePartner$: Observable<Action> = this.actions.pipe(
    ofType<UpdatePartner>(PartnerTypes.UpdatePartner),
    concatMap((action) =>
      from(this.partnerService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Partner has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdatePartnersuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Partner", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdatePartnerError());
        })
      )
    )
  );
  @Effect()
  deletePartner$: Observable<Action> = this.actions.pipe(
    ofType<DeletePartner>(PartnerTypes.DeletePartner),
    concatMap((action) =>
      from(this.partnerService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Partner has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeletePartnersuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Partner:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeletePartnerError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
