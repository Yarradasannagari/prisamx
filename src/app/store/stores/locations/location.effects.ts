import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { LocationService } from "../../../services/locations/location.service"; 

import {
  GetLocation,
  GetLocationSuccess,
  GetLocationError,
  CreateLocation,
  CreateLocationsuccess,
  CreateLocationError,
  DeleteLocation,
  DeleteLocationsuccess,
  DeleteLocationError,
  LocationTypes,
  UpdateLocation,
  UpdateLocationsuccess,
  UpdateLocationError,
} from "./location.actions";

@Injectable()
export class LocationEffects {
  constructor(
    private actions: Actions,
    private locationService: LocationService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getLocation$: Observable<Action> = this.actions.pipe(
    ofType<GetLocation>(LocationTypes.GetLocation),
    exhaustMap(() =>
      from(this.locationService.list()).pipe(
        map((payload) => {
          return new GetLocationSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Location", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetLocationError(true));
        })
      )
    )
  );

  @Effect()
  createLocation$: Observable<Action> = this.actions.pipe(
    ofType<CreateLocation>(LocationTypes.CreateLocation),
    concatMap((action) =>
      from(this.locationService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateLocationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Location", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateLocationError());
        })
      )
    )
  );
  @Effect()
  updateLocation$: Observable<Action> = this.actions.pipe(
    ofType<UpdateLocation>(LocationTypes.UpdateLocation),
    concatMap((action) =>
      from(this.locationService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Location has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateLocationsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Location", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateLocationError());
        })
      )
    )
  );
  @Effect()
  deleteLocation$: Observable<Action> = this.actions.pipe(
    ofType<DeleteLocation>(LocationTypes.DeleteLocation),
    concatMap((action) =>
      from(this.locationService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Location has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteLocationsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Location:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteLocationError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
