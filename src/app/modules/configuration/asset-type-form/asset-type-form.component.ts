import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Assettype } from "src/app/services/asset-type/asset-type.service";
import { CreateAssettype, UpdateAssettype } from "src/app/store/stores/asset-type/asset-type.actions";
import { getAssettypeCreated, getAssettypeUpdated } from "src/app/store/stores/asset-type/asset-type.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-asset-type-form",
  templateUrl: "./asset-type-form.component.html",
  styleUrls: ["./asset-type-form.component.scss"],
})
export class AssetTypeFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    name: "",
    type: "",
    active: true,
  };
  assettype: Assettype;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        // console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.name = this.route.snapshot.data["report"].name;
        this.formData.type =  this.route.snapshot.data["report"].type;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    // console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      name: [
        this.formData.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(20)
          
        ]),
      ],
      type: [
        this.formData.type,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const assettype: Assettype = {
      id: this.formData.id,
      name: this.formData.name,
      type: this.formData.type,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateAssettype(assettype);
    } else {
      this.createAssettype(assettype);
    }
  }
  private createAssettype(assettype: Assettype): void {
    this.store.dispatch(new CreateAssettype(assettype));
    this.store
      .select(getAssettypeCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateAssettype(assettype: Assettype): void {
    this.store.dispatch(new UpdateAssettype(assettype));
    this.store
      .select(getAssettypeUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["configuration", "asset-type"]);
  }
}
