import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteLocationTypeModelComponent } from './delete-location-type-model.component';

describe('DeleteLocationTypeModelComponent', () => {
  let component: DeleteLocationTypeModelComponent;
  let fixture: ComponentFixture<DeleteLocationTypeModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteLocationTypeModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteLocationTypeModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
