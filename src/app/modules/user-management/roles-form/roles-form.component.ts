import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Role } from "src/app/services/roles/role.service";
import { CreateRole, UpdateRole } from "src/app/store/stores/roles/roles.actions";
import { getRoleCreated, getRoleUpdated } from "src/app/store/stores/roles/roles.store";
import { State, Store } from "../../../store";

@Component({
  selector: "app-roles-form",
  templateUrl: "./roles-form.component.html",
  styleUrls: ["./roles-form.component.scss"],
})
export class RolesFormComponent implements OnInit {
  routeId: any;
  isLoading = false;
  formGroup: FormGroup;
  formData = {
    id: null,
    role: "",
    description: "",
    active: true,
  };
  role: Role;
  private readonly destroy = new Subject<void>();
  constructor(
    private store: Store<State>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy)).subscribe((data) => {
      if (data.id) {
        this.routeId = data.id;
        console.log("form data ==" + this.route.snapshot.data["usrole"]);
        this.formData.id = this.route.snapshot.data["report"].id;
        this.formData.role = this.route.snapshot.data["report"].role;
        this.formData.description =
          this.route.snapshot.data["report"].description;
        this.formData.active = this.route.snapshot.data["report"].active;
        this.loadForm();
      } else {
        this.loadForm();
      }
    });
  }
  loadForm() {
    console.log("form data ==" + this.formData.role);
    this.formGroup = this.fb.group({
      role: [
        this.formData.role,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20),
          // Validators.pattern("[a-zA-Z0-9_]+"),
        ]),
      ],
      description: [
        this.formData.description,
        Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255),
        ]),
      ],
      active: [this.formData.active],
    });
  }
  submit() {
    const role: Role = {
      id: this.formData.id,
      description: this.formData.description,
      role: this.formData.role,
      active: this.formData.active,
    };
    if (this.formData.id) {
      this.updateRole(role);
    } else {
      this.createRole(role);
    }
  }
  private createRole(role: Role): void {
    this.store.dispatch(new CreateRole(role));
    this.store
      .select(getRoleCreated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  private updateRole(role: Role): void {
    this.store.dispatch(new UpdateRole(role));
    this.store
      .select(getRoleUpdated)
      .pipe(takeUntil(this.destroy))
      .subscribe((created) => {
        if (created) {
          //  this.snackBar.open('Access control rule has been created.', null, { duration: 6000 });
          this.cancel();
        }
      });
  }
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation, controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  cancel(): void {
    this.router.navigate(["user-management", "roles"]);
  }
}
