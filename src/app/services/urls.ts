import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

const endpoints = {
  roles: '/usroles',
  users: '/users',
  groups: '/usgrps',
  organizations: '/orgids',
  country: '/countries',
  plants: '/plants',
  locationcat: '/loc-catergories',
  locationtype: '/loc-types',
  assetcat: '/asset-catergories',
  assettype: '/asset-types',
  inspectioncat: '/inspcs',
  inspectiontype: '/inspts',
  inspectioncode: '/inspvs',
  inspectiongroup: '/inspgs',
  inspectionclass: '/inspclasses',
  unit: '/uoms',
  assets: '/assets',
  locations: '/locations',
  checklist: 'eotfclrs',
  otfheader: '/eotfhs',
  otfheaderupload: '/eotfhs/upload',
  otfitems: '/eotfis',
  inspectionpoint: '/points',
  partner: '/partner',
  document: '/documents',
  classification: '/classification',

};

@Injectable()
export class Url {
  public static roles = `${environment.apiUrl}${endpoints.roles}`;
  public static users = `${environment.apiUrl}${endpoints.users}`;
  public static groups = `${environment.apiUrl}${endpoints.groups}`;
  public static organizations = `${environment.apiUrl}${endpoints.organizations}`;
  public static country = `${environment.apiUrl}${endpoints.country}`;
  public static plants = `${environment.apiUrl}${endpoints.plants}`;
  public static locationcat = `${environment.apiUrl}${endpoints.locationcat}`;
  public static locationtype = `${environment.apiUrl}${endpoints.locationtype}`;
  public static assetcat = `${environment.apiUrl}${endpoints.assetcat}`;
  public static assettype = `${environment.apiUrl}${endpoints.assettype}`;
  public static inspectioncat = `${environment.apiUrl}${endpoints.inspectioncat}`;
  public static inspectiontype = `${environment.apiUrl}${endpoints.inspectiontype}`;
  public static inspectioncode = `${environment.apiUrl}${endpoints.inspectioncode}`;
  public static inspectiongroup = `${environment.apiUrl}${endpoints.inspectiongroup}`;
  public static inspectionclass = `${environment.apiUrl}${endpoints.inspectionclass}`;
  public static unit = `${environment.apiUrl}${endpoints.unit}`;
  public static assets = `${environment.apiUrl}${endpoints.assets}`;
  public static locations = `${environment.apiUrl}${endpoints.locations}`;
  public static checklist = `${environment.apiUrl}${endpoints.checklist}`;
  public static otfheader = `${environment.apiUrl}${endpoints.otfheader}`;
  public static otfheaderupload = `${environment.apiUrl}${endpoints.otfheaderupload}`;
  public static otfitems = `${environment.apiUrl}${endpoints.otfitems}`;
  public static inspectionpoint = `${environment.apiUrl}${endpoints.inspectionpoint}`;
  public static partner = `${environment.apiUrl}${endpoints.partner}`;
  public static document = `${environment.apiUrl}${endpoints.document}`;
  public static classification = `${environment.apiUrl}${endpoints.classification}`;

}
