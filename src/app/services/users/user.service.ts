import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { StringLiteral } from "typescript";
import { EntityService } from "../entity.service";

import { Url } from "../urls";

export interface User {
  id: string;
  activated: boolean;
  authorities: any;
  deviceId?: string;
  devicepf?: string;
  devicesno?: string;
  email?: string;
  firstName?: string;
  imageUrl?: string;
  langKey?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  lastName?: string;
  login: string;
  orgId?: string;
  parvw?: string;
  personId?: string;
  pgroup?: string;
  plant?: string;
  pplant?: string;
  telmb?: string;
  telnr?: string;
  timezone?: string;
  avalue?: string;
  usgrp?: string;
  ustyp?: string;
  wkctr?: string;
  createdBy?: string;
  createdDate?: string;
  changedBy?: number;
  changedDate?: string;
}

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(
    private readonly http: HttpClient,
    private entitySvc: EntityService<any>
  ) {}
  get(id: any): Observable<User> {
    return this.entitySvc.get(Url.users, id);
  }

  list(params?: Record<string, any>): Observable<User[]> {
    return this.entitySvc.list(Url.users, params);
  }

  create(user: User): Observable<User> {
    return this.entitySvc.create(Url.users, user);
  }

  update(user: User): Observable<User> {
    return this.entitySvc.update(Url.users, user);
  }

  delete(id: string): Observable<void> {
    return this.entitySvc.delete(Url.users, id);
  }
}
