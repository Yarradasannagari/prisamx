import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { PlantService } from "../../../services/plant/plant.service"; 

import {
  GetPlant,
  GetPlantSuccess,
  GetPlantError,
  CreatePlant,
  CreatePlantsuccess,
  CreatePlantError,
  DeletePlant,
  DeletePlantsuccess,
  DeletePlantError,
  PlantTypes,
  UpdatePlant,
  UpdatePlantsuccess,
  UpdatePlantError,
} from "./plant.actions";

@Injectable()
export class PlantEffects {
  constructor(
    private actions: Actions,
    private plantService: PlantService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getPlant$: Observable<Action> = this.actions.pipe(
    ofType<GetPlant>(PlantTypes.GetPlant),
    exhaustMap(() =>
      from(this.plantService.list()).pipe(
        map((payload) => {
          return new GetPlantSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Plant", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetPlantError(true));
        })
      )
    )
  );

  @Effect()
  createPlant$: Observable<Action> = this.actions.pipe(
    ofType<CreatePlant>(PlantTypes.CreatePlant),
    concatMap((action) =>
      from(this.plantService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Plant has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreatePlantsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Plant", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreatePlantError());
        })
      )
    )
  );
  @Effect()
  updatePlant$: Observable<Action> = this.actions.pipe(
    ofType<UpdatePlant>(PlantTypes.UpdatePlant),
    concatMap((action) =>
      from(this.plantService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Plant has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdatePlantsuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Plant", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdatePlantError());
        })
      )
    )
  );
  @Effect()
  deletePlant$: Observable<Action> = this.actions.pipe(
    ofType<DeletePlant>(PlantTypes.DeletePlant),
    concatMap((action) =>
      from(this.plantService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Plant has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeletePlantsuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Plant:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeletePlantError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
