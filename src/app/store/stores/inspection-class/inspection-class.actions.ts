import { Action } from "@ngrx/store";
import { Inspectionclass } from "../../../services/inspection-class/inspection-class.service";

export enum InspectionclassTypes { 
  GetInspectionclass = "[Inspectionclass] GetInspectionclass",
  GetInspectionclassSuccess = "[Inspectionclass] GetInspectionclassSuccess",
  GetInspectionclassError = "[Inspectionclass] GetInspectionclassError",
  CreateInspectionclass = "[Inspectionclass] CreateInspectionclass",
  CreateInspectionclasssuccess = "[Inspectionclass] CreateInspectionclasssuccess",
  CreateInspectionclassError = "[Inspectionclass] CreateInspectionclassError",
  UpdateInspectionclass = "[Inspectionclass] UpdateInspectionclass",
  UpdateInspectionclasssuccess = "[Inspectionclass] UpdateInspectionclasssuccess",
  UpdateInspectionclassError = "[Inspectionclass] UpdateInspectionclassError",
  DeleteInspectionclass = "[Inspectionclass] DeleteInspectionclass",
  DeleteInspectionclassError = "[Inspectionclass] DeleteInspectionclassError",
  DeletingAclRule = "[AclRule] DeletingAclRule",
  DeleteInspectionclasssuccess = "[AclRule] DeleteInspectionclasssuccess",
}

export class GetInspectionclass implements Action { 
  readonly type = InspectionclassTypes.GetInspectionclass;
}

export class GetInspectionclassSuccess implements Action {
  readonly type = InspectionclassTypes.GetInspectionclassSuccess;
  constructor(readonly payload: Inspectionclass[]) {}
}
export class GetInspectionclassError implements Action {
  readonly type = InspectionclassTypes.GetInspectionclassError;
  constructor(readonly payload: boolean) {}
}

export class CreateInspectionclass implements Action {
  readonly type = InspectionclassTypes.CreateInspectionclass;
  constructor(readonly payload: Inspectionclass) {}
}

export class CreateInspectionclasssuccess implements Action {
  readonly type = InspectionclassTypes.CreateInspectionclasssuccess;
  constructor(readonly payload: Inspectionclass) {}
}

export class CreateInspectionclassError implements Action {
  readonly type = InspectionclassTypes.CreateInspectionclassError;
}
export class UpdateInspectionclass implements Action {
  readonly type = InspectionclassTypes.UpdateInspectionclass;
  constructor(readonly payload: Inspectionclass) {}
}

export class UpdateInspectionclasssuccess implements Action {
  readonly type = InspectionclassTypes.UpdateInspectionclasssuccess;
  constructor(readonly payload: Inspectionclass) {}
}

export class UpdateInspectionclassError implements Action {
  readonly type = InspectionclassTypes.UpdateInspectionclassError;
}

export class DeleteInspectionclass implements Action {
  readonly type = InspectionclassTypes.DeleteInspectionclass;
  constructor(readonly payload: string) {}
}

export class DeleteInspectionclasssuccess implements Action {
  readonly type = InspectionclassTypes.DeleteInspectionclasssuccess;
  constructor(readonly payload: string) {}
}

export class DeleteInspectionclassError implements Action {
  readonly type = InspectionclassTypes.DeleteInspectionclassError;
  constructor(readonly payload: boolean) {}
}

export type Union =
  | GetInspectionclass
  | GetInspectionclassSuccess
  | GetInspectionclassError
  | CreateInspectionclass
  | CreateInspectionclasssuccess
  | CreateInspectionclassError
  | DeleteInspectionclass
  | DeleteInspectionclasssuccess
  | DeleteInspectionclassError
  | UpdateInspectionclass
  | UpdateInspectionclasssuccess
  | UpdateInspectionclassError
