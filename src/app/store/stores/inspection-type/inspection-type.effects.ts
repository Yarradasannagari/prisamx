import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { from, Observable, of } from "rxjs";
import { catchError, concatMap, exhaustMap, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

import { InspectiontypeService } from "../../../services/inspection-types/inspection-type.service";  

import {
  GetInspectiontype,
  GetInspectiontypeSuccess,
  GetInspectiontypeError,
  CreateInspectiontype,
  CreateInspectiontypesuccess,
  CreateInspectiontypeError,
  DeleteInspectiontype,
  DeleteInspectiontypesuccess,
  DeleteInspectiontypeError,
  InspectiontypeTypes,
  UpdateInspectiontype,
  UpdateInspectiontypesuccess,
  UpdateInspectiontypeError,
} from "./inspection-type.actions";

@Injectable()
export class InspectiontypeEffects {
  constructor(
    private actions: Actions,
    private inspectiontypeService: InspectiontypeService,
    private toastrService: ToastrService
  ) {}

  @Effect()
  getInspectiontype$: Observable<Action> = this.actions.pipe(
    ofType<GetInspectiontype>(InspectiontypeTypes.GetInspectiontype),
    exhaustMap(() =>
      from(this.inspectiontypeService.list()).pipe(
        map((payload) => {
          return new GetInspectiontypeSuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to retrieve Inspection Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          //  this.showMessage(`Failed to retrieve Access Control Rules: ${err.statusText}`);
          return of(new GetInspectiontypeError(true));
        })
      )
    )
  );

  @Effect()
  createInspectiontype$: Observable<Action> = this.actions.pipe(
    ofType<CreateInspectiontype>(InspectiontypeTypes.CreateInspectiontype),
    concatMap((action) =>
      from(this.inspectiontypeService.create(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Type has been created.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new CreateInspectiontypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to create Inspection Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new CreateInspectiontypeError());
        })
      )
    )
  );
  @Effect()
  updateInspectiontype$: Observable<Action> = this.actions.pipe(
    ofType<UpdateInspectiontype>(InspectiontypeTypes.UpdateInspectiontype),
    concatMap((action) =>
      from(this.inspectiontypeService.update(action.payload)).pipe(
        map((payload) => {
          this.toastrService.success(
            "The Inspection Type has been updated.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new UpdateInspectiontypesuccess(payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to update Inspection Type", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to create Access Control Rule: ${err.statusText}`);
          return of(new UpdateInspectiontypeError());
        })
      )
    )
  );
  @Effect()
  deleteInspectiontype$: Observable<Action> = this.actions.pipe(
    ofType<DeleteInspectiontype>(InspectiontypeTypes.DeleteInspectiontype),
    concatMap((action) =>
      from(this.inspectiontypeService.delete(action.payload)).pipe(
        map(() => {
          this.toastrService.success(
            "The Inspection Type has been deleted.",
            "Success...",
            {
              timeOut: 3000,
              positionClass: "toast-bottom-center",
            }
          );
          return new DeleteInspectiontypesuccess(action.payload);
        }),
        catchError((err) => {
          this.toastrService.error("Failed to delete Inspection Type:", "Error...", {
            timeOut: 3000,
            positionClass: "toast-bottom-center",
          });
          // this.showMessage(`Failed to delete AclRUle: ${err.statusText}`);
          return of(new DeleteInspectiontypeError(true));
        })
      )
    )
  );

  // showMessage(message: string): void {
  //   this.snackBar.open(message, undefined, { duration: 10000 });
  // }
}
